import Batteries.Data.Fin.Fold
import Mathlib.Topology.UnitInterval
import Complex.Topology
import Complex.Norm
import Complex.Differentiation
import Complex.Exp

@[simp]
theorem Path.cast_range [TopologicalSpace X] {a b a' b' : X} {p : Path a b}
    {ha : a' = a} {hb : b' = b} : Set.range (p.cast ha hb) = Set.range p := rfl

namespace Complex₁

open Interval
open scoped unitInterval

-- some technical lemmas needed later

lemma restrict_extend_subtype {T U : Type*} {s : Set T} {f : s → U} {g : T → U} :
    s.restrict (Function.extend Subtype.val f g) = f := by
  funext y
  simp

lemma derivWithin_eq_deriv_restrict_extend {f f' : ℝ → ℂ₁} (fderiv : ∀ x, HasDerivAt f (f' x) x) :
    derivWithin f I = Function.extend Subtype.val ((I).restrict (deriv f)) 0 := by
  funext x
  by_cases xIn : x ∈ I
  . rw [Function.extend, dif_pos ⟨⟨x, xIn⟩, by simp⟩]
    simp
    rw [(fderiv x).hasDerivWithinAt.derivWithin]; rotate_left
    . exact uniqueDiffOn_Icc_zero_one.uniqueDiffWithinAt xIn
    have : ∃ (a : I), a = x := ⟨⟨x, xIn⟩, by simp⟩
    rw [this.choose_spec]
    exact (fderiv x).deriv.symm
  . rw [Function.extend, dif_neg]; rotate_left
    . simp at xIn ⊢
      exact xIn
    apply derivWithin_zero_of_nmem_closure
    convert xIn
    simp

lemma hasDerivWithinAt_restrict_path_extend_iff {a b f' : ℂ₁} {p : Path a b} (f : ℝ → ℂ₁)
    (funeq : p.toFun = (I).restrict f) :
    HasDerivWithinAt p.extend f' I x ↔ HasDerivWithinAt f f' I x := by
  by_cases xIn : x ∈ I; rotate_left
  . constructor
    all_goals
      intros
      apply hasFDerivWithinAt_of_nmem_closure
      simp [xIn]
  constructor
  all_goals (intro h; apply h.congr)
  any_goals (intro x xIn)
  pick_goal 3; symm
  pick_goal 4; symm
  all_goals
    rw [Path.extend_extends _ xIn]
    show f x = p.toFun ⟨x, xIn⟩
    rw [funeq]
    simp

lemma differentiableWithinAt_restrict_path_extend_iff {a b : ℂ₁} {p : Path a b} (f : ℝ → ℂ₁)
    (funeq : p.toFun = (I).restrict f) :
    DifferentiableWithinAt ℝ p.extend I x ↔ DifferentiableWithinAt ℝ f I x := by
  constructor
  . rintro ⟨f', h⟩
    use f'
    convert ((hasDerivWithinAt_restrict_path_extend_iff f funeq).mp h.hasDerivWithinAt).hasFDerivWithinAt
    simp
  . rintro ⟨f', h⟩
    use f'
    convert ((hasDerivWithinAt_restrict_path_extend_iff f funeq).mpr h.hasDerivWithinAt).hasFDerivWithinAt
    simp

lemma derivWithin_restrict_path_extend_eq {a b : ℂ₁} {p : Path a b} (f : ℝ → ℂ₁)
    (funeq : p.toFun = (I).restrict f) :
    derivWithin p.extend I = derivWithin f I := by
  funext x
  by_cases diff : DifferentiableWithinAt ℝ f I x
  . by_cases xIn : x ∈ I; rotate_left
    . rw [derivWithin_zero_of_nmem_closure, derivWithin_zero_of_nmem_closure]
      all_goals simp [xIn]
    obtain ⟨f', diffEq⟩ := diff
    apply (·.hasDerivWithinAt) at diffEq
    have diffEq' := (hasDerivWithinAt_restrict_path_extend_iff f funeq).mpr diffEq
    have uniqDiff : UniqueDiffWithinAt ℝ I x := uniqueDiffOn_Icc_zero_one.uniqueDiffWithinAt xIn
    rw [diffEq.derivWithin uniqDiff, diffEq'.derivWithin uniqDiff]
  . have diff' := (differentiableWithinAt_restrict_path_extend_iff f funeq).mp.mt diff
    rw [derivWithin_zero_of_not_differentiableWithinAt diff,
        derivWithin_zero_of_not_differentiableWithinAt diff']

lemma xSymmIn (xIn : x ∈ I) : 1-x ∈ I := by simp at *; tauto

-- define curve as C1 path ℝ → ℂ₁

structure Curve (source target : ℂ₁) where
  path : Path source target
  isDifferentiable : ∀ x ∈ I, DifferentiableWithinAt ℝ path.extend I x
  derivContinuous : ContinuousOn (derivWithin path.extend I) I

@[ext]
protected theorem Curve.ext : ∀ {γ₁ γ₂ : Curve a b}, γ₁.path = γ₂.path → γ₁ = γ₂ := by
  rintro ⟨p₁, _, _⟩ ⟨p₂, _, _⟩ pathEq
  simp at pathEq
  simp_rw [pathEq]

instance Curve.funLike : FunLike (Curve a b) I ℂ₁ where
  coe γ := ⇑γ.path
  coe_injective' := by
    intro γ₁ γ₂ coeEq
    simp [DFunLike.coe_fn_eq] at coeEq
    ext : 1
    exact coeEq

def Curve.cast (γ : Curve a b) (ha : a' = a) (hb : b' = b) : Curve a' b' where
  path := γ.path.cast ha hb
  isDifferentiable := γ.isDifferentiable
  derivContinuous := γ.derivContinuous

@[simp]
def Curve.cast_eq {γ : Curve a b} : (γ.cast ha hb) x = γ x := rfl

@[simp]
theorem Curve.cast_path_extend {γ : Curve a b} : (γ.cast ha hb).path.extend x = γ.path.extend x := rfl

-- continuously extends derivative of path to the whole of ℝ
-- this makes it easier to work with
noncomputable def Curve.deriv {a b : ℂ₁} (γ : Curve a b) : ℝ → ℂ₁ :=
  Set.IccExtend (by norm_num) ((I).restrict (derivWithin γ.path.extend I))

theorem Curve.derivContinuous' (γ : Curve a b) : Continuous γ.deriv := by
  apply Continuous.Icc_extend'
  rw [← continuousOn_iff_continuous_restrict]
  exact γ.derivContinuous

-- make sure this extended derivative still agrees with the original one on I
theorem Curve.deriv_eq {γ : Curve a b} (xIn : x ∈ I) : (γ.deriv x) = (derivWithin γ.path.extend I) x := by
  apply Set.IccExtend_of_mem
  exact xIn

@[simp]
theorem Curve.cast_deriv {γ : Curve a b} : (γ.cast ha hb).deriv = γ.deriv := rfl

theorem Curve.hasDerivWithinAt (γ : Curve a b) (xIn : x ∈ I) :
    HasDerivWithinAt γ.path.extend (γ.deriv x) I x := by
  rw [deriv_eq xIn]
  exact (γ.isDifferentiable x xIn).hasDerivWithinAt

noncomputable def Curve.length (γ : Curve a b) := ∫ x in (0 : ℝ)..1, ‖γ.deriv x‖

theorem Curve.length_nonneg {γ : Curve a b} : γ.length ≥ 0 := by
  simp [Curve.length]
  apply intervalIntegral.integral_nonneg (by norm_num)
  intros
  exact norm_nonneg _

theorem Curve.range_compact {γ : Curve a b} : IsCompact (Set.range γ) := by
  rw [← Set.image_univ]
  exact CompactSpace.isCompact_univ.image γ.path.continuous

@[simp]
theorem Curve.cast_range {γ : Curve a b} : Set.range (γ.cast ha hb) = Set.range γ := rfl

theorem Curve.path_extend_range_eq_range {γ : Curve a b} : Set.range γ.path.extend = Set.range γ := by
  ext x; constructor
  . intro xIn
    obtain ⟨y, rfl⟩ := xIn
    by_cases yle : y ≤ 0
    . rw [γ.path.extend_of_le_zero yle]
      use 0
      exact γ.path.source
    by_cases yge : y ≥ 1
    . rw [γ.path.extend_of_one_le yge]
      use 1
      exact γ.path.target
    have yI : y ∈ I := by simp; constructor <;> linarith
    use ⟨y, yI⟩
    rw [γ.path.extend_extends yI]
    rfl
  . intro xIn
    obtain ⟨y, rfl⟩ := xIn
    use y
    rw [γ.path.extend_extends']
    rfl

-- reverse curve (-γ)

lemma curve_reverseFun_hasDeriv {γ : Curve a b} (xIn : x ∈ I) :
    HasDerivWithinAt γ.path.symm.extend (-γ.deriv (1-x)) I x := by
  let f := γ.path.extend ∘ (fun t ↦ 1-t)
  apply HasDerivWithinAt.congr (f := f); rotate_left
  intro x xIn
  iterate 2
    simp [f, Path.extend_extends _ xIn, unitInterval.symm]
    rw [Path.extend_extends]

  let L₁ : ℝ →L[ℝ] ℂ₁ := (ContinuousLinearMap.id ℝ ℝ).smulRight (γ.deriv (1-x))
  let L₂ : ℝ →L[ℝ] ℝ := (ContinuousLinearMap.id ℝ ℝ).smulRight (-1)
  have : (L₁.comp L₂) 1 = -γ.deriv (1-x) := by simp [L₁, L₂]
  rw [← this]
  apply HasFDerivWithinAt.hasDerivWithinAt
  apply HasFDerivWithinAt.comp (t := I); rotate_right
  . intro x xIn
    simp at xIn ⊢
    tauto
  . apply HasDerivWithinAt.hasFDerivWithinAt
    obtain ⟨d, dEq⟩ := γ.isDifferentiable (1-x) (xSymmIn xIn)
    apply γ.hasDerivWithinAt
    simp at xIn ⊢
    tauto
  . apply HasDerivWithinAt.hasFDerivWithinAt
    rw [← zero_sub (1)]
    exact (hasDerivWithinAt_const _ _ _).sub (hasDerivWithinAt_id _ _)

def Curve.reverse (γ : Curve a b) : Curve b a where
  path := γ.path.symm
  isDifferentiable := fun _ xIn ↦ (curve_reverseFun_hasDeriv xIn).differentiableWithinAt
  derivContinuous := by
    intro x xIn
    rw [continuousWithinAt_iff_continuousAt_restrict _ xIn]
    have : (I).restrict (derivWithin γ.path.symm.extend I) =
        (I).restrict (fun y ↦ -(γ.deriv (1-y))) := by
      funext ⟨y, yIn⟩
      simp
      rw [(curve_reverseFun_hasDeriv yIn).derivWithin]
      exact uniqueDiffOn_Icc_zero_one.uniqueDiffWithinAt yIn
    rw [this, ← continuousWithinAt_iff_continuousAt_restrict]
    apply γ.derivContinuous'.continuousWithinAt.neg.comp (s := I) (t := I)
    . exact continuousWithinAt_const.sub continuousWithinAt_id
    . intro y yIn
      exact xSymmIn yIn

@[simp]
theorem Curve.reverse_range {γ : Curve a b} : Set.range (γ.reverse) = Set.range γ :=
  Path.symm_range _

-- define contour as sum of curves

structure Contour (source target : ℂ₁) where
  n : ℕ
  sources : Fin (n+1) → ℂ₁
  targets : Fin (n+1) → ℂ₁
  curves : (i : Fin (n+1)) → Curve (sources i) (targets i)
  sourceEq : source = sources 0
  targetEq : target = targets (Fin.last _)
  adjacent : ∀ (i : Fin n), targets i = sources (i+1)

namespace Contour

def cast (γ : Contour a b) (ha : a' = a) (hb : b' = b) : Contour a' b' where
  n := γ.n
  sources := γ.sources
  targets := γ.targets
  curves := γ.curves
  sourceEq := by rw [ha]; exact γ.sourceEq
  targetEq := by rw [hb]; exact γ.targetEq
  adjacent := γ.adjacent

noncomputable def path (γ : Contour a b) : Path a b :=
  let path' :=
    Fin.dfoldr γ.n
      (fun n ↦ Path (γ.sources n) b)
      (fun n nextPath ↦ ((γ.curves n).path.cast (by simp) (by rw [γ.adjacent]; simp)).trans nextPath)
      ((γ.curves γ.n).cast (by simp) (by simp; exact γ.targetEq)).path
  path'.cast γ.sourceEq rfl

def single (c : Curve a b) : Contour a b := ⟨0, _, _, fun _ ↦ c, rfl, rfl, Fin.elim0⟩

theorem curve_path_eq_single_contour_path : c.path = (Contour.single c).path := by
  simp [single]
  rfl

def reverse (c : Contour a b) : Contour b a where
  n := c.n
  sources := c.targets ∘ Fin.revPerm
  targets := c.sources ∘ Fin.revPerm
  curves := fun i ↦ (c.curves (Fin.revPerm i)).reverse
  sourceEq := c.targetEq
  targetEq := by simp [c.sourceEq]
  adjacent := by
    intro i
    simp
    suffices ∃ (j : Fin c.n), c.targets j = c.sources i.castSucc.rev ∧
        c.targets j = c.targets i.succ.rev by
      obtain ⟨j, eq₁, eq₂⟩ := this
      rw [← eq₁, eq₂]
    rw [Fin.exists_iff]
    have lt : ↑i.succ.rev < c.n := by simp [Fin.rev, Fin.pos i]
    refine' ⟨i.succ.rev, lt, _, _⟩
    . rw [c.adjacent ⟨↑i.succ.rev, lt⟩]
      simp [Fin.rev]; congr; norm_cast
      ext
      rw [Fin.val_cast_of_lt]
      . simp; omega
      . omega
    . simp [Fin.rev]; congr; ext
      rw [Fin.val_cast_of_lt]
      omega

def join (γ₁ : Contour a b) (γ₂ : Contour b c) : Contour a c where
  n := (γ₁.n + 1) + γ₂.n
  sources := fun i ↦ Fin.append γ₁.sources γ₂.sources (Fin.cast rfl i)
  targets := fun i ↦ Fin.append γ₁.targets γ₂.targets (Fin.cast rfl i)
  curves :=
    Fin.addCases (m := γ₁.n + 1) (n := γ₂.n + 1)
      (fun i ↦ (γ₁.curves i).cast (by simp) (by simp))
      (fun i ↦ (γ₂.curves i).cast (by simp) (by simp))
  sourceEq := by
    simp_rw [γ₁.sourceEq]
    symm
    exact Fin.append_left γ₁.sources γ₂.sources 0
  targetEq := by
    simp_rw [γ₂.targetEq]
    symm
    exact Fin.append_right γ₁.targets γ₂.targets (Fin.last γ₂.n)
  adjacent := by
    intro i
    by_cases ilt : i < γ₁.n
    . convert γ₁.adjacent (Fin.castLT i ilt)
      . convert Fin.append_left γ₁.targets γ₂.targets i using 2
        apply Fin.eq_of_val_eq
        simp
        rw [Nat.mod_eq_of_lt]
        linarith
      . convert Fin.append_left γ₁.sources γ₂.sources (i+1) using 2
        apply Fin.eq_of_val_eq
        simp
        rw [Fin.val_add]
        simp
        rw [Nat.mod_eq_of_lt]
        linarith
    by_cases igt : i > γ₁.n
    . let j' := i - γ₁.n - 1
      have j'lt : j' < γ₂.n := by omega
      let j := Fin.mk j' j'lt
      convert γ₂.adjacent j
      . convert Fin.append_right γ₁.targets γ₂.targets j
        apply Fin.eq_of_val_eq
        simp [j]
        rw [Nat.mod_eq_of_lt (by omega)]
        omega
      . convert Fin.append_right γ₁.sources γ₂.sources (j+1)
        apply Fin.eq_of_val_eq
        simp [j]
        rw [Fin.val_add]
        simp
        rw [Nat.mod_eq_of_lt (by omega)]
        omega
    . have ieq : i = γ₁.n := by omega
      have ilt' : i < γ₁.n + 1 + (γ₂.n + 1) := by omega
      have i1lt : i+1 < γ₁.n + 1 + (γ₂.n + 1) := by omega
      calc
        _ = Fin.append γ₁.targets γ₂.targets (Fin.castLT i ilt') := by simp; congr
        _ = b := by
          simp_rw [γ₁.targetEq]
          convert Fin.append_left _ _ _
          apply Fin.eq_of_val_eq
          simp [ieq]
        _ = Fin.append γ₁.sources γ₂.sources (Fin.mk (i+1) i1lt) := by
          simp_rw [γ₂.sourceEq]
          symm
          convert Fin.append_right _ _ _
          simp [ieq]
        _ = _ := by simp; congr

noncomputable def length (γ : Contour a b) := ∑ i : Fin (γ.n+1), (γ.curves i).length

theorem length_nonneg {γ : Contour a b} : γ.length ≥ 0 := by
  simp [Contour.length]
  apply Finset.sum_nonneg
  intro i _
  exact (γ.curves i).length_nonneg

theorem single_length : (single γ).length = γ.length := by simp [single, length]

def range (γ : Contour a b) := ⋃ (i : Fin (γ.n+1)), Set.range (γ.curves i)

theorem source_mem_range {γ : Contour a b} : a ∈ γ.range := by
  simp [range]
  refine' ⟨0, 0, by norm_num, _⟩
  convert (γ.curves 0).path.source
  exact γ.sourceEq

theorem target_mem_range {γ : Contour a b} : b ∈ γ.range := by
  simp [range]
  refine' ⟨Fin.last _, 1, by norm_num, _⟩
  convert (γ.curves (Fin.last _)).path.target
  exact γ.targetEq

theorem segment_range_subset_contour_range {γ : Contour a b} {i : Fin (γ.n+1)} :
    Set.range (γ.curves i) ⊆ γ.range := by
  simp [range]
  exact Set.subset_iUnion_of_subset i (subset_refl _)

theorem path_range_eq_range {γ : Contour a b} : Set.range γ.path = γ.range := by
  have : ∀ (m : ℕ) (a' b' : ℂ₁) (γ' : Contour a' b'), γ'.n = m → Set.range γ'.path = γ'.range := by
    intro m
    induction' m with m' ih
    . intro a' b' γ' γl
      let {n, sources, targets, curves, sourceEq, targetEq, adjacent} := γ'
      simp at γl; subst γl
      symm; apply Set.iUnion_eq_const
      intro i
      rw [Fin.fin_one_eq_zero i]
      rfl
    . intro a' b' γ' γl
      let {n, sources, targets, curves, sourceEq, targetEq, adjacent} := γ'
      simp at γl; subst γl
      let γ'' : Contour (sources 1) b' :=
        Contour.mk m' (Fin.tail sources) (Fin.tail targets) (Fin.tail curves)
          rfl
          (by simp [targetEq]; rfl)
          (by
            intro i
            convert adjacent i.succ
            . rw [Fin.tail_def]
              norm_cast
            . rw [Fin.tail_def]
              push_cast
              simp
          )
      specialize ih (sources 1) b' γ'' rfl
      dsimp [path, range] at ih ⊢
      rw [Fin.dfoldr_succ, Path.trans_range]
      have union_eq :
        ⋃ i, Set.range (curves i) = Set.range (curves 0) ∪ (⋃ i, Set.range (γ''.curves i)) := by
        ext x; constructor
        . rintro ⟨S, ⟨i, iS⟩, xS⟩
          rw [← iS] at xS
          by_cases iz : i = 0
          . rw [iz] at xS
            exact Or.intro_left _ xS
          . right
            obtain ⟨i', rfl⟩ := Fin.eq_succ_of_ne_zero iz
            rw [Set.mem_iUnion]
            use i'
            exact xS
        . rintro (h | ⟨S, ⟨i, iS⟩, xS⟩)
          . rw [Set.mem_iUnion]
            exact ⟨0, h⟩
          . rw [Set.mem_iUnion]
            rw [← iS] at xS
            exact ⟨i.succ, xS⟩
      rw [union_eq, ← ih]
      congr
      simp only [γ'', Function.comp_def, Fin.tail_def]
      congr! 7
      . norm_cast
      . congr; norm_cast
      all_goals
        push_cast
        simp
        norm_cast
        exact Fin.natCast_eq_last (m' + 1)

  apply this γ.n a b γ rfl

theorem range_compact {γ : Contour a b} : IsCompact γ.range :=
  isCompact_iUnion (fun _ ↦ Curve.range_compact)

@[simp]
theorem single_range {γ : Curve a b} : (single γ).range = Set.range γ := by
  simp [range, single]
  exact Set.iUnion_const (Set.range ⇑γ)

@[simp]
theorem reverse_range {γ : Contour a b} : γ.reverse.range = γ.range := by
  simp [range, reverse]
  ext x; constructor
  all_goals
    rintro ⟨S, ⟨i, iS⟩, xS⟩
    simp at iS
    rw [Set.mem_iUnion]
    use i.rev
    try rw [Fin.rev_rev]
    rwa [iS]

@[simp]
theorem join_range {γ : Contour a b} {η : Contour b c} : (γ.join η).range = γ.range ∪ η.range := by
  simp [range, join]
  ext x; constructor
  . rintro ⟨S, ⟨i, iS⟩, xS⟩
    simp at iS
    rw [← iS] at xS
    obtain ⟨y, rfl⟩ := xS
    by_cases ile : ↑i ≤ γ.n
    . left
      rw [Set.mem_iUnion]
      use i; use y
      rw [Fin.addCases, dif_pos (by omega)]
      simp
      congr
      all_goals
        apply Fin.eq_of_val_eq
        simp; omega
    . right
      rw [Set.mem_iUnion]
      let j : Fin (η.n + 1) := ⟨i-(γ.n+1), by omega⟩
      have ij : i = Fin.natAdd (γ.n+1) j := by apply Fin.eq_of_val_eq; simp [j]; omega
      use j; use y
      rw [ij, Fin.addCases_right]
      simp
  . rw [Set.mem_union]
    rintro (⟨S, ⟨i, iS⟩, xS⟩ | ⟨S, ⟨i, iS⟩, xS⟩)
    all_goals
      simp at iS
      rw [← iS] at xS
      obtain ⟨y, rfl⟩ := xS
      rw [Set.mem_iUnion]
    . use i; use y
      have : ((i : ℕ) : Fin (γ.n+1+η.n+1)) = Fin.castAdd (η.n+1) i := by
        apply Fin.eq_of_val_eq
        simp; omega
      rw [this, Fin.addCases_left]
      simp
    . use Fin.natAdd (γ.n+1) i; use y
      rw [Fin.addCases_right]
      simp

end Contour

-- simple curves and contours

namespace Curve

open Differentiation in
lemma lineFun_hasDerivAt (z w : ℂ₁) (x : ℝ) :
    HasDerivAt (fun (t : ℝ) ↦ (1-t)*z + t*w) (w - z) x := by
  have : w-z=-z+w := by ring
  rw [this]
  apply HasDerivAt.add
  . have : -z = (0 - 1)*z := by ring
    rw [this]
    apply ((hasDerivAt_const _ _).sub hasDerivAt_r_inj_c).mul_const
  . have : w = 1*w := by ring
    nth_rw 2 [this]
    apply hasDerivAt_r_inj_c.mul_const

noncomputable def line (z w : ℂ₁) : Curve z w := {
  path := {
    toFun := fun t ↦ (1-t)*z + t*w
    continuous_toFun := by
      apply Continuous.add
      . apply Continuous.mul _ continuous_const
        apply Continuous.sub continuous_const
        exact r_inj_c_continuous.comp (Continuous.subtype_val continuous_id)
      . apply Continuous.mul _ continuous_const
        exact r_inj_c_continuous.comp (Continuous.subtype_val continuous_id)
    source' := by simp [r_inj_c, ← zero_def]
    target' := by simp [r_inj_c, ← one_def]
  }
  isDifferentiable := by
    intro x xIn
    apply DifferentiableWithinAt.congr (f := (fun (t : ℝ) ↦ (1-t)*z + t*w))
    exact (lineFun_hasDerivAt z w x).differentiableAt.differentiableWithinAt
    all_goals (
      try intro x xIn
      rw [Path.extend_extends]; rotate_left
      . simp at xIn ⊢
        exact xIn
      congr
    )
  derivContinuous := by
    let f := fun (x : ℝ) ↦ by
      by_cases h : x ∈ I
      . exact w-z
      . exact 0
    have f_cont : ContinuousOn f I := by
      intro x xIn
      rw [continuousWithinAt_iff_continuousAt_restrict _ xIn]
      have : (I).restrict f = fun _ ↦ w-z := by
        funext t
        unfold f
        simp
        have tElem : ↑t ∈ (I : Set ℝ) := Subtype.coe_prop t
        rw [Set.mem_Icc] at tElem
        intro h
        have := h tElem.left
        linarith
      rw [this]
      exact continuousAt_const
    convert f_cont
    let F := fun (t : ℝ) ↦ (1-t)*z + t*w
    have F_deriv : ∀ x, derivWithin F I x = f x := by
      intro x
      unfold f
      by_cases xIn : x ∈ I
      . rw [dif_pos xIn]
        apply (lineFun_hasDerivAt z w x).hasDerivWithinAt.derivWithin
        apply UniqueDiffOn.uniqueDiffWithinAt uniqueDiffOn_Icc_zero_one xIn
      . rw [dif_neg xIn]
        apply derivWithin_zero_of_nmem_closure
        convert xIn
        simp
    funext x
    by_cases xIn : x ∈ I
    . rw [← F_deriv, derivWithin_congr]
      intro x xIn
      all_goals (
        rw [Path.extend_extends _ xIn]
        simp [F]
      )
    . unfold f
      rw [dif_neg xIn]
      apply derivWithin_zero_of_nmem_closure
      convert xIn using 1
      simp
}

theorem line_sub_le : ‖(line z w) t - z‖ ≤ ‖w-z‖ := by
  simp [line]
  conv => lhs; arg 1; arg 1; change (1-t)*z +t*w
  have : (1-t)*z+t*w-z = t*(w-z) := by ring
  rw [this, NormedField.norm_mul']
  nth_rw 2 [← one_mul ‖w-z‖]
  gcongr
  simp [abs_le]
  constructor <;> linarith [t.prop.left, t.prop.right]

theorem line_range_memBall (wball : w ∈ Metric.ball z r): Set.range (line z w) ⊆ Metric.ball z r := by
  intro x
  simp
  rintro _ _ rfl
  rw [NormedField.dist_eq]
  apply lt_of_le_of_lt line_sub_le
  exact wball

theorem line_length : (line z w).length = ‖w-z‖ := by
  simp [length, line]
  let f := fun (t : ℝ) ↦ (1-t)*z + t*w
  let g := fun (t : ℝ) ↦ ‖w-z‖
  rw [intervalIntegral.integral_congr (g := g)]
  . simp [g]
  intro x xI
  simp [Curve.deriv_eq (by simp at xI ⊢; constructor <;> linarith)]
  rw [derivWithin_restrict_path_extend_eq (f := f)]
  . simp [f, g]
    rw [(lineFun_hasDerivAt z w x).hasDerivWithinAt.derivWithin]
    exact uniqueDiffOn_Icc_zero_one.uniqueDiffWithinAt (by simp at xI ⊢; constructor <;> linarith)
  . simp [f]
    funext t
    rfl

open Exp

noncomputable def circleMap (c : ℂ₁) (r : ℝ) : ℝ → ℂ₁ := fun φ ↦ c + r*(exp (i*(2*π*φ)))

open Differentiation in
theorem circleMap_hasDerivAt (φ : ℝ) : HasDerivAt (circleMap c r) (2*π*i*r*(exp (i*(2*π*φ)))) φ := by
  rw [← zero_add (2*π*i*_*_)]
  apply HasDerivAt.add (hasDerivAt_const _ _)
  have : 2*π*i*r*(exp (i*(2*π*φ))) = r*(2*π*i*(exp (i*(2*π*φ)))) := by ring
  rw [this]
  apply HasDerivAt.const_mul
  conv =>
    arg 1
    ext ψ
    change (exp ∘ (fun χ ↦ i*(2*π*χ))) ψ
  rw [mul_comm (2*π*i) (exp _)]
  apply exp_deriv.comp
  simp
  conv =>
    arg 1
    ext ψ
    rw [← mul_assoc, mul_comm, mul_comm i]
  nth_rw 2 [← one_mul (2*π*i)]
  apply hasDerivAt_r_inj_c.mul_const

theorem circleMap_deriv : _root_.deriv (circleMap c r) = fun (φ : ℝ) ↦ 2*π*i*r*(exp (i*(2*π*φ))) := by
  funext x
  exact (circleMap_hasDerivAt x).deriv

theorem circleMap_continuous : Continuous (circleMap c r) :=
  continuous_iff_continuousAt.mpr fun z ↦ (circleMap_hasDerivAt z).continuousAt

open scoped unitInterval in
noncomputable def circle (c : ℂ₁) (r : ℝ) : Curve (c + r) (c + r) where
  path := {
    toFun := (I).restrict (circleMap c r)
    continuous_toFun := circleMap_continuous.continuousOn.restrict
    source' := by simp [circleMap, mul_def, ← zero_def, exp_of_zero]
    target' := by
      simp [circleMap, r_inj_c_to_hom]
      have : (2 : ℂ₁)*(π : ℝ) = (2*π : ℝ) := by push_cast; congr
      rw [← r_inj_c_to_hom, this, exp_of_i_two_pi]
      ring
  }
  isDifferentiable := by
    intro x xIn
    apply (@circleMap_hasDerivAt c r x).differentiableAt.differentiableWithinAt.congr
    intro x xIn
    all_goals (
      rw [Path.extend_extends _ xIn]
      congr
    )
  derivContinuous := by
    rw [derivWithin_restrict_path_extend_eq (circleMap c r)]; rotate_left
    . simp
    rw [derivWithin_eq_deriv_restrict_extend (fun x ↦ circleMap_hasDerivAt x)]
    intro x xIn
    rw [continuousWithinAt_iff_continuousAt_restrict _ xIn, restrict_extend_subtype]
    rw [circleMap_deriv, ← continuousWithinAt_iff_continuousAt_restrict _ xIn]
    apply Continuous.continuousWithinAt
    apply continuous_const.mul (exp_continuous.comp _)
    conv => arg 1; ext x; rw [← mul_assoc]
    exact continuous_const.mul r_inj_c_continuous

namespace Contour

noncomputable def joinedLines {n : ℕ} (points : Fin (n+2) → ℂ₁) :
    Contour (points 0) (points (Fin.last _)) where
  n := n
  sources := fun i ↦ points i
  targets := fun i ↦ points (i+1)
  curves := fun i ↦ line (points i) (points (i+1))
  sourceEq := by simp
  targetEq := by
    simp; congr; norm_cast
    rw [Fin.natCast_eq_last]
  adjacent := by simp
