import Mathlib.Data.Real.Sign
import Complex.Basic
import Complex.Norm

namespace Real

noncomputable def sign_nz (r : ℝ) : ℝ := if r ≥ 0 then 1 else -1

namespace Complex₁

open Real

noncomputable def sqrt₁ (z : ℂ₁) : ℂ₁ :=
  ⟨sqrt ((1 / 2) * (z.re + ‖z‖)), z.im.sign_nz * sqrt ((1 / 2) * (-z.re + ‖z‖))⟩

noncomputable def sqrt₂ (z : ℂ₁) : ℂ₁ := -sqrt₁ z

theorem sq_eq (z : ℂ₁) : z^2 = ⟨z.re^2 - z.im^2, 2*z.re*z.im⟩ := by
  ext
  . simp [pow_two]
  . simp [pow_two]
    ring

theorem sqrt_eq_iff_sq_eq (z w : ℂ₁) : z^2 = w ↔ z = sqrt₁ w ∨ z = sqrt₂ w := by
  have rw_sqrt (r : ℝ) : (sqrt 2)⁻¹ * sqrt r = sqrt ((1 / 2) * r) := by
    rw [inv_eq_one_div]
    nth_rw 1 [← sqrt_one]
    rw [← sqrt_div (by norm_num), ← sqrt_mul (by norm_num)]

  have rhsnn₁ : (1/2) * (w.re + ‖w‖) ≥ 0 := by
    have : -w.re ≤ ‖w‖ := le_trans (neg_le_abs _) (Complex₁.abs_re_le_norm _)
    linarith
  have rhsnn₂ : (1/2) * (-w.re + ‖w‖) ≥ 0 := by
    have : w.re ≤ ‖w‖ := le_trans (le_abs_self _) (Complex₁.abs_re_le_norm _)
    linarith

  constructor
  . intro eq

    have eq₁ : z.re^2 - z.im^2 = w.re := by
      apply_fun (·.re) at eq
      simp [sq_eq] at eq
      exact eq

    have eq₂ : 2*z.re*z.im = w.im := by
      apply_fun (·.im) at eq
      simp [sq_eq] at eq
      exact eq

    have eq₃ : ‖w‖ = z.re^2 + z.im^2 := by
      rw [← eq, norm_pow]
      dsimp [Norm.norm, Complex₁.norm]
      rw [sq_sqrt]
      . exact add_nonneg (sq_nonneg _) (sq_nonneg _)

    have re_sq_eq : (|z.re|)^2 = (1/2) * (w.re + ‖w‖) := by linarith [sq_abs z.re]
    have im_sq_eq : (|z.im|)^2 = (1/2) * (-w.re + ‖w‖) := by linarith [sq_abs z.im]
    have re_eq := (Real.sqrt_eq_iff_eq_sq rhsnn₁ (abs_nonneg z.re)).mpr re_sq_eq.symm
    have im_eq := (Real.sqrt_eq_iff_eq_sq rhsnn₂ (abs_nonneg z.im)).mpr im_sq_eq.symm

    rcases lt_trichotomy 0 z.im with zimpos | zimz | zimneg
    . by_cases zresign : (z.re ≥ 0)
      . left
        unfold sqrt₁
        ext <;> dsimp
        . rw [← abs_eq_self.mpr zresign]
          exact re_eq.symm
        . have wimnn : w.im ≥ 0 := by
            rw [← eq₂]
            repeat apply mul_nonneg
            norm_num
            . assumption
            . apply le_of_lt; assumption
          simp [sign_nz, wimnn]
          rw [rw_sqrt, ← abs_eq_self.mpr (le_of_lt zimpos)]
          exact im_eq.symm
      . right
        unfold sqrt₂
        unfold sqrt₁
        have zreneg : z.re < 0 := by linarith
        ext <;> dsimp
        . rw [← neg_eq_iff_eq_neg, ← abs_eq_neg_self.mpr (le_of_lt zreneg)]
          exact re_eq.symm
        . have wimneg : w.im < 0 := by
            rw [← eq₂]
            apply mul_neg_of_neg_of_pos
            apply mul_neg_of_pos_of_neg (by norm_num)
            repeat assumption
          have : ¬w.im ≥ 0 := by linarith
          simp [sign_nz, this]
          rw [rw_sqrt, ← abs_eq_self.mpr (le_of_lt zimpos)]
          exact im_eq.symm

    . have wimz : w.im = 0 := by
        rw [← eq₂, ← zimz]
        linarith
      have wnormre : ‖w‖ = w.re := by
        simp [Norm.norm, Complex₁.norm, wimz]
        rw [sqrt_sq]
        . simp [← eq₁, ← zimz]
          apply sq_nonneg
      by_cases zresign : (z.re ≥ 0)
      . left
        unfold sqrt₁
        ext <;> dsimp
        . rw [← abs_eq_self.mpr zresign]
          exact re_eq.symm
        . simp [wnormre]
          exact zimz.symm
      . right
        unfold sqrt₂
        unfold sqrt₁
        rw [← neg_eq_iff_eq_neg]
        ext <;> dsimp
        . have : z.re < 0 := by linarith
          rw [← abs_eq_neg_self.mpr (le_of_lt this)]
          exact re_eq.symm
        . simp [wnormre]
          exact zimz.symm

    . rcases lt_trichotomy 0 z.re with zrepos | zrez | zreneg
      . left
        unfold sqrt₁
        ext <;> dsimp
        . rw [← abs_eq_self.mpr (le_of_lt zrepos)]
          exact re_eq.symm
        . have wimn : w.im < 0 := by
            rw [← eq₂]
            apply mul_neg_of_pos_of_neg
            apply mul_pos (by norm_num)
            repeat assumption
          have : ¬w.im ≥ 0 := by linarith
          simp [sign_nz, this]
          rw [rw_sqrt, ← neg_eq_iff_eq_neg, ← abs_eq_neg_self.mpr (le_of_lt zimneg)]
          exact im_eq.symm
      . have wimz : w.im = 0 := by
          rw [← eq₂, ← zrez]
          linarith
        have wnormre : ‖w‖ = -w.re := by
          simp [Norm.norm, Complex₁.norm, wimz]
          rw [sqrt_sq_eq_abs, abs_eq_neg_self.mpr _]
          . simp [← eq₁, ← zrez]
            apply sq_nonneg
        right
        unfold sqrt₂
        unfold sqrt₁
        ext <;> dsimp
        . simp [← zrez, wnormre]
        . have wimnn : w.im ≥ 0 := by linarith
          simp [sign_nz, wimnn]
          rw [rw_sqrt, ← neg_eq_iff_eq_neg, ← abs_eq_neg_self.mpr (le_of_lt zimneg)]
          exact im_eq.symm
      . right
        unfold sqrt₂
        unfold sqrt₁
        rw [← neg_eq_iff_eq_neg]
        ext <;> dsimp
        . rw [← abs_eq_neg_self.mpr (le_of_lt zreneg)]
          exact re_eq.symm
        . have wimnn : w.im ≥ 0 := by
            rw [← eq₂]
            apply mul_nonneg_of_nonpos_of_nonpos
            apply mul_nonpos_of_nonneg_of_nonpos (by norm_num)
            repeat' (apply le_of_lt; assumption)
          simp [sign_nz, wimnn]
          rw [rw_sqrt, ← abs_eq_neg_self.mpr (le_of_lt zimneg)]
          exact im_eq.symm

  . have sign_nz_sq (r : ℝ) : (sign_nz r)^2 = 1 := by
      by_cases rsign : (r ≥ 0) <;> simp [sign_nz, rsign]

    have case₁ (eq : z^2 = (sqrt₁ w)^2) : z^2 = w := by
      rw [eq]
      unfold sqrt₁
      simp [sq_eq]
      repeat rw [rw_sqrt]
      ext <;> dsimp
      . rw [mul_pow, sign_nz_sq, one_mul, sq_sqrt, sq_sqrt]
        ring
        repeat' assumption
      . rw [mul_assoc, ← mul_assoc (sqrt _), mul_comm _ (sign_nz _), mul_assoc (sign_nz _), ← sqrt_mul rhsnn₁]
        have : (1/2) * (w.re + ‖w‖) * ((1/2) * (-w.re + ‖w‖)) = (1/4) * (‖w‖^2 - w.re^2) := by ring
        rw [this, sqrt_mul (by norm_num)]
        have : sqrt (1/4) = 1/2 := by
          apply (Real.sqrt_eq_iff_eq_sq (by norm_num) (by norm_num)).mpr
          norm_num
        rw [this, ← mul_assoc (sign_nz _), mul_comm (sign_nz _), ← mul_assoc, ← mul_assoc, mul_one_div_cancel (by norm_num), one_mul]
        simp [Norm.norm, Complex₁.norm, sq_sqrt (add_nonneg (sq_nonneg _) (sq_nonneg _))]
        by_cases wimsign : (w.im ≥ 0)
        . simp [sign_nz, wimsign]
        . have : w.im ≤ 0 := by linarith
          simp [sign_nz, wimsign, sqrt_sq_eq_abs, abs_eq_neg_self.mpr this]

    rintro (eq | eq)
    . apply_fun (·^2) at eq
      exact case₁ eq
    . have : z^2 = (sqrt₁ w)^2 := by simp [eq, sqrt₂]
      exact case₁ this
