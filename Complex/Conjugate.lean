import Complex.Basic

namespace Complex₁

open Function Real Complex₁

theorem r_auto_unique (f : ℝ ≃+* ℝ) : f = RingHom.id ℝ := by
  have monotone : StrictMono f := by
    intro x x' lt
    let ysq := x'-x
    have ysq_pos : ysq > 0 := sub_pos.mpr lt
    let y := Real.sqrt ysq
    have : f x' - f x > 0 := by
      calc
        f x' - f x = f (x'-x) := (map_sub _ _ _).symm
        _ = f ysq := rfl
        _ = (f (y^2)) := by
          rw [sq_sqrt]
          linarith
        _ = (f y)^2 := map_pow _ _ _
        _ > 0 := by
            apply sq_pos_of_ne_zero
            intro contra
            rw [← f.map_zero] at contra
            apply f.injective at contra
            have : y > 0 := by apply sqrt_pos_of_pos ysq_pos
            linarith
    linarith

  ext x; simp
  apply eq_of_forall_dist_le
  intro ε εpos
  obtain ⟨up, ⟨xltup, uplt⟩⟩ := exists_rat_btwn (by linarith : x < x + ε/2)
  obtain ⟨down, ⟨ltdown, downltx⟩⟩ := exists_rat_btwn (by linarith : x > x - ε/2)
  apply monotone at xltup
  rw [map_ratCast] at xltup
  apply monotone at downltx
  rw [map_ratCast] at downltx
  apply abs_le.mpr
  constructor <;> linarith

def conj (z : ℂ₁) : ℂ₁ := ⟨z.re, -z.im⟩

@[simp]
theorem conj_re (z : ℂ₁) : z.conj.re = z.re := by rfl

@[simp]
theorem conj_im (z : ℂ₁) : z.conj.im = -z.im := by rfl

@[simp]
theorem conj_decomp (a b : ℝ) : (a + b*i).conj = a - b*i := by
  simp [complex_decomp']
  ext <;> simp [conj, i]

theorem add_conj_self (z : ℂ₁) : z + z.conj = 2*z.re := by
  simp [conj]
  ext <;> (simp; norm_cast)
  . ring
  . left; rfl

theorem sub_conj_self (z : ℂ₁) : z - z.conj = 2*i*z.im := by
  simp [conj]
  ext <;> (simp [i]; norm_cast)
  . left; rfl
  . ring

theorem conj_conj (z : ℂ₁) : z.conj.conj = z := by
  ext
  . rfl
  . simp

theorem conj_mul (z w : ℂ₁) : (z*w).conj = z.conj * w.conj := by
  simp [conj, mul_def]
  ring

theorem conj_add (z w : ℂ₁) : (z+w).conj = z.conj + w.conj := by
  simp [conj, add_def]
  ring

def conj_auto : ℂ₁ ≃+* ℂ₁ where
  toFun := conj
  invFun := conj
  left_inv := conj_conj
  right_inv := conj_conj
  map_mul' := conj_mul
  map_add' := conj_add

theorem conj_eq_conj_auto : conj = conj_auto := rfl

theorem r_fixpoint_set_of_conj : ∀ (z : ℂ₁), z.conj = z ↔ z ∈ ℝ' := by
  intro z
  constructor
  . intro conj_eq
    apply_fun (·.im) at conj_eq
    simp [conj] at conj_eq
    use z.re
    ext
    . rfl
    . simp
      exact (CharZero.eq_neg_self_iff.mp conj_eq.symm).symm
  . rintro ⟨r', req⟩
    rw [← req]
    ext
    . rfl
    . simp [conj]

theorem c_auto_unique (f : ℂ₁ →+* ℂ₁) (fixes_r : ∀ r ∈ ℝ', f r = r) : f = RingHom.id ℂ₁ ∨ f = conj := by
  have fi_eq : f i = i ∨ f i = -i := by
    have : f = f.toFun := rfl
    rw [← roots_of_neg_one, pow_two, this, ← f.map_mul', ← pow_two]
    simp
  rcases fi_eq with eq | eq
  . left
    ext x : 1
    rw [complex_decomp x]
    simp [eq]
    rw [fixes_r, fixes_r] <;> apply r_cast_mem
  . right
    ext x : 1
    rw [complex_decomp x]
    simp [eq]
    rw [fixes_r, fixes_r]
    ring
    repeat apply r_cast_mem
