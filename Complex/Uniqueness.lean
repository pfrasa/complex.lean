import Mathlib.LinearAlgebra.Dimension.Finrank
import Mathlib.LinearAlgebra.LinearIndependent
import Complex.Basic

namespace Complex₁.Uniqueness

open Module FiniteDimensional LinearIndependent

protected lemma li_base_extension [Field F] [Algebra ℝ F] (z : F) (notinr : ∀r : ℝ, r•1 ≠ z) :
    LinearIndependent ℝ ((fun x ↦ match x with | 0 => 1 | 1 => z) : (Fin 2) → F) := by
  apply Fintype.linearIndependent_iff.mpr
  intro g sumeqz
  simp at sumeqz
  contrapose! sumeqz
  rcases sumeqz with ⟨i, gieq⟩
  by_cases g1z : (g 1 = 0)
  . fin_cases i <;> simp at gieq
    . simp [g1z]
      exact gieq
    . contradiction
  . intro contra
    have : z = (g 0 / -(g 1)) • 1 := by
      rw [div_eq_inv_mul, mul_smul]
      symm
      rw [inv_smul_eq_iff₀ (neg_ne_zero.mpr g1z), neg_smul, eq_neg_iff_add_eq_zero]
      exact contra
    contrapose! notinr
    use (g 0 / -g 1)
    exact this.symm

lemma c_unique_2d [Field F] [Algebra ℝ F] [Module.Finite ℝ F] (dim2 : finrank ℝ F = 2) : Nonempty (F ≃+* ℂ₁) := by
  have ex_w : ∃w : F, ∀r : ℝ, r•1 ≠ w := by
    have b := finBasisOfFinrankEq ℝ F dim2
    by_cases in_r : (∀r : ℝ, r•1 ≠ b 0)
    . use b 0
    . by_cases in_r' : (∀ r : ℝ, r•1 ≠ b 1)
      . use b 1
      . exfalso
        have not_li : ¬LinearIndependent ℝ b := by
          push_neg at *
          obtain ⟨r1, r1eq⟩ := in_r
          obtain ⟨r2, r2eq⟩ := in_r'
          apply Fintype.not_linearIndependent_iff.mpr
          use (fun x ↦ match x with | 0 => r2 | 1 => -r1)
          simp
          constructor
          . rw [← r1eq, ← r2eq, ← smul_assoc, ← smul_assoc, ← sub_eq_add_neg, ← sub_smul]
            simp
            ring
          . use 0
            simp
            intro contra
            rw [contra, zero_smul] at r2eq
            exact b.ne_zero 1 r2eq.symm
        exact not_li b.linearIndependent
  obtain ⟨w, weq⟩ := ex_w

  let b : (Fin 2) → F := fun x ↦ match x with | 0 => 1 | 1 => w
  have li_b : LinearIndependent ℝ b := Complex₁.Uniqueness.li_base_extension w weq
  have b_card_eq : Fintype.card (Fin 2) = finrank ℝ F := by simp; symm; assumption

  let basis_b := basisOfLinearIndependentOfCardEqFinrank li_b b_card_eq
  have basis_b_0 : basis_b 0 = 1 := by apply Basis.mk_apply
  have basis_b_1 : basis_b 1 = w := by apply Basis.mk_apply

  let a := (basis_b.repr (w^2)) 0
  let b := (basis_b.repr (w^2)) 1
  have wsq_decomp : w^2 = a•1 + b•w := by
    apply Basis.ext_elem basis_b
    intro i
    fin_cases i <;> (
      simp
      rw [← basis_b_0, ← basis_b_1, Basis.repr_self_apply, Basis.repr_self_apply, basis_b_1]
      simp [a, b]
    )

  let u : F := w - ((1/2)*b)•1
  have unotr : ∀r : ℝ, r•1 ≠ u := by
    by_contra contra
    push_neg at contra
    rcases contra with ⟨r, req⟩
    have : ¬(∀r : ℝ, r•1 ≠ w) := by
      push_neg
      use r + (1/2)*b
      rw [add_smul, req, sub_add_cancel]
    exact this weq

  let c := a + ((1/2)*b)^2
  have usqr : c•1 = u^2 := by
    rw [pow_two u, sub_mul, mul_sub, mul_sub, ← pow_two w, wsq_decomp]
    simp
    have : a•1 + b•w - (2⁻¹*b)•w -((2⁻¹*b)•w - (2⁻¹*b)•(2⁻¹*b)•1) = (a•1 + (2⁻¹*b)•(2⁻¹*b)•1) + (b•w - (2⁻¹*b)•w - (2⁻¹*b)•w) := by ring
    rw [this, ← sub_smul, ← sub_smul, sub_sub, ← add_mul]
    have : (2⁻¹+2⁻¹ : ℝ) = 1 := by norm_num
    rw [this, one_mul, sub_self, zero_smul, add_zero, add_smul, pow_two, mul_smul]
    simp

  have usqneg : c < 0 := by
    by_cases cpos : (c ≥ 0)
    . contrapose! unotr
      have : u^2 - c•1 = 0 := sub_eq_zero_of_eq usqr.symm
      let sqrt_r : F := (Real.sqrt c)•1
      have : (u + sqrt_r)*(u - sqrt_r) = 0 := by
        have sqeq : (Real.sqrt c)^2 = c := Real.sq_sqrt cpos
        rw [add_mul, mul_sub, mul_sub sqrt_r, sub_add, ← sub_add (u * sqrt_r), mul_comm sqrt_r u, sub_self]
        rw [zero_add, ← pow_two, smul_mul_smul_comm, ← pow_two, one_mul, sqeq]
        exact this
      cases' mul_eq_zero.mp this with h h
      . use -(Real.sqrt c)
        symm
        rw [neg_smul, eq_neg_iff_add_eq_zero]
        exact h
      . use Real.sqrt c
        symm
        exact sub_eq_zero.mp h
    . exact lt_of_not_ge cpos

  let d := Real.sqrt (-(c⁻¹))
  have dsqeq : d^2 = -(c⁻¹) := by
    rw [Real.sq_sqrt]
    apply neg_nonneg.mpr
    apply inv_nonpos.mpr
    exact le_of_lt usqneg

  let v := d•u
  have vsq : v^2 = -1 := by
    rw [pow_two, smul_mul_smul_comm, ← pow_two, ← pow_two]
    rw [← usqr, dsqeq, neg_smul, smul_comm, smul_inv_smul₀]
    exact ne_of_lt usqneg
  have vnotr : ∀r : ℝ, r•1 ≠ v := by
    have : (1 : F) = (1 : ℝ)•1 := by simp
    intro r eq
    apply_fun (·^2) at eq
    rw [vsq, pow_two, smul_mul_smul_comm, one_mul, ← pow_two] at eq
    apply add_eq_zero_iff_eq_neg.mpr at eq
    nth_rw 2 [this] at eq
    rw [← add_smul] at eq
    rcases smul_eq_zero.mp eq with h | h
    . linarith [sq_nonneg r]
    . norm_num at h

  let b' : (Fin 2) → F := fun x ↦ match x with | 0 => 1 | 1 => v
  have li_b' : LinearIndependent ℝ b' := Complex₁.Uniqueness.li_base_extension v vnotr
  let basis_b' := basisOfLinearIndependentOfCardEqFinrank li_b' b_card_eq
  have basis_b'_0 : basis_b' 0 = 1 := by apply Basis.mk_apply
  have basis_b'_1 : basis_b' 1 = v := by apply Basis.mk_apply

  have decomp (z : F) : z = ((basis_b'.repr z) 0)•1 + ((basis_b'.repr z) 1)•v := by
    apply Basis.ext_elem basis_b'
    intro i
    fin_cases i <;> (
      simp
      rw [← basis_b'_0, ← basis_b'_1, Basis.repr_self_apply, Basis.repr_self_apply]
      simp
    )

  let iso : F ≃+* ℂ₁ := {
    toFun := fun w ↦ ⟨(basis_b'.repr w) 0, (basis_b'.repr w) 1⟩
    invFun := fun ⟨a, b⟩ ↦  a•1 + b•v
    left_inv := by
      intro z
      simp
      symm
      apply decomp
    right_inv := by
      intro z
      simp
      ext <;> (
        simp
        rw [← basis_b'_0, ← basis_b'_1, Basis.repr_self_apply, Basis.repr_self_apply]
        simp
      )
    map_mul' := by
      have repr0 : basis_b'.repr (basis_b' 0) 0 = 1 := by rw [Basis.repr_self_apply]; simp
      have repr1 : basis_b'.repr (basis_b' 1) 0 = 0 := by rw [Basis.repr_self_apply]; simp
      have repr2 : basis_b'.repr (basis_b' 0) 1 = 0 := by rw [Basis.repr_self_apply]; simp

      intro x y
      ext <;> (
        simp
        rw [decomp x, decomp y, add_mul, mul_add, mul_add]
        simp
        rw [← basis_b'_0, ← basis_b'_1]
        simp [repr0, repr1]
        rw [basis_b'_1, ← pow_two, vsq]
        simp
        rw [← basis_b'_0]
      )
      . rw [repr0]
        ring
      . rw [repr2]
        ring

    map_add' := by
      intro x y
      ext <;> simp
  }

  exact Nonempty.intro iso
