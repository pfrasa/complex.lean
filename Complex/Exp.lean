import Mathlib.Data.Nat.Factorial.Basic
import General.PowerSeries
import General.Topology
import Complex.Norm
import Complex.Topology
import Complex.PowerSeries

-- Exp function

namespace Complex₁.Exp

open Nat General General.Series General.PowerSeries Complex₁.PowerSeries

@[reducible]
noncomputable def general_exp_powerSeries (F : Type*) [Field F] := General.PowerSeries.mk (fun n ↦ 1/((n !) : F)) 0

@[reducible]
noncomputable def exp_powerSeries := general_exp_powerSeries ℂ₁
theorem exp_powerSeries_def (z : ℂ₁) : exp_powerSeries.series_at z = (fun n ↦ 1/((n !) : ℂ₁) * z^n) := by simp

@[reducible]
noncomputable def re_exp_powerSeries := general_exp_powerSeries ℝ
theorem re_exp_powerSeries_def (x : ℝ) : re_exp_powerSeries.series_at x = (fun n ↦ 1/((n !) : ℝ) * x^n) := by simp

lemma general_exp_powerSeries_ratio_le [NontriviallyNormedField F] (z : F) (nat_norm_eq : ∀ n : ℕ, ‖(n : F)‖ = n) :
    ∀ n ≥ ⌈2*‖z‖-1⌉₊, ‖(general_exp_powerSeries F).series_at z (n+1) / (general_exp_powerSeries F).series_at z n‖ ≤ 1/2 := by
  intro n ngt
  simp only [exp_powerSeries, PowerSeries.series_at, sub_zero]
  rw [pow_succ, mul_comm _ z, ← mul_assoc, mul_div_mul_comm]
  by_cases zz : z = 0
  . simp [zz]
  rw [div_self, mul_one, mul_comm, mul_one_div, div_div, mul_one_div, factorial]
  push_cast
  rw [mul_div_assoc, div_self, mul_one, norm_div]
  apply (div_le_div_iff₀ _ _).mpr
  . norm_cast
    rw [one_mul, mul_comm, nat_norm_eq]
    push_cast
    apply (OrderedSub.tsub_le_iff_right _ _ _).mp
    exact (FloorSemiring.gc_ceil _ _).mp ngt
  . norm_cast
    rw [nat_norm_eq]
    norm_cast
    omega
  . norm_num
  . apply norm_eq_zero.mpr.mt
    rw [nat_norm_eq]
    norm_cast
    exact factorial_ne_zero n
  . exact pow_ne_zero _ zz

lemma exp_powerSeries_ratio_le (z : ℂ₁) :
    ∀ n ≥ ⌈2*‖z‖-1⌉₊, ‖exp_powerSeries.series_at z (n+1) / exp_powerSeries.series_at z n‖ ≤ 1/2 :=
  general_exp_powerSeries_ratio_le _ (by intros; norm_cast)

lemma re_exp_powerSeries_ratio_le (x : ℝ) :
    ∀ n ≥ ⌈2*‖x‖-1⌉₊, ‖re_exp_powerSeries.series_at x (n+1) / re_exp_powerSeries.series_at x n‖ ≤ 1/2 :=
  general_exp_powerSeries_ratio_le _ (by simp)

lemma general_exp_absolutelyConvergent [NontriviallyNormedField F] [CompleteSpace F] (z : F) (nat_norm_eq : ∀ n : ℕ, ‖(n : F)‖ = n) :
    AbsolutelyConvergent ((general_exp_powerSeries F).series_at z) := by
  by_cases zz : z = 0
  . use ‖(general_exp_powerSeries F).coeff 0‖
    rw [zz]
    apply powerSeries_norm_converges_to_at_center

  apply absolutely_convergent_by_ratio_test ⌈2*‖z‖-1⌉₊ (q := 1/2) (by norm_num) (by norm_num) _
    (general_exp_powerSeries_ratio_le z nat_norm_eq)

  intro n _
  simp
  push_neg
  constructor
  . apply norm_eq_zero.mpr.mt
    rw [nat_norm_eq]
    norm_cast
    exact factorial_ne_zero n
  . intro zdist
    contradiction

theorem exp_absolutelyConvergent (z : ℂ₁) : AbsolutelyConvergent (exp_powerSeries.series_at z) :=
  general_exp_absolutelyConvergent z (by intros; norm_cast)

theorem re_exp_absolutelyConvergent (x : ℝ) : AbsolutelyConvergent (re_exp_powerSeries.series_at x) :=
  general_exp_absolutelyConvergent x (by simp)

open ENNReal in
theorem exp_radius : RadiusOfConvergence exp_powerSeries = ∞ := by
  apply powerSeries_radius_infty_of_everywhere_convergent
  intro z
  exact (exp_absolutelyConvergent z).convergent

noncomputable def exp (z : ℂ₁) := ∑ᵢ exp_powerSeries.series_at z

theorem exp_def (z : ℂ₁) : exp z = ∑ᵢ fun n ↦ 1/((n !) : ℂ₁) * z^n := by simp [exp]

theorem exp_of_zero : exp 0 = 1 := by
  apply SeriesConvergesTo.get_isum
  have : exp_powerSeries.coeff 0 = 1 := by simp
  rw [← this]
  apply powerSeries_converges_to_at_center

open General.Series.Multiplication General.Algebra in
theorem exp_add : exp (z + w) = exp z * exp w := by
  convert cauchyProd_get_isum _ _ (exp_absolutelyConvergent z) (exp_absolutelyConvergent w)
  dsimp [exp]
  congr
  funext n
  simp only [CauchyProd, exp_powerSeries, series_at, sub_zero]
  have funeq : (fun (k : Fin (n+1)) ↦ 1/(n-k)! * z^(n-k)*(1/k ! * w^(k : ℕ))) = fun (k : Fin (n+1)) ↦ (Nat.choose n k * z^(n-k) * w^(k : ℕ))*(1/n !) := by
    funext k
    rw [Nat.choose_eq_factorial_div_factorial, ← mul_assoc]
    nth_rw 2 [mul_comm]
    rw [← mul_assoc]
    nth_rw 4 [mul_comm]
    rw [← mul_assoc]
    congr 1
    rw [← mul_assoc]
    congr 1
    rw [one_div_mul_one_div, nat_cast_div, div_mul_div_cancel₀]
    norm_cast
    . rw [nat_cast_ne_zero]
      exact factorial_ne_zero n
    . apply factorial_mul_factorial_dvd_factorial (Fin.is_le k)
    . exact Fin.is_le k
  rw [Finset.sum_range, funeq, ← Finset.sum_mul, mul_comm, binomial_theorem, Finset.sum_range]

lemma exp_mul_exp_neg (z : ℂ₁) : exp z * exp (-z) = 1 := by
  rw [← exp_add, add_neg_cancel]
  exact exp_of_zero

theorem exp_ne_zero (z : ℂ₁) : exp z ≠ 0 := by
  intro eqz
  have := exp_mul_exp_neg z
  rw [eqz, zero_mul] at this
  norm_num at this

theorem exp_inv (z : ℂ₁) : (exp z)⁻¹ = exp (-z) := by
  apply inv_eq_of_mul_eq_one_right
  apply exp_mul_exp_neg

open Multiplicative in
noncomputable def exp_hom : Multiplicative ℂ₁ →* ℂ₁ˣ where
  toFun z := by
    refine' ⟨exp (toAdd z), (exp (toAdd z))⁻¹, mul_inv_cancel₀ (exp_ne_zero _), _⟩
    rw [mul_comm]
    apply mul_inv_cancel₀ (exp_ne_zero _)
  map_one' := by
    simp [exp_of_zero]
    rfl
  map_mul' := by
    intro z w
    simp [exp_add]
    rfl

@[reducible]
noncomputable def exp_powerSeries_expansion_at (a : ℂ₁) := General.PowerSeries.mk (fun n ↦ (exp a)*(1/((n !) : ℂ₁))) a

theorem exp_equals_powerSeries_expansion_at (a : ℂ₁) : exp = fun z ↦ ∑ᵢ (exp_powerSeries_expansion_at a).series_at z := by
  funext z
  unfold series_at
  simp only [exp_powerSeries_expansion_at, mul_assoc]
  have : SeriesConvergent (fun n ↦ 1 / ((n !) : ℂ₁) * (z-a)^n) := by
    rw [← exp_powerSeries_def]
    apply AbsolutelyConvergent.convergent
    apply exp_absolutelyConvergent
  rw [isum_const_mul_of_series_convergent this, ← exp_def (z-a), sub_eq_neg_add, ← exp_add]
  ring_nf

theorem exp_powerSeries_expansion_at_convergesTo (a z : ℂ₁) :
    SeriesConvergesTo ((exp_powerSeries_expansion_at a).series_at z) (exp z) := by
  suffices SeriesConvergent ((exp_powerSeries_expansion_at a).series_at z) by
    obtain ⟨w, convto_w⟩ := this
    have := convto_w.get_isum
    have eq := exp_equals_powerSeries_expansion_at a
    apply_fun (fun f ↦ f z) at eq
    simp at eq
    rw [← eq] at this
    rwa [this]
  by_contra div
  have expz : (∑ᵢ (exp_powerSeries_expansion_at a).series_at z) = 0 := SeriesDivergent.get_isum div
  have expnz : exp z ≠ 0 := exp_ne_zero _
  rw [exp_equals_powerSeries_expansion_at] at expnz
  contradiction

theorem exp_conj (z : ℂ₁) : exp z.conj = (exp z).conj := by
  apply powerSeries_conj_isum
  . intro n
    use (n !)⁻¹
    ext <;> simp [pow_two]
  . use 0
    norm_cast

theorem exp_real_pos (x : ℝ) : exp x ∈ (ℝpos' : Set ℂ₁) := by
  suffices exp x ∈ (ℝnn' : Set ℂ₁) by
    rw [rpos_mem_of_rnn_nz]
    exact ⟨this, exp_ne_zero _⟩

  wlog xnn : x ≥ 0
  . simp
    rw [← inv_inv (exp _), exp_inv, rnn_mem_iff]
    have negxnn : -x ≥ 0 := le_of_lt <| neg_pos_of_neg <| lt_of_not_ge xnn
    have negin := this _ negxnn
    simp [rnn_mem_iff] at negin ⊢
    constructor
    . left
      norm_cast
      exact negin.left
    . apply div_nonneg
      . norm_cast
        exact negin.right
      . exact add_nonneg (sq_nonneg _) (sq_nonneg _)

  obtain ⟨y, convto⟩ := (exp_absolutelyConvergent x).convergent
  rw [exp, convto.get_isum]
  refine' rnn_closed.isSeqClosed _ convto
  intro n
  apply sum_mem
  intro k _
  rw [rnn_mem_iff]
  simp
  constructor
  . right
    norm_cast
  . norm_cast
    apply mul_nonneg _ (pow_nonneg xnn _)
    apply div_nonneg (cast_nonneg _) (cast_nonneg _)

noncomputable def re_exp (x : ℝ) := (exp x).re

theorem re_exp_eq_exp (x : ℝ) : re_exp x = exp x := by
  ext
  . simp [re_exp]
  . simp
    exact Eq.symm <| And.left <| rnn_mem_iff.mp <| And.left <| rpos_mem_of_rnn_nz.mp (exp_real_pos x)

lemma re_exp_powerSeries_at_eq_exp_powerSeries_at (x : ℝ) : r_inj_c ∘ re_exp_powerSeries.series_at x = exp_powerSeries.series_at x := by
  funext n
  unfold series_at
  simp
  push_cast
  congr

theorem re_exp_eq_powerSeries (x : ℝ) : re_exp x = ∑ᵢ re_exp_powerSeries.series_at x := by
  apply_fun r_inj_c
  . rw [re_exp_eq_exp, exp, ← re_exp_powerSeries_at_eq_exp_powerSeries_at]
    obtain ⟨y, convto_y⟩ := (re_exp_absolutelyConvergent x).convergent
    rw [convto_y.get_isum]
    apply SeriesConvergesTo.get_isum
    unfold series_at at convto_y ⊢
    unfold SeriesConvergesTo at convto_y ⊢
    convert r_inj_c_continuous.seqContinuous convto_y
    dsimp
    rw [r_inj_c_to_hom, map_sum]
  . rw [r_inj_c_to_hom]
    exact r_c_hom_injective

theorem re_exp_pos (x : ℝ) : re_exp x > 0 := by
  rw [re_exp]
  have := rpos_mem_of_rnn_nz.mp (exp_real_pos x)
  simp [rnn_mem_iff] at this
  apply lt_of_le_of_ne this.left.right
  . intro rez
    have expz : exp x = 0 := by
      ext
      . exact rez.symm
      . simp
        exact this.left.left
    exact this.right expz

theorem re_exp_of_zero : re_exp 0 = 1 := by
  apply r_eq_by_c_eq
  rw [re_exp_eq_exp]
  exact exp_of_zero

theorem re_exp_add : re_exp (x + y) = re_exp x * re_exp y := by
  apply r_eq_by_c_eq
  push_cast
  simp_rw [re_exp_eq_exp]
  push_cast
  exact exp_add

theorem re_exp_inv (x : ℝ) : (re_exp x)⁻¹ = re_exp (-x) := by
  apply r_eq_by_c_eq
  push_cast
  simp_rw [re_exp_eq_exp]
  push_cast
  exact exp_inv _

theorem norm_exp_eq_exp_re (z : ℂ₁) : ‖exp z‖ = re_exp z.re := by
  suffices ‖exp z‖^2 = (re_exp z.re)^2 by
    rcases eq_or_eq_neg_of_sq_eq_sq _ _ this with pos | neg
    . exact pos
    . have norm_nn : ‖exp z‖ ≥ 0 := norm_nonneg _
      have neg_exp_neg : -re_exp z.re < 0 := neg_neg_of_pos (re_exp_pos _)
      linarith
  apply_fun r_c_hom using r_c_hom_injective
  rw [← r_inj_c_to_hom]
  push_cast
  rw [norm_def, norm_sq_eq_prod_conj', ← exp_conj, ← exp_add, add_conj_self, two_mul, exp_add, ← pow_two, re_exp_eq_exp]

theorem norm_exp_one_of_imaginary {z : ℂ₁} (zim : z.re = 0) : ‖exp z‖ = 1 := by
  have : ((0 : ℝ) : ℂ₁) = 0 := rfl
  rw [norm_exp_eq_exp_re, zim, re_exp, this, exp_of_zero]
  simp

noncomputable def e := exp 1

theorem exp_nat_eq_e_pow (n : ℕ) : exp n = e^n := by
  induction' n with n' ih
  . simp [exp_of_zero]
  . push_cast
    rw [exp_add, pow_add, ih, e, pow_one]

lemma exp_series_from_norm_le (z : ℂ₁) {m : ℕ} (mnz : m ≠ 0) (mge : m ≥ ⌈2*‖z‖-1⌉₊) :
    ‖∑ᵢfrom m, exp_powerSeries.series_at z‖ ≤ 2*‖z‖^m/(m)! := by
  by_cases zz : z = 0
  . convert div_nonneg _ _
    . rw [norm_eq_zero]
      apply SeriesConvergesTo.get_isum
      apply tendsto_atTop_of_eventually_const (i₀ := 0)
      intro i _
      apply Finset.sum_eq_zero
      intro k _
      simp
      right
      exact ⟨zz, fun mz _ ↦ mnz mz⟩
    . exact inferInstance
    . exact inferInstance
    . exact inferInstance
    . apply mul_nonneg (by norm_num) (pow_nonneg (norm_nonneg _) _)
    . simp

  convert norm_le_by_ratio_test' m (q := 1/2) (by norm_num) (by norm_num) _ _ m (le_refl _) using 1
  . have : 1 - (1/2) = (1/2 : ℝ) := by norm_num
    simp only [exp_powerSeries, series_at, sub_zero, this, div_div_eq_mul_div, div_one, NormedField.norm_mul', norm_div, norm_pow]
    norm_cast
    ring
  . have : CompleteSpace ℂ₁ := inferInstance
    exact this
  . intro n _
    simp
    push_neg
    constructor
    . intro eqz
      apply_fun (·.re) at eqz
      simp at eqz
      exact (factorial_ne_zero _) eqz
    . intro zz'
      contradiction
  . intro n nge
    apply exp_powerSeries_ratio_le
    linarith

lemma norm_exp_minus_one_le {z : ℂ₁} (norm_le_one : ‖z‖ ≤ 1): ‖exp z - 1‖ ≤ 2*‖z‖ := by
  convert exp_series_from_norm_le z (m := 1) (by norm_num) _ using 1
  . rw [isum_from_of_series_convergent (exp_absolutelyConvergent _).convergent]
    congr
    ext <;> simp
  . simp
  . have : 1+1 = (2 : ℝ) := by norm_num
    simp [this, norm_le_one]

open Filter in
@[continuity]
theorem exp_continuous : Continuous exp := by
  apply SeqContinuous.continuous
  intro s z sttz
  have : exp ∘ s = fun n ↦ exp (s n - z) * exp z := by
    funext n
    rw [← exp_add]
    simp
  rw [this]
  convert Tendsto.mul_const (exp z) (c := 1) _ using 1
  . rw [one_mul]
  . apply Metric.tendsto_atTop.mpr
    intro ε ε0
    obtain ⟨N, sn_z_dist_le⟩ := Metric.tendsto_atTop.mp sttz (min 1 (ε/2)) (lt_min (by norm_num) (div_pos ε0 (by norm_num)))
    use N
    intro n nge
    have lt_min := sn_z_dist_le n nge
    have norm_le_one : ‖s n - z‖ ≤ 1 := by
      apply le_of_lt
      exact (lt_min_iff.mp lt_min).left
    rw [dist_eq_norm]
    apply lt_of_le_of_lt
    apply norm_exp_minus_one_le norm_le_one
    apply (lt_div_iff₀' (by norm_num)).mp
    exact (lt_min_iff.mp lt_min).right

@[continuity]
theorem re_exp_continuous : Continuous re_exp := re_continuous.comp <| exp_continuous.comp r_inj_c_continuous

lemma re_exp_gt_one_plus_of_pos (pos : x > 0) : re_exp x > 1 + x := by
  obtain ⟨y, convto_y⟩ := (re_exp_absolutelyConvergent x).convergent
  rw [re_exp_eq_powerSeries, convto_y.get_isum]
  have : 1 + x + 2⁻¹*x^2 > 1 + x := by simp; linarith [sq_pos_of_pos pos]
  apply lt_of_lt_of_le this
  apply ge_of_tendsto convto_y
  rw [Filter.eventually_atTop]
  use 2
  intro n nge
  have : n+1 = 3+(n-2) := by omega
  rw [this, Finset.sum_range_add, Finset.sum_range_succ, Finset.sum_range_succ]
  simp [series_at]
  apply Finset.sum_nonneg
  intro k _
  apply mul_nonneg
  . rw [inv_nonneg]
    norm_num
  . exact pow_nonneg (le_of_lt pos) _

lemma re_exp_lt_one_div_one_sub_of_neg (neg : x < 0) : re_exp x < 1/(1-x) := by
  nth_rw 1 [← neg_neg x]
  rw [← re_exp_inv, inv_eq_one_div, sub_eq_add_neg]
  apply one_div_lt_one_div_of_lt _ (re_exp_gt_one_plus_of_pos (neg_pos_of_neg neg))
  linarith

lemma re_exp_gt_one_of_pos (pos : x > 0) : re_exp x > 1 := lt_trans (by linarith) (re_exp_gt_one_plus_of_pos pos)

theorem re_exp_mono : StrictMono re_exp := by
  intro x y gt
  rw [← sub_add_cancel y x, re_exp_add]
  apply (lt_mul_iff_one_lt_left (re_exp_pos _)).mpr
  apply re_exp_gt_one_of_pos
  exact sub_pos_of_lt gt

theorem re_exp_injective : Function.Injective re_exp := re_exp_mono.injective

theorem re_exp_bij_on_rpos : Set.BijOn re_exp Set.univ {x : ℝ | x > 0} := by
  refine' ⟨fun x _ ↦ re_exp_pos x, re_exp_injective.injOn, _⟩
  intro y ypos
  rw [Set.image_univ]
  apply mem_range_of_exists_le_of_exists_ge re_exp_continuous
  . by_cases yge1 : y ≥ 1
    . use 0
      rw [re_exp_of_zero]
      exact yge1
    . use 1-(1/y)
      apply le_of_lt
      convert re_exp_lt_one_div_one_sub_of_neg _
      . simp
      . apply sub_neg_of_lt
        apply one_lt_one_div
        . exact ypos
        . exact lt_of_not_ge yge1

  . use y
    apply le_of_lt
    apply lt_trans _ (re_exp_gt_one_plus_of_pos ypos)
    linarith

noncomputable def re_log := re_exp.invFun

theorem re_log_re_exp : re_log (re_exp x) = x := by
  have : ∃ y, re_exp y = re_exp x := ⟨x, rfl⟩
  rw [re_log, Function.invFun, dif_pos this]
  apply re_exp_bij_on_rpos.injOn trivial trivial
  rw [this.choose_spec]

theorem re_exp_re_log (xpos : x > 0) : re_exp (re_log x) = x := by
  apply Function.invFun_eq
  obtain ⟨y, _, eq⟩ := re_exp_bij_on_rpos.surjOn xpos
  exact ⟨y, eq⟩

theorem re_log_mono : StrictMonoOn re_log {x : ℝ | x > 0} := by
  intro x xpos y ypos xlty
  apply re_exp_mono.lt_iff_lt.mp
  rwa [re_exp_re_log xpos, re_exp_re_log ypos]

open General.Topology Set in
theorem re_log_continuous : ContinuousOn re_log {x : ℝ | x > 0} := by
  apply Metric.continuousOn_iff.mpr
  intro x xpos ε ε0
  simp at xpos
  obtain ⟨a, ⟨apos, altx⟩⟩ := exists_between xpos
  obtain ⟨b, bgtx⟩ := exists_gt x
  have contOnAB : ContinuousOn re_exp (Icc (re_log a) (re_log b)) := re_exp_continuous.continuousOn
  have invContOnAB := invFun_continuous_of_compact (isCompact_Icc) (re_exp_injective) contOnAB
  have x_im_Icc : x ∈ re_exp '' Icc (re_log a) (re_log b) := by
    refine' ⟨re_log x, ⟨_, _⟩, re_exp_re_log xpos⟩
    . apply le_of_lt
      exact re_log_mono apos xpos altx
    . apply le_of_lt
      exact re_log_mono xpos (lt_trans xpos bgtx) bgtx
  obtain ⟨δ, δ0, δcond⟩ := Metric.continuousOn_iff.mp invContOnAB x x_im_Icc ε ε0
  let δ' := min δ <| min |x-a| |x-b|
  refine' ⟨δ', lt_min δ0 (lt_min _ _), _⟩
  . exact abs_pos_of_pos (sub_pos_of_lt altx)
  . exact abs_pos_of_neg (sub_neg_of_lt bgtx)
  intro y ypos dist_lt
  refine' δcond y _ (lt_of_lt_of_le dist_lt (min_le_left _ _))
  simp
  refine' ⟨re_log y, ⟨_, _⟩, re_exp_re_log ypos⟩
  . apply le_of_lt
    apply re_log_mono apos ypos
    calc
      y = x + (y-x) := by ring
      _ > x - δ' := by
        have : -(y-x) < δ' := lt_of_le_of_lt (neg_le_abs _) dist_lt
        linarith
      _ ≥ x - (x-a) := by
        have : δ' ≤ |x-a| := le_trans (min_le_right _ _) (min_le_left _ _)
        have abseq : |x-a| = x-a := abs_eq_self.mpr (le_of_lt (sub_pos_of_lt altx))
        linarith
      _ = a := by ring
  . apply le_of_lt
    apply re_log_mono ypos (lt_trans xpos bgtx)
    calc
      y = x + (y-x) := by ring
      _ < x + δ' := by
        have : y-x < δ' := lt_of_le_of_lt (le_abs_self _) dist_lt
        linarith
      _ ≤ x + (b-x) := by
        have : δ' ≤ |x-b| := le_trans (min_le_right _ _) (min_le_right _ _)
        have abseq : |x-b| = -(x-b) := abs_eq_neg_self.mpr (le_of_lt (sub_neg_of_lt bgtx))
        linarith
      _ = b := by ring

-- Derivative of exp

open ENNReal in
@[simp]
theorem exp_deriv : HasDerivAt exp (exp x) x := by
  nth_rw 1 [exp_equals_powerSeries_expansion_at x]
  convert powerSeries_hasDerivAt_center _
  . simp
  . exact inferInstance
  . exact inferInstance
  . suffices RadiusOfConvergence (exp_powerSeries_expansion_at x) = ∞ by
      rw [this]
      exact top_ne_bot
    apply powerSeries_radius_infty_of_everywhere_convergent
    intro z
    exact ⟨_, exp_powerSeries_expansion_at_convergesTo _ _⟩

-- sin, cos

noncomputable def sin (z : ℂ₁) := (exp (i*z) - exp (-(i*z))) / (2*i)
noncomputable def cos (z : ℂ₁) := (exp (i*z) + exp (-(i*z))) / 2

theorem sin_neg : sin (-z) = -(sin z) := by
  rw [sin, sin, neg_div', neg_sub]
  ring_nf

theorem cos_neg : cos (-z) = cos z := by
  rw [cos, cos]
  ring_nf

theorem sin_of_zero : sin 0 = 0 := by simp [sin]

theorem cos_of_zero : cos 0 = 1 := by
  simp [cos, exp_of_zero, one_add_one_eq_two]
  rw [div_self]
  show (⟨2, 0⟩ : ℂ₁) ≠ ⟨0, 0⟩
  simp

noncomputable def sin_powerSeries := General.PowerSeries.mk (fun n ↦
    if Even n
    then 0
    else ((-1 : ℂ₁)^((n-1)/2)/(n !))
  ) 0

noncomputable def cos_powerSeries := General.PowerSeries.mk (fun n ↦
    if Even n
    then ((-1 : ℂ₁)^(n/2)/(n !))
    else 0
  ) 0

lemma exp_iz_powerSeries_eq :
    exp_powerSeries.series_at (i*z) = (cos_powerSeries.series_at z) + (fun n ↦ i*(sin_powerSeries.series_at z n)) := by
  funext n
  simp [exp_powerSeries, general_exp_powerSeries, cos_powerSeries, sin_powerSeries, series_at]
  by_cases nz : n = 0
  . have even : Even n := by use 0
    simp [if_pos even, nz]

  mod_cases nmod : n % 4
  . obtain ⟨k, keq⟩ := Nat.modEq_zero_iff_dvd.mp nmod
    have even : Even n := by
      use 2*k
      rw [keq]
      ring
    simp_rw [if_pos even, mul_pow, i_pow_mod_0 nmod, one_mul, add_zero, div_eq_inv_mul]
    ring_nf
    nth_rw 1 [← mul_one (_ * z^n)]
    congr
    have : (4 * k / 2 : ℕ) = (2 * k : ℕ) := by omega
    rw [keq, this, pow_mul, neg_one_sq, one_pow]

  . have nge1 : n ≥ 1 := by omega
    obtain ⟨k, keq⟩ := (Nat.modEq_iff_dvd' nge1).mp nmod.symm
    apply_fun (.+1) at keq
    rw [Nat.sub_add_cancel nge1] at keq
    have odd : Odd n := by
      use 2*k
      simp [keq, ← mul_assoc]
    rw [← not_even_iff_odd] at odd
    simp_rw [if_neg odd, mul_pow, i_pow_mod_1 nmod, zero_add, div_eq_inv_mul]
    ring_nf
    nth_rw 1 [← mul_one (_ * z^n)]
    congr 1
    rw [keq, Nat.add_sub_cancel]
    have : 4 * k / 2 = 2 * k := by omega
    rw [this, pow_mul, neg_one_sq, one_pow]

  . have nne1 : n ≠ 1 := by
      intro ne1
      simp [ne1] at nmod
      apply ModEq.eq_of_lt_of_lt at nmod
      norm_num at nmod
    have nge2 : n ≥ 2 := by omega
    obtain ⟨k, keq⟩ := (Nat.modEq_iff_dvd' nge2).mp nmod.symm
    apply_fun (.+2) at keq
    rw [Nat.sub_add_cancel nge2] at keq
    have even : Even n := by
      rw [keq]
      use 2*k+1
      ring
    simp_rw [if_pos even, mul_pow, i_pow_mod_2 nmod, add_zero, div_eq_inv_mul, ← mul_assoc]
    congr 2
    have : (4 * k + 2) / 2 = 2 * k + 1 := by omega
    rw [keq, this, pow_add, pow_mul, neg_one_sq, one_pow, one_mul, pow_one]

  . have nne1 : n ≠ 1 := by
      intro ne1
      simp [ne1] at nmod
      apply ModEq.eq_of_lt_of_lt at nmod
      norm_num at nmod
    have nne2 : n ≠ 2 := by
      intro ne2
      simp [ne2] at nmod
      apply ModEq.eq_of_lt_of_lt at nmod
      norm_num at nmod
    have nge3 : n ≥ 3 := by omega
    obtain ⟨k, keq⟩ := (Nat.modEq_iff_dvd' nge3).mp nmod.symm
    have keq : n-1 = 4*k + 2 := by
      apply_fun (·+2) at keq
      rw [← keq]
      omega
    have keq : n-1 = 2*(2*k+1) := by
      rw [keq]
      ring
    have odd : Odd n := by
      use 2*k+1
      rw [← keq]
      omega
    rw [← not_even_iff_odd] at odd
    simp_rw [if_neg odd, mul_pow, i_pow_mod_3 nmod, zero_add, div_eq_inv_mul]
    ring_nf
    rw [neg_eq_neg_one_mul, mul_comm (-1)]
    congr
    rw [keq, Nat.mul_div_cancel_left, pow_add, pow_mul, neg_one_sq, one_pow, one_mul, pow_one]
    omega

lemma exp_neg_iz_powerSeries_eq :
    exp_powerSeries.series_at (-(i*z)) = (cos_powerSeries.series_at z) - (fun n ↦ i*(sin_powerSeries.series_at z n)) := by
  have : -(i*z) = i*(-z) := by ring
  rw [this, exp_iz_powerSeries_eq]
  congr
  . funext n
    simp_rw [cos_powerSeries, series_at]
    by_cases even : Even n
    . simp_rw [if_pos even, sub_zero, neg_eq_neg_one_mul z, mul_pow]
      nth_rw 2 [← one_mul (z^n)]
      congr 2
      obtain ⟨k, rfl⟩ := even_iff_exists_two_mul.mp even
      rw [pow_mul, neg_one_sq, one_pow]
    . simp_rw [if_neg even, zero_mul]
  . funext n
    have : -(fun n ↦ i * sin_powerSeries.series_at z n) n = -(i * sin_powerSeries.series_at z n) := rfl
    rw [this, sin_powerSeries, series_at, series_at]
    by_cases even : Even n
    . simp [if_pos even]
    . simp_rw [if_neg even, sub_zero, neg_eq_neg_one_mul z, mul_pow]
      rw [neg_eq_neg_one_mul (i * _), mul_comm (-1)]
      obtain ⟨k, keq⟩ := not_even_iff_odd.mp even
      nth_rw 3 [keq]
      rw [pow_add, pow_mul, neg_one_sq, one_pow, one_mul, pow_one]
      ring

lemma sin_powerSeries_eq :
    sin_powerSeries.series_at z = fun n ↦ (exp_powerSeries.series_at (i*z) n - exp_powerSeries.series_at (-(i*z)) n)/(2*i) := by
  rw [exp_iz_powerSeries_eq, exp_neg_iz_powerSeries_eq]
  conv =>
    rhs
    ext n
    simp
    rw [← mul_add, ← mul_comm i, mul_div_mul_left _ _ (by intro eq; apply_fun (·.im) at eq; simp at eq),
        ← mul_two, mul_div_cancel_right₀ _ (by
          have : (2 : ℂ₁) = ⟨2, 0⟩ := rfl
          rw [this]
          intro eq
          apply_fun (·.re) at eq
          simp at eq
        )]

lemma cos_powerSeries_eq :
    cos_powerSeries.series_at z = fun n ↦ (exp_powerSeries.series_at (i*z) n + exp_powerSeries.series_at (-(i*z)) n)/2 := by
  rw [exp_iz_powerSeries_eq, exp_neg_iz_powerSeries_eq]
  conv =>
    rhs
    ext n
    simp [← mul_two]
    rw [mul_div_cancel_right₀ _ (by
      have : (2 : ℂ₁) = ⟨2, 0⟩ := rfl
      rw [this]
      intro eq
      apply_fun (·.re) at eq
      simp at eq
    )]

theorem sin_powerSeries_convergesTo_sin : SeriesConvergesTo (sin_powerSeries.series_at z) (sin z) := by
  rw [sin_powerSeries_eq]
  conv =>
    lhs
    ext n
    rw [div_eq_mul_one_div, mul_comm]
  rw [sin]
  nth_rw 2 [div_eq_mul_one_div _ (2*i)]
  rw [mul_comm _ (1/(2*i))]
  apply SeriesConvergesTo.const_mul (a := 1/(2*i))
  simp_rw [sub_eq_add_neg]
  apply SeriesConvergesTo.add
  . obtain ⟨y, convto⟩ := (exp_absolutelyConvergent (i*z)).convergent
    convert convto
    rw [exp, convto.get_isum]
  . nth_rw 2 [neg_eq_neg_one_mul]
    conv =>
      arg 1
      ext n
      rw [neg_eq_neg_one_mul]
    apply SeriesConvergesTo.const_mul
    obtain ⟨y, convto⟩ := (exp_absolutelyConvergent (-(i*z))).convergent
    convert convto
    rw [exp, convto.get_isum]

theorem sin_eq_powerSeries : sin z = ∑ᵢ sin_powerSeries.series_at z := by
  symm
  exact sin_powerSeries_convergesTo_sin.get_isum

theorem cos_powerSeries_convergesTo_cos : SeriesConvergesTo (cos_powerSeries.series_at z) (cos z) := by
  rw [cos_powerSeries_eq]
  conv =>
    lhs
    ext n
    rw [div_eq_mul_one_div, mul_comm]
  rw [cos]
  nth_rw 2 [div_eq_mul_one_div]
  rw [mul_comm _ (1/2)]
  apply SeriesConvergesTo.const_mul
  apply SeriesConvergesTo.add
  . obtain ⟨y, convto⟩ := (exp_absolutelyConvergent (i*z)).convergent
    convert convto
    rw [exp, convto.get_isum]
  . obtain ⟨y, convto⟩ := (exp_absolutelyConvergent (-(i*z))).convergent
    convert convto
    rw [exp, convto.get_isum]

theorem cos_eq_powerSeries : cos z = ∑ᵢ cos_powerSeries.series_at z := by
  symm
  exact cos_powerSeries_convergesTo_cos.get_isum

@[continuity]
theorem sin_continuous : Continuous sin := by
  unfold sin
  continuity

@[continuity]
theorem cos_continuous : Continuous cos := by
  unfold cos
  continuity

private lemma real_pow_im_z (x : ℝ) (k : ℕ) : (x^k : ℂ₁).im = 0 := by
  induction' k with k' ih
  . simp
  . simp [pow_succ]
    exact Or.inl ih

private lemma neg_one_pow_im_z (k : ℕ) : ((-1)^k : ℂ₁).im = 0 := by
  induction' k with k' ih
  . simp
  . simp [pow_succ]
    exact ih

theorem sin_real_of_real (x : ℝ) : sin x ∈ (ℝ' : Set ℂ₁) := by
  apply r_closed.isSeqClosed _ sin_powerSeries_convergesTo_sin
  intro n
  apply sum_mem
  intro k _
  rw [r_mem_iff]
  by_cases even : Even k
  . simp [sin_powerSeries, if_pos even]
  . simp [sin_powerSeries, if_neg even, real_pow_im_z]
    left
    simp [div_eq_inv_mul, mul_im]
    right
    apply neg_one_pow_im_z

theorem cos_real_of_real (x : ℝ) : cos x ∈ (ℝ' : Set ℂ₁) := by
  apply r_closed.isSeqClosed _ cos_powerSeries_convergesTo_cos
  intro n
  apply sum_mem
  intro k _
  rw [r_mem_iff]
  by_cases even : Even k
  . simp [cos_powerSeries, if_pos even, real_pow_im_z]
    left
    simp [div_eq_inv_mul]
    right
    apply neg_one_pow_im_z
  . simp [cos_powerSeries, if_neg even]

-- Euler's formula

theorem exp_iz_eq_cos_plus_i_sin : exp (i*z) = cos z + i*(sin z) := by
  rw [cos, sin, mul_comm 2 i, ← mul_div_assoc, mul_div_mul_left]
  . ring_nf
    rw [mul_assoc, inv_mul_cancel₀, mul_one]
    show (⟨2, 0⟩ : ℂ₁) ≠ ⟨0, 0⟩
    simp
  . show (⟨0, 1⟩ : ℂ₁) ≠ ⟨0, 0⟩
    simp

-- addition theorems

theorem cos_add (z w : ℂ₁) : cos (z + w) = (cos z)*(cos w) - (sin z)*(sin w) := by
  have : -(i*(z+w)) = i*((-z)+(-w)) := by ring
  rw [cos, this, mul_add, mul_add, exp_add, exp_add]
  simp_rw [exp_iz_eq_cos_plus_i_sin]
  simp only [mul_add, add_mul, cos_neg, sin_neg]
  apply (div_eq_iff _).mpr
  ring_nf
  rw [i_sq]
  ring
  apply (nat_cast_ne_zero _).mpr
  simp

theorem sin_add (z w : ℂ₁) : sin (z + w) = (sin z)*(cos w) + (sin w)*(cos z) := by
  have : -(i*(z+w)) = i*((-z)+(-w)) := by ring
  rw [sin, this, mul_add, mul_add, exp_add, exp_add]
  simp_rw [exp_iz_eq_cos_plus_i_sin]
  simp only [mul_add, add_mul, cos_neg, sin_neg]
  apply (div_eq_iff _).mpr
  ring
  simp [i]
  push_neg
  constructor
  . apply (nat_cast_ne_zero _).mpr
    simp
  . intro contra
    apply_fun im at contra
    simp at contra

theorem sin_sq_plus_cos_sq (z : ℂ₁) : (sin z)^2 + (cos z)^2 = 1 := by
  symm
  convert cos_add z (-z) using 1
  . simp [cos_of_zero]
  . simp [cos_neg, sin_neg]
    ring

-- re_sin, re_cos

noncomputable def re_sin (x : ℝ) := (sin x).re

@[continuity]
theorem re_sin_continuous : Continuous re_sin := by
  unfold re_sin
  continuity

theorem re_sin_eq_sin (x : ℝ) : re_sin x = sin x := by
  ext
  . simp [re_sin]
  . simp
    symm
    rw [← r_mem_iff]
    exact sin_real_of_real _

noncomputable def re_sin_powerSeries := General.PowerSeries.mk (fun n ↦
    if Even n
    then 0
    else ((-1 : ℝ)^((n-1)/2)/(n !))
  ) 0

lemma sin_powerSeries_at_re_eq_re_sin_powerSeries_at (x : ℝ) (n : ℕ) : (sin_powerSeries.series_at x n).re = re_sin_powerSeries.series_at x n := by
  unfold series_at
  simp_rw [sin_powerSeries, re_sin_powerSeries]
  by_cases even : Even n
  . simp [if_pos even]
  . simp_rw [if_neg even, sub_zero]
    conv =>
      lhs
      simp [div_eq_inv_mul, mul_re, neg_one_pow_im_z]
      rw [← div_eq_inv_mul, pow_two, div_mul_cancel_right₀ (cast_ne_zero.mpr (factorial_ne_zero _)), ← div_eq_inv_mul]
    congr <;> (apply r_eq_by_c_eq; norm_cast)

theorem re_sin_powerSeries_convergesTo_re_sin : SeriesConvergesTo (re_sin_powerSeries.series_at x) (re_sin x) := by
  unfold SeriesConvergesTo
  convert re_continuous.seqContinuous sin_powerSeries_convergesTo_sin
  dsimp
  rw [re_eq_re_linear_map, map_sum]
  simp_rw [← re_eq_re_linear_map]
  congr
  funext n
  exact (sin_powerSeries_at_re_eq_re_sin_powerSeries_at _ _).symm

theorem re_sin_eq_powerSeries (x : ℝ) : re_sin x = ∑ᵢ re_sin_powerSeries.series_at x := by
  rw [re_sin_powerSeries_convergesTo_re_sin.get_isum]

noncomputable def re_cos (x : ℝ) := (cos x).re

@[continuity]
theorem re_cos_continuous : Continuous re_cos := by
  unfold re_cos
  continuity

theorem re_cos_eq_cos (x : ℝ) : re_cos x = cos x := by
  ext
  . simp [re_cos]
  . simp
    symm
    rw [← r_mem_iff]
    exact cos_real_of_real _

noncomputable def re_cos_powerSeries := General.PowerSeries.mk (fun n ↦
    if Even n
    then ((-1 : ℝ)^(n/2)/(n !))
    else 0
  ) 0

theorem cos_powerSeries_at_re_eq_re_cos_powerSeries_at (x : ℝ) (n : ℕ) : (cos_powerSeries.series_at x n).re = re_cos_powerSeries.series_at x n := by
  unfold series_at
  simp_rw [cos_powerSeries, re_cos_powerSeries]
  by_cases even : Even n
  . simp_rw [if_pos even, sub_zero]
    conv =>
      lhs
      simp [div_eq_inv_mul, mul_re, neg_one_pow_im_z]
      rw [← div_eq_inv_mul, pow_two, div_mul_cancel_right₀ (cast_ne_zero.mpr (factorial_ne_zero _)), ← div_eq_inv_mul]
    congr <;> (apply r_eq_by_c_eq; norm_cast)
  . simp [if_neg even]

lemma re_cos_powerSeries_convergesTo_re_cos : SeriesConvergesTo (re_cos_powerSeries.series_at x) (re_cos x) := by
  unfold SeriesConvergesTo
  convert re_continuous.seqContinuous cos_powerSeries_convergesTo_cos
  dsimp
  rw [re_eq_re_linear_map, map_sum]
  simp_rw [← re_eq_re_linear_map]
  congr
  funext n
  exact (cos_powerSeries_at_re_eq_re_cos_powerSeries_at _ _).symm

theorem re_cos_eq_powerSeries (x : ℝ) : re_cos x = ∑ᵢ re_cos_powerSeries.series_at x := by
  rw [re_cos_powerSeries_convergesTo_re_cos.get_isum]

-- adapt theorems for re_cos, re_sin

theorem re_cos_of_zero : re_cos 0 = 1 := by
  apply r_eq_by_c_eq
  rw [re_cos_eq_cos]
  convert cos_of_zero

theorem re_sin_of_zero : re_sin 0 = 0 := by
  apply r_eq_by_c_eq
  rw [re_sin_eq_sin]
  convert sin_of_zero

theorem re_cos_neg : re_cos (-z) = re_cos z := by
  apply r_eq_by_c_eq
  simp [re_cos_eq_cos]
  push_cast
  apply cos_neg

theorem re_sin_neg : re_sin (-z) = -(re_sin z) := by
  apply r_eq_by_c_eq
  push_cast
  simp [re_sin_eq_sin]
  convert sin_neg
  norm_cast

theorem re_cos_add (z w : ℝ) : re_cos (z + w) = (re_cos z)*(re_cos w) - (re_sin z)*(re_sin w) := by
  apply r_eq_by_c_eq
  push_cast
  simp [re_cos_eq_cos, re_sin_eq_sin]
  push_cast
  exact cos_add _ _

theorem re_sin_add (z w : ℝ) : re_sin (z + w) = (re_sin z)*(re_cos w) + (re_sin w)*(re_cos z) := by
  apply r_eq_by_c_eq
  push_cast
  simp [re_cos_eq_cos, re_sin_eq_sin]
  push_cast
  exact sin_add _ _

theorem re_sin_sq_plus_re_cos_sq : (re_sin z)^2 + (re_cos z)^2 = 1 := by
  symm
  convert re_cos_add z (-z) using 1
  . simp [re_cos_of_zero]
  . simp [re_cos_neg, re_sin_neg]
    ring

-- derivatives of sin, cos, re_sin, re_cos

lemma iz_deriv : HasDerivAt (fun z ↦ i*z) i z := by
  convert HasDerivAt.const_mul _ (hasDerivAt_id' z)
  simp

lemma neg_iz_deriv : HasDerivAt (fun z ↦ -(i*z)) (-i) z := iz_deriv.neg

lemma exp_iz_deriv : HasDerivAt (fun z ↦ exp (i*z)) (i*exp (i*z)) z := by
  have : (fun z ↦ exp (i*z)) = exp ∘ (fun z ↦ i*z) := by funext; simp
  rw [mul_comm, this]
  apply HasDerivAt.comp
  exact exp_deriv
  exact iz_deriv

lemma exp_neg_iz_deriv : HasDerivAt (fun z ↦ exp (-(i*z))) (-(i*exp (-(i*z)))) z := by
  have : (fun z ↦ exp (-(i*z))) = exp ∘ (fun z ↦ -(i*z)) := by funext; simp
  rw [← neg_mul, mul_comm, this]
  apply HasDerivAt.comp
  exact exp_deriv
  exact neg_iz_deriv

theorem sin_deriv : HasDerivAt sin (cos z) z := by
  convert (exp_iz_deriv.sub exp_neg_iz_deriv).div_const (2*i) using 1
  simp
  rw [cos, ← mul_add, mul_comm 2 i, mul_div_mul_left]
  rw [i]
  intro h
  apply_fun im at h
  simp at h

theorem cos_deriv : HasDerivAt cos (-(sin z)) z := by
  convert (exp_iz_deriv.add exp_neg_iz_deriv).div_const 2 using 1
  rw [sin, ← sub_eq_add_neg, ← mul_sub, mul_comm 2, ← one_mul ((exp _) - (exp _)), mul_div_mul_comm]
  have : 1/i = -i := by
    simp [i, inv_def]
    ext <;> simp
  rw [this]
  ring

open General.Differentiation in
theorem re_sin_deriv : HasDerivAt re_sin (re_cos x) x := by
  apply hasDerivAt_iff_exists_continuous.mpr
  have ⟨f₁, ⟨sin_eq, deriv_eq, cont⟩⟩ := hasDerivAt_iff_exists_continuous.mp (sin_deriv (z := x))
  use (fun y ↦ (f₁ ⟨y,0⟩).re)
  constructor
  . intro u
    apply r_eq_by_c_eq
    push_cast
    simp [re_sin_eq_sin]
    convert sin_eq u
    ext <;> simp
    . rfl
    . by_cases eq : u = x
      . rw [eq, deriv_eq]
        exact (r_mem_iff.mp (cos_real_of_real x)).symm
      have : f₁ u = (sin u - sin x) / (u - x) := by
        have nc : (u : ℂ₁) ≠ x := by
          intro contra
          apply_fun re at contra
          simp at contra
          contradiction
        have nz : (u - x : ℂ₁) ≠ 0 := sub_ne_zero_of_ne nc
        apply (eq_div_iff nz).mpr
        apply eq_sub_iff_add_eq.mpr
        rw [mul_comm, add_comm]
        symm
        exact sin_eq u
      rw [this]
      norm_cast
      rw [div_re_split, add_im]
      norm_cast
      rw [zero_add, sub_eq_add_neg, add_im, neg_im]
      rw [r_mem_iff.mp (sin_real_of_real u), r_mem_iff.mp (sin_real_of_real x)]
      simp
      have : (0 : ℝ) = (0 : ℂ₁) := by ext <;> simp
      simp [this]
  . constructor
    . apply_fun re at deriv_eq
      convert deriv_eq using 1
    . have : (fun y ↦ (f₁ ⟨y,0⟩).re) = re ∘ f₁ ∘ r_inj_c := by funext; simp [r_inj_c]
      rw [this]
      exact re_continuous.continuousAt.comp (cont.comp r_inj_c_continuous.continuousAt)

open General.Differentiation in
theorem re_cos_deriv : HasDerivAt re_cos (-(re_sin x)) x := by
  apply hasDerivAt_iff_exists_continuous.mpr
  have ⟨f₁, ⟨cos_eq, deriv_eq, cont⟩⟩ := hasDerivAt_iff_exists_continuous.mp (cos_deriv (z := x))
  use (fun y ↦ (f₁ ⟨y,0⟩).re)
  constructor
  . intro u
    apply r_eq_by_c_eq
    push_cast
    simp [re_cos_eq_cos]
    convert cos_eq u
    ext <;> simp
    . rfl
    . by_cases eq : u = x
      . rw [eq, deriv_eq, neg_im]
        apply zero_eq_neg.mpr
        exact r_mem_iff.mp (sin_real_of_real x)
      have : f₁ u = (cos u - cos x) / (u - x) := by
        have nc : (u : ℂ₁) ≠ x := by
          intro contra
          apply_fun re at contra
          simp at contra
          contradiction
        have nz : (u - x : ℂ₁) ≠ 0 := sub_ne_zero_of_ne nc
        apply (eq_div_iff nz).mpr
        apply eq_sub_iff_add_eq.mpr
        rw [mul_comm, add_comm]
        symm
        exact cos_eq u
      rw [this]
      norm_cast
      rw [div_re_split, add_im]
      norm_cast
      rw [zero_add, sub_eq_add_neg, add_im, neg_im]
      rw [r_mem_iff.mp (cos_real_of_real u), r_mem_iff.mp (cos_real_of_real x)]
      simp
      have : (0 : ℝ) = (0 : ℂ₁) := by ext <;> simp
      simp [this]
  . constructor
    . apply_fun re at deriv_eq
      convert deriv_eq using 1
    . have : (fun y ↦ (f₁ ⟨y,0⟩).re) = re ∘ f₁ ∘ r_inj_c := by funext; simp [r_inj_c]
      rw [this]
      exact re_continuous.continuousAt.comp (cont.comp r_inj_c_continuous.continuousAt)

-- definition of π

open Filter Topology BigOperators Finset in
lemma re_cos_two_le_neg_one_third : re_cos 2 ≤ -(1/3) := by
  let ϕ : ℕ → ℕ := fun n ↦ 2*n
  have ϕ_tt : Tendsto ϕ atTop atTop := by
    rw [tendsto_atTop]
    intro n; simp; use n
    intro m lte
    simp [ϕ]
    linarith

  let s₁ := fun n ↦ (-1 : ℝ)^n * (4^n / (2*n)!)
  have conv₁ : SeriesConvergesTo s₁ (re_cos 2) := by
    let f : ℕ → ℝ := fun n ↦ ∑ k in range (n+1), re_cos_powerSeries.series_at 2 k
    have f_tt : Tendsto f atTop (𝓝 (re_cos 2)) := re_cos_powerSeries_convergesTo_re_cos
    have f_comp_tt : Tendsto (f ∘ ϕ) atTop (𝓝 (re_cos 2)) := f_tt.comp ϕ_tt
    have comp_eq : f ∘ ϕ = fun n ↦ ∑ k in range (n+1), s₁ k := by
      funext n
      induction' n with n' ih
      . unfold f ϕ s₁
        simp [series_at, re_cos_powerSeries]
      . dsimp [f, ϕ] at ih ⊢
        have : 2*n'.succ + 1 = (2*n' + 1).succ.succ := by omega
        rw [this, sum_range_succ, sum_range_succ, ih, series_at, series_at, re_cos_powerSeries]
        have odd : ¬(Even (2*n' + 1)) := by
          rw [not_even_iff_odd]
          exact odd_two_mul_add_one n'
        have even : Even (2*n' + 2) := even_add_one.mpr odd
        simp [if_neg odd, if_pos even]
        have : n'.succ + 1 = (n'+1).succ := by omega
        rw [this]
        nth_rw 2 [sum_range_succ]
        congr 1
        have : 2*n' + 2 = 2*(n'+1) := by omega
        rw [this, pow_mul]
        norm_num
        have : n'.succ = n'+1 := by omega
        simp [s₁, this]
        ring
    rw [SeriesConvergesTo, ← comp_eq]
    exact f_comp_tt

  let s₂ := fun (n : ℕ) ↦ ((4 : ℝ)^(2*n+1)/(4*n+2)!) * (1-(4/((4*n+3)*(4*n+4))))
  have conv₂ : Tendsto (fun n ↦ ∑ k in range n, s₂ k) atTop (𝓝 (1 - (re_cos 2))) := by
    let f := fun n ↦ ∑ k in range (n+1), s₁ k
    have f_comp_tt : Tendsto (f ∘ ϕ) atTop (𝓝 (re_cos 2)) := conv₁.comp ϕ_tt
    have comp_eq : f ∘ ϕ = fun n ↦ 1 - ∑ k in range n, ((4 : ℝ)^(2*k+1) / (4*k+2)!) * (1 - (4/((4*k+3)*(4*k+4)))) := by
      funext n
      induction' n with n' ih
      . simp [ϕ, f, s₁]
      simp [ϕ, f] at ih ⊢
      have : 2 * n'.succ + 1 = (2*n' + 1).succ.succ := by omega
      rw [this, sum_range_succ, sum_range_succ, ih, sum_range_succ, sub_add, sub_add, sub_sub]
      congr
      rw [sub_eq_add_neg]
      congr
      have pow_even : (-1 : ℝ)^(2*n'+2) = 1 := by
        apply (neg_one_pow_eq_one_iff_even _).mpr
        apply even_iff_exists_two_mul.mpr
        use (n'+1)
        omega
        norm_num
      have pow_odd : (-1 : ℝ)^(2*n'+1) = (-1) := by
        rw [pow_add, pow_one]
        nth_rw 3 [← one_mul (-1)]
        congr
        apply (neg_one_pow_eq_one_iff_even _).mpr
        apply even_iff_exists_two_mul.mpr
        use n'
        norm_num
      simp [s₁, pow_even, pow_odd]
      have : (4 : ℝ)^(2*n'+2) = 4^(2*n'+1)*4 := by ring
      rw [this]
      ring_nf
      congr
      have : 4 + n'*4 = ((2 + n'*4) + 1) + 1 := by omega
      rw [this, factorial_succ, factorial_succ, mul_comm, mul_comm _ (2+n'*4)!, mul_assoc]
      push_cast
      rw [mul_inv]
      ring
    have := ((tendsto_neg _).comp f_comp_tt).const_add 1
    convert this using 1
    funext n
    simp [comp_eq, s₂]

  have ge : 1 - (re_cos 2) ≥ 4/3 := by
    apply ge_of_tendsto conv₂
    simp; use 1; intro n nge
    induction' n with n' ih
    . norm_num at nge
    . cases' n' with n''
      . simp [s₂]
        norm_num
      rw [Finset.sum_range_succ]
      have nonneg : s₂ (n'' + 1) ≥ 0 := by
        unfold s₂
        apply mul_nonneg
        . positivity
        . rw [sub_nonneg, div_le_one]
          . ring_nf
            have : (12 : ℝ) = 4 + 8 := by norm_num
            rw [this, add_assoc, add_assoc]
            apply le_add_of_nonneg_right
            positivity
          . positivity
      have : 1 ≤ succ n'' := by omega
      have ih := ih this
      linarith

  linarith

open Filter Topology BigOperators Finset in
lemma re_sin_zero_two_pos (gt : x > 0) (le : x ≤ 2) : re_sin x > 0 := by
  let ϕ : ℕ → ℕ := fun n ↦ 2*n+1
  have ϕ_tt : Tendsto ϕ atTop atTop := by
    rw [tendsto_atTop]
    intro n; simp; use n
    intro m lte
    simp [ϕ]
    linarith

  have even : ∀ n, Even (2*n + 2) := by
    intro n
    use n+1
    omega

  have odd : ∀ n, ¬(Even (2*n + 3)) := by
    intro n
    exact even_add_one.mp.mt (not_not.mpr (even n))

  let s₁ := fun n ↦ (-1 : ℝ)^n * (x^(2*n+1)/(2*n + 1)!)
  have conv₁ : SeriesConvergesTo s₁ (re_sin x) := by
    let f : ℕ → ℝ := fun n ↦ ∑ k in range (n+1), re_sin_powerSeries.series_at x k
    have f_tt : Tendsto f atTop (𝓝 (re_sin x)) := re_sin_powerSeries_convergesTo_re_sin
    have f_comp_tt : Tendsto (f ∘ ϕ) atTop (𝓝 (re_sin x)) := f_tt.comp ϕ_tt
    have comp_eq : f ∘ ϕ = fun n ↦ ∑ k in range (n+1), s₁ k := by
      funext n
      induction' n with n' ih
      . unfold f ϕ s₁
        simp [series_at, re_sin_powerSeries, sum_range_succ]
      . dsimp [f, ϕ] at ih ⊢
        have : 2*n'.succ + 1 + 1 = (2*n' + 1 + 1).succ.succ := by omega
        rw [this, sum_range_succ, sum_range_succ, ih, series_at, series_at, re_sin_powerSeries]
        simp [if_pos (even n'), if_neg (odd n')]
        have : n'.succ + 1 = (n'+1).succ := by omega
        rw [this]
        nth_rw 2 [sum_range_succ]
        congr 1
        have : 2*n' + 3 = 2*(n'+1) + 1 := by omega
        simp [s₁]
        rw [this, mul_comm, mul_div, mul_comm, ← mul_div]
    rw [SeriesConvergesTo, ← comp_eq]
    exact f_comp_tt

  let s₂ := fun (n : ℕ) ↦ (x^(4*n + 1)/(4*n+1)!)*(1-(x^2/((4*n+2)*(4*n+3))))
  have conv₂ : Tendsto (fun n ↦ ∑ k in range (n+1), s₂ k) atTop (𝓝 (re_sin x)) := by
    let f := fun n ↦ ∑ k in range (n+1), s₁ k
    have f_comp_tt : Tendsto (f ∘ ϕ) atTop (𝓝 (re_sin x)) := conv₁.comp ϕ_tt
    have comp_eq : f ∘ ϕ = fun n ↦ ∑ k in range (n+1), s₂ k := by
      funext n
      induction' n with n' ih
      . simp [ϕ, f, s₁, s₂, sum_range_succ, mul_sub, ← sub_eq_add_neg]
        rw [mul_div, mul_comm x, ← pow_succ]
        congr
        norm_cast
      simp [f, ϕ] at ih ⊢
      have : 2*n'.succ + 1 + 1 = (2*n' + 1 + 1).succ.succ := by omega
      rw [this, sum_range_succ, sum_range_succ, ih, sum_range_succ, sum_range_succ, sum_range_succ, add_assoc]
      congr 1
      simp [s₁, s₂, (even n').neg_one_pow, (not_even_iff_odd.mp (odd n')).neg_one_pow, ← sub_eq_add_neg]
      rw [mul_sub, mul_one, _root_.div_mul_div_comm, ← pow_add]
      congr 3
      . omega
      . congr 1; omega
      . omega
      . norm_cast
        have eq₁ : 2*(2*n'+3)+1 = 4*n' + 7 := by omega
        have eq₂ : 4*(n'+1)+1 = 4*n' + 5 := by omega
        have eq₃ : 4*(n'+1)+2 = 4*n' + 6 := by omega
        have eq₄ : 4*(n'+1)+3 = 4*n' + 7 := by omega
        rw [eq₁, eq₂, eq₃, eq₄, factorial_succ, factorial_succ]
        ring
    rw [← comp_eq]
    exact f_comp_tt

  suffices re_sin x ≥ x/3 by linarith

  have xsq_le : x^2 ≤ 4 := by
    have : (4 : ℝ) = 2*2 := by norm_num
    rw [this, pow_two]
    apply mul_le_mul <;> linarith

  apply ge_of_tendsto conv₂
  simp; use 0; intro n _
  induction' n with n' ih
  . simp [s₂]
    have : x^2/(2*3) ≤ 2/3 := by
      norm_num
      linarith
    have : 1 - x^2/(2*3) ≥ 1/3 := by linarith
    have : x / 3 = x * (1/3) := by rw [div_eq_mul_one_div]
    rw [this]
    apply mul_le_mul <;> linarith
  . rw [sum_range_succ]
    apply le_trans (ih (by omega))
    rw [le_add_iff_nonneg_right]
    dsimp [s₂]
    apply mul_nonneg
    . apply div_nonneg
      . apply pow_nonneg
        linarith
      . norm_cast
        exact le_of_lt (factorial_pos _)
    . apply sub_nonneg_of_le
      apply div_le_one_of_le₀
      . apply le_trans xsq_le
        norm_cast
        have : n'+1 ≥ 1 := by omega
        have : (4*(n'+1) + 2)*(4*(n'+1) + 3) ≥ (4+2)*(4+3) := by
          apply mul_le_mul <;> omega
        apply le_trans _ this
        norm_num
      . norm_cast
        apply Nat.zero_le

theorem exists_re_cos_zero_between_zero_two : ∃ x, re_cos x = 0 ∧ x > 0 ∧ x < 2 := by
  have lte : (0 : ℝ) ≤ 2 := by norm_num
  have zero_between : 0 ∈ Set.Ioo (re_cos 2) (re_cos 0) := by
    rw [Set.mem_Ioo]
    constructor
    . apply lt_of_le_of_lt re_cos_two_le_neg_one_third
      norm_num
    . unfold re_cos
      have : (0 : ℝ) = (0 : ℂ₁) := by norm_cast
      simp [this, cos_of_zero]
  have := intermediate_value_Ioo' lte re_cos_continuous.continuousOn
  convert (this zero_between) using 0
  simp; tauto

noncomputable def τ : ℝ := exists_re_cos_zero_between_zero_two.choose
noncomputable def π : ℝ := 2*τ

theorem pi_over_two_eq_tau : π / 2 = τ := by simp [π]
theorem tau_gt_zero : τ > 0 := exists_re_cos_zero_between_zero_two.choose_spec.right.left
theorem tau_lt_two : τ < 2 := exists_re_cos_zero_between_zero_two.choose_spec.right.right

theorem pi_pos : π > 0 := by simp [π]; linarith [tau_gt_zero]

open Set in
lemma re_cos_strict_anti_between_zero_two : StrictAntiOn re_cos (Ioo 0 2) := by
  apply strictAntiOn_of_deriv_neg
  . exact convex_Ioo 0 2
  . exact re_cos_continuous.continuousOn
  . intro x xIn
    rw [re_cos_deriv.deriv]
    simp at xIn
    apply neg_lt_zero.mpr
    apply re_sin_zero_two_pos
    . exact xIn.left
    . exact le_of_lt xIn.right

theorem tau_unique_zero : ∀ x, (re_cos x = 0 ∧ x > 0 ∧ x < 2) → x = τ := by
  intro x ⟨eqz, gtz, lt2⟩
  have tau_spec := exists_re_cos_zero_between_zero_two.choose_spec
  apply re_cos_strict_anti_between_zero_two.injOn ⟨gtz, lt2⟩ ⟨tau_gt_zero, tau_lt_two⟩
  rwa [τ, tau_spec.left]

-- special values of sin, cos functions

theorem cos_of_pi_over_two : cos (π / 2 : ℝ) = 0 := by
  rw [← re_cos_eq_cos]
  rw [pi_over_two_eq_tau, τ, exists_re_cos_zero_between_zero_two.choose_spec.left]
  ext <;> simp

theorem re_cos_of_pi_over_two : re_cos (π / 2) = 0 := by
  apply r_eq_by_c_eq
  rw [re_cos_eq_cos]
  convert cos_of_pi_over_two

theorem sin_of_pi_over_two : sin (π / 2 : ℝ) = 1 := by
  have := sin_sq_plus_cos_sq (π / 2 : ℝ)
  simp [cos_of_pi_over_two] at this
  rcases this with h | h
  . exact h
  . rw [← re_sin_eq_sin] at h
    apply_fun re at h
    simp at h
    have : re_sin (π / 2) > 0 := by
      rw [pi_over_two_eq_tau]
      exact re_sin_zero_two_pos tau_gt_zero (le_of_lt tau_lt_two)
    exfalso; linarith

theorem re_sin_of_pi_over_two : re_sin (π / 2) = 1 := by
  apply r_eq_by_c_eq
  rw [re_sin_eq_sin]
  convert sin_of_pi_over_two

theorem cos_of_pi : cos π = -1 := by
  have : π = (π / 2) + (π / 2) := by ring
  rw [this]
  push_cast
  rw [cos_add]
  norm_cast
  simp [cos_of_pi_over_two, sin_of_pi_over_two]

theorem re_cos_of_pi : re_cos π = -1 := by
  apply r_eq_by_c_eq
  rw [re_cos_eq_cos]
  convert cos_of_pi
  ext <;> simp

theorem sin_of_pi : sin π = 0 := by
  have : π = (π / 2) + (π / 2) := by ring
  rw [this]
  push_cast
  rw [sin_add]
  norm_cast
  simp [cos_of_pi_over_two]

theorem re_sin_of_pi : re_sin π = 0 := by
  apply r_eq_by_c_eq
  rw [re_sin_eq_sin]
  apply sin_of_pi

theorem cos_of_three_pi_over_two : cos ((3 * π) / 2 : ℝ) = 0 := by
  have : (3 * π) / 2 = (π / 2) + π := by ring
  rw [this]
  push_cast
  rw [cos_add]
  norm_cast
  simp [cos_of_pi_over_two, sin_of_pi]

theorem re_cos_of_three_pi_over_two : re_cos ((3 * π) / 2) = 0 := by
  apply r_eq_by_c_eq
  rw [re_cos_eq_cos]
  convert cos_of_three_pi_over_two

theorem sin_of_three_pi_over_two : sin ((3 * π) / 2 : ℝ) = -1 := by
  have : (3 * π) / 2 = (π / 2) + π := by ring
  rw [this]
  push_cast
  rw [sin_add]
  norm_cast
  simp [sin_of_pi, cos_of_pi, sin_of_pi_over_two]

theorem re_sin_of_three_pi_over_two : re_sin ((3 * π) / 2) = -1 := by
  apply r_eq_by_c_eq
  rw [re_sin_eq_sin]
  convert sin_of_three_pi_over_two
  ext <;> simp

theorem cos_of_two_pi : cos (2*π : ℝ) = 1 := by
  have : 2*π = π + π := by ring
  rw [this]
  push_cast
  simp [cos_add, cos_of_pi, sin_of_pi]

theorem re_cos_of_two_pi : re_cos (2*π) = 1 := by
  apply r_eq_by_c_eq
  rw [re_cos_eq_cos]
  convert cos_of_two_pi

theorem sin_of_two_pi : sin (2*π : ℝ) = 0 := by
  have : 2*π = π + π := by ring
  rw [this]
  push_cast
  simp [sin_add, sin_of_pi, cos_of_pi]

theorem re_sin_of_two_pi : re_sin (2*π) = 0 := by
  apply r_eq_by_c_eq
  rw [re_sin_eq_sin]
  convert sin_of_two_pi

-- special values of exp function

theorem exp_of_i_pi_over_two : exp (i*((π/2) : ℝ)) = i := by
  rw [exp_iz_eq_cos_plus_i_sin, cos_of_pi_over_two, sin_of_pi_over_two]
  ring

theorem exp_of_i_pi : exp (i*π) = -1 := by
  rw [exp_iz_eq_cos_plus_i_sin, cos_of_pi, sin_of_pi]
  ring

theorem exp_of_i_three_pi_over_two : exp (i*((3*π)/2 : ℝ)) = -i := by
  rw [exp_iz_eq_cos_plus_i_sin, cos_of_three_pi_over_two, sin_of_three_pi_over_two]
  ring

theorem exp_of_i_two_pi : exp (i*(2*π : ℝ)) = 1 := by
  rw [exp_iz_eq_cos_plus_i_sin, cos_of_two_pi, sin_of_two_pi]
  ring

-- periodicity of sin, cos

theorem cos_plus_two_pi : cos (z + (2*π : ℝ)) = cos z := by
  simp [cos_add, cos_of_two_pi, sin_of_two_pi]

theorem re_cos_plus_two_pi : re_cos (x + 2*π) = re_cos x := by
  simp [re_cos_add, re_cos_of_two_pi, re_sin_of_two_pi]

theorem sin_plus_two_pi : sin (z + (2*π : ℝ)) = sin z := by
  simp [sin_add, cos_of_two_pi, sin_of_two_pi]

theorem re_sin_plus_two_pi : re_sin (x + 2*π) = re_sin x := by
  simp [re_sin_add, re_sin_of_two_pi, re_cos_of_two_pi]

theorem cos_plus_pi : cos (z + π) = -(cos z) := by
  simp [cos_add, cos_of_pi, sin_of_pi]

theorem re_cos_plus_pi : re_cos (x + π) = -(re_cos x) := by
  simp [re_cos_add, re_cos_of_pi, re_sin_of_pi]

theorem cos_plus_pi_over_two : cos (z + (π / 2 : ℝ)) = -(sin z) := by
  simp [cos_add, cos_of_pi_over_two, sin_of_pi_over_two]

theorem re_cos_plus_pi_over_two : re_cos (x + (π / 2)) = -(re_sin x) := by
  simp [re_cos_add, re_cos_of_pi_over_two, re_sin_of_pi_over_two]

theorem sin_plus_pi_over_two : sin (z + (π / 2 : ℝ)) = cos z := by
  simp [sin_add, cos_of_pi_over_two, sin_of_pi_over_two]

theorem re_sin_plus_pi_over_two : re_sin (x + (π / 2)) = re_cos x := by
  simp [re_sin_add, re_cos_of_pi_over_two, re_sin_of_pi_over_two]

-- sign and monotonicity of re_sin, re_cos

theorem re_cos_pos_between (gt : x > -(π/2)) (lt : x < π/2) : re_cos x > 0 := by
  wlog nz : x ≠ 0
  . rw [not_not] at nz
    rw [nz, re_cos_of_zero]
    norm_num
  wlog gtz : x > 0
  . rw [← re_cos_neg]
    apply this
    . simpa
    . linarith
    . rwa [neg_ne_zero]
    . apply neg_pos_of_neg
      apply lt_of_le_of_ne _ nz
      linarith
  rw [← re_cos_of_pi_over_two]
  have lt2 : π/2 < 2 := by
    rw [pi_over_two_eq_tau]
    exact tau_lt_two
  apply re_cos_strict_anti_between_zero_two _ _ lt
  . constructor
    . exact gtz
    . exact lt_trans lt lt2
  . constructor
    . exact lt_trans gtz lt
    . exact lt2

theorem re_cos_neg_between (gt : x > π/2) (lt : x < (3*π)/2) : re_cos x < 0 := by
  have : x = (x-π)+π := by ring
  rw [this, re_cos_plus_pi]
  apply neg_neg_of_pos
  apply re_cos_pos_between <;> linarith

theorem re_sin_pos_between (gt : x > 0) (lt : x < π) : re_sin x > 0 := by
  have : x = (x-π/2)+(π/2) := by ring
  rw [this, re_sin_plus_pi_over_two]
  apply re_cos_pos_between <;> linarith

theorem re_sin_neg_between (gt : x > π) (lt : x < 2*π) : re_sin x < 0 := by
  have : x = (x-π/2)+(π/2) := by ring
  rw [this, re_sin_plus_pi_over_two]
  apply re_cos_neg_between <;> linarith

open Set in
theorem re_cos_strict_anti_on : StrictAntiOn re_cos (Icc 0 π) := by
  apply strictAntiOn_of_deriv_neg
  . exact convex_Icc _ _
  . exact re_cos_continuous.continuousOn
  . intro x xIn
    rw [re_cos_deriv.deriv]
    apply neg_neg_of_pos
    simp at xIn
    apply re_sin_pos_between xIn.left xIn.right

open Set in
theorem re_cos_strict_mono_on : StrictMonoOn re_cos (Icc π (2*π)) := by
  apply strictMonoOn_of_deriv_pos
  . exact convex_Icc _ _
  . exact re_cos_continuous.continuousOn
  . intro x xIn
    rw [re_cos_deriv.deriv]
    apply neg_pos_of_neg
    simp at xIn
    apply re_sin_neg_between xIn.left xIn.right

open Set in
theorem re_sin_strict_mono_on : StrictMonoOn re_sin (Icc (-(π/2)) (π/2)) := by
  apply strictMonoOn_of_deriv_pos
  . exact convex_Icc _ _
  . exact re_sin_continuous.continuousOn
  . intro x xIn
    rw [re_sin_deriv.deriv]
    simp at xIn
    apply re_cos_pos_between xIn.left xIn.right

open Set in
theorem re_sin_strict_anti_on : StrictAntiOn re_sin (Icc (π/2) ((3*π)/2)) := by
  apply strictAntiOn_of_deriv_neg
  . exact convex_Icc _ _
  . exact re_sin_continuous.continuousOn
  . intro x xIn
    rw [re_sin_deriv.deriv]
    simp at xIn
    apply re_cos_neg_between xIn.left xIn.right

-- polar coordinates

open Set

theorem re_cos_between_neg_one_and_one : re_cos x ∈ Icc (-1 : ℝ) 1 := by
  simp
  rw [← abs_le]
  have := re_sin_sq_plus_re_cos_sq (z := x)
  contrapose! this
  apply _root_.ne_of_gt
  rw [← abs_sq (re_cos x), pow_two (re_cos x), abs_mul, add_comm, ← one_mul 1]
  apply lt_add_of_lt_of_nonneg _ (sq_nonneg _)
  apply mul_lt_mul <;> try simp
  exact this
  linarith

noncomputable def re_cos_restricted : (Icc 0 π) → (Icc (-1 : ℝ) 1) :=
  fun ⟨x, _⟩ ↦ ⟨re_cos x, re_cos_between_neg_one_and_one⟩

theorem re_cos_restricted_eq_re_cos (x : Icc 0 π) : re_cos_restricted x = re_cos x := by
  simp [re_cos_restricted]

open Function in
theorem re_cos_restricted_bij : Bijective re_cos_restricted := by
  constructor
  . intro ⟨x, xIn⟩ ⟨y, yIn⟩
    simp [re_cos_restricted]
    exact re_cos_strict_anti_on.injOn xIn yIn
  . intro ⟨_, yIn⟩
    simp [re_cos_restricted]
    have := intermediate_value_Icc' (le_of_lt pi_pos) re_cos_continuous.continuousOn
    simp [re_cos_of_pi, re_cos_of_zero] at this
    exact this yIn

noncomputable def re_cos_restricted_equiv := Equiv.ofBijective _ re_cos_restricted_bij

noncomputable def arccos := re_cos_restricted_equiv.invFun

theorem re_cos_of_arccos (x : Icc (-1) 1) : re_cos (arccos x) = x := by
  rw [← re_cos_restricted_eq_re_cos]; congr
  apply re_cos_restricted_equiv.right_inv

theorem arccos_of_re_cos (x : Icc 0 π) : arccos ⟨re_cos x, re_cos_between_neg_one_and_one⟩ = x := by
  exact re_cos_restricted_equiv.left_inv x

theorem arccos_of_zero : arccos ⟨0, by simp⟩ = π/2 := by
  simp_rw [← re_cos_of_pi_over_two]
  conv =>
    lhs
    arg 1
    rw [arccos_of_re_cos ⟨_, by simp; constructor <;> linarith [pi_pos]⟩]

end Exp

open Exp Set

-- for z = 0, the result could be anything, here we return π/2

noncomputable def arg (z : ℂ₁) : Ioc (-π) π := by
  have le1 : |z.re/‖z‖| ≤ 1 := by
    rw [abs_div, abs_norm]
    apply div_le_one_of_le₀ (abs_re_le_norm _)
    exact norm_nonneg _
  have inIcc : z.re/‖z‖ ∈ Icc (-1) 1 := by
    simp
    exact abs_le.mp le1
  let w := arccos ⟨_, inIcc⟩
  by_cases zimnn : z.im ≥ 0
  . use w
    constructor
    . apply lt_of_lt_of_le (neg_neg_of_pos pi_pos)
      exact w.property.left
    . exact w.property.right
  . use -(w : ℝ)
    constructor
    . apply neg_lt_neg
      apply lt_of_le_of_ne
      . exact w.property.right
      . intro eq
        apply_fun re_cos at eq
        rw [re_cos_of_pi, re_cos_of_arccos] at eq
        simp at eq
        rw [div_eq_iff] at eq
        apply_fun (.^2) at eq
        rw [mul_pow, neg_one_sq, one_mul, norm_def, norm, Real.sq_sqrt] at eq
        simp at eq
        linarith
        . exact add_nonneg (sq_nonneg _) (sq_nonneg _)
        . intro eq
          apply zero_if_norm_zero at eq
          apply_fun (·.im) at eq; simp at eq
          linarith
    . apply neg_le_of_neg_le
      apply le_trans (le_of_lt (neg_neg_of_pos pi_pos))
      exact w.property.left

lemma re_cos_of_arg : re_cos (arg z) = z.re/‖z‖ := by
  by_cases zimnn : z.im ≥ 0
  . simp_rw [arg, dif_pos zimnn, re_cos_of_arccos]
  . simp_rw [arg, dif_neg zimnn, re_cos_neg, re_cos_of_arccos]

lemma re_sin_of_arg (nz : z ≠ 0) : re_sin (arg z) = z.im/‖z‖ := by
  have sin_arg_abs : |re_sin (arg z)| = |z.im/‖z‖| := by
    have sq_eq : (re_sin (arg z))^2 + (re_cos (arg z))^2 = 1 := re_sin_sq_plus_re_cos_sq
    have one_eq : 1 = ((z.re^2 + z.im^2)/‖z‖^2 : ℝ) := by
      rw [norm_def, norm, Real.sq_sqrt (add_nonneg (sq_nonneg _) (sq_nonneg _)), div_self]
      rw [← Real.sq_sqrt (add_nonneg (sq_nonneg _) (sq_nonneg _)), ← norm]
      apply ne_of_gt
      apply sq_pos_of_ne_zero
      apply (zero_if_norm_zero _).mt nz
    rw [re_cos_of_arg, div_pow, one_eq] at sq_eq
    apply eq_sub_of_add_eq at sq_eq
    simp [div_sub_div_same] at sq_eq
    rw [← div_pow] at sq_eq
    apply (sq_eq_sq_iff_abs_eq_abs _ _).mp sq_eq
  by_cases zimnn : z.im ≥ 0
  . have : (arg z).val ∈ Icc 0 π := by
      simp [arg, dif_pos zimnn]
    have : re_sin (arg z) ≥ 0 := by
      by_cases argz : (arg z).val = 0
      . rw [argz, re_sin_of_zero]
      by_cases argpi : (arg z).val = π
      . rw [argpi, re_sin_of_pi]
      apply le_of_lt
      apply re_sin_pos_between
      . apply lt_of_le_of_ne this.left
        symm
        exact argz
      . exact lt_of_le_of_ne this.right argpi
    rw [← abs_of_nonneg this, sin_arg_abs, abs_div, abs_norm]; congr
    rw [abs_of_nonneg zimnn]
  . have : -(arg z).val ∈ Icc 0 π := by
      simp [arg, dif_neg zimnn]
    have : re_sin (arg z) ≤ 0 := by
      rw [← neg_neg (arg _ : ℝ), re_sin_neg]
      simp
      by_cases argz : (arg z).val = 0
      . rw [argz, neg_zero, re_sin_of_zero]
      by_cases argpi : -(arg z).val = π
      . rw [argpi, re_sin_of_pi]
      apply le_of_lt
      apply re_sin_pos_between
      . apply lt_of_le_of_ne this.left
        symm
        exact neg_ne_zero.mpr argz
      . exact lt_of_le_of_ne this.right argpi
    rw [← neg_neg (re_sin _), ← abs_of_nonpos this, sin_arg_abs, abs_div, abs_norm, ← neg_div]
    congr
    rw [abs_of_nonpos (le_of_lt (lt_of_not_ge zimnn)), neg_neg]

theorem polar_decomp (nz : z ≠ 0) : z = ‖z‖*(exp (i*(arg z))) := by
  rw [exp_iz_eq_cos_plus_i_sin, ← re_cos_eq_cos, ← re_sin_eq_sin, re_cos_of_arg, re_sin_of_arg nz]
  push_cast
  field_simp
  rw [mul_div_cancel_left₀]; rotate_left
  . apply (r_cast_eq _ _).mp.mt
    exact (zero_if_norm_zero _).mt nz
  rw [mul_comm]
  exact complex_decomp _

noncomputable def PolarMap : ℝpos' × (Ioc (-π) π) ≃ ℂ₁ˣ where
  toFun := fun ⟨⟨r, rIn⟩, ⟨arg, _⟩⟩ ↦ by
    refine' Units.mk0 (r*(exp (i*arg))) _
    simp; push_neg; constructor
    . intro eq
      apply_fun (·.re) at eq; simp at eq
      linarith [rIn.right]
    . exact exp_ne_zero _
  invFun := fun z ↦ by
    refine' ⟨⟨‖z.val‖, _⟩, arg z⟩
    constructor <;> simp
  left_inv := by
    intro ⟨⟨r, rIn⟩, ⟨arg, argIn⟩⟩
    have norm_eq : ‖r‖*‖(exp (i*arg))‖ = r.re := by
      simp [norm_exp_one_of_imaginary]
      simp [norm_def, norm, rIn.left]
      rw [Real.sqrt_sq]
      exact le_of_lt rIn.right
    have re_eq : (exp (i*arg)).re = re_cos arg := by
      apply r_eq_by_c_eq
      rw [exp_iz_eq_cos_plus_i_sin, re_cos_eq_cos]
      ext <;> simp [rIn.left]
      . exact r_mem_iff.mp (sin_real_of_real _)
      . symm; exact r_mem_iff.mp (cos_real_of_real _)
    have im_eq : (exp (i*arg)).im = re_sin arg := by
      apply r_eq_by_c_eq
      rw [exp_iz_eq_cos_plus_i_sin, re_sin_eq_sin]
      ext <;> simp [rIn.left]
      . exact r_mem_iff.mp (cos_real_of_real _)
      . symm; exact r_mem_iff.mp (sin_real_of_real _)
    simp; constructor
    . rw [norm_eq]; ext <;> simp
      exact rIn.left.symm
    . have cancel : r.re * re_cos arg / r.re = re_cos arg := by
        rw [mul_div_cancel_left₀]
        exact ne_of_gt rIn.right
      by_cases imnn : (r*(exp (i*arg))).im ≥ 0
      . simp [Complex₁.arg, rIn.left]
        rw [dif_pos (mul_nonneg _ _)]
        . simp [re_eq, norm_eq, cancel]
          have argIn' : arg ∈ Icc 0 π := by
            simp; refine' ⟨_, argIn.right⟩
            apply le_of_not_gt
            contrapose! imnn
            simp [rIn.left]
            apply mul_neg_of_pos_of_neg rIn.right
            rw [im_eq, ← re_sin_plus_two_pi]
            apply re_sin_neg_between <;> linarith [argIn.left]
          simp_rw [arccos_of_re_cos ⟨arg, argIn'⟩]
        linarith [rIn.right]
        contrapose! imnn
        simp [rIn.left]
        exact mul_neg_of_pos_of_neg rIn.right imnn
      . simp [Complex₁.arg, rIn.left]
        rw [dif_neg]
        . simp [re_eq, norm_eq, cancel]
          have argIn' : (-arg) ∈ Icc 0 π := by
            simp; constructor
            . apply le_of_not_gt
              have imnn' := imnn
              contrapose! imnn'
              simp [rIn.left]
              apply mul_nonneg (le_of_lt rIn.right)
              rw [im_eq]
              apply le_of_lt
              apply re_sin_pos_between imnn' (lt_of_le_of_ne argIn.right _)
              contrapose! imnn
              simp [imnn, exp_iz_eq_cos_plus_i_sin, cos_of_pi, sin_of_pi]
              linarith [rIn.left]
            . linarith [argIn.left]
          have : arccos ⟨re_cos arg, re_cos_between_neg_one_and_one⟩
              = arccos ⟨re_cos (-arg), re_cos_between_neg_one_and_one⟩ := by
            congr 2
            exact re_cos_neg.symm
          simp [this, arccos_of_re_cos ⟨-arg, argIn'⟩]
        simp [rIn.right]
        contrapose! imnn
        simpa [rIn.left, rIn.right]
  right_inv := by
    intro z
    dsimp
    conv =>
      lhs
      arg 1
      simp
      rw [← polar_decomp (Units.ne_zero _)]
    simp

-- periodicity of exp

namespace Exp

lemma re_cos_of_plus_k_times_two_pi (k : ℤ) : re_cos (x + 2*π*k) = re_cos x := by
  have for_nat (y : ℝ) (n : ℕ) : re_cos (y + 2*π*n) = re_cos y := by
    induction' n with n' ih
    . simp
    . push_cast
      rwa [mul_add_one, ← add_assoc, re_cos_plus_two_pi]
  cases' k with k' k'
  . simp
    exact for_nat x k'
  . rw [← re_cos_neg]
    push_cast
    rw [mul_neg, neg_add, neg_neg]
    norm_cast
    rw [for_nat _ _, re_cos_neg]

lemma re_sin_of_plus_k_times_two_pi (k : ℤ) : re_sin (x + 2*π*k) = re_sin x := by
  have for_nat (y : ℝ) (n : ℕ) : re_sin (y + 2*π*n) = re_sin y := by
    induction' n with n' ih
    . simp
    . push_cast
      rwa [mul_add_one, ← add_assoc, re_sin_plus_two_pi]
  cases' k with k' k'
  . simp
    exact for_nat x k'
  . rw [← neg_neg (re_sin _), ← re_sin_neg]
    push_cast
    rw [mul_neg, neg_add, neg_neg]
    norm_cast
    rw [for_nat _ _, re_sin_neg, neg_neg]

lemma ones_of_re_cos : re_cos x = 1 ↔ ∃ (k : ℤ), x = 2*π*k := by
  constructor
  . intro eq
    use ⌊x/(2*π)⌋
    let xrem := x - ⌊x/(2*π)⌋*(2*π)
    have xremnn : xrem ≥ 0 := Int.sub_floor_div_mul_nonneg _ (by simp [pi_pos])
    have xremlt : xrem < 2*π := Int.sub_floor_div_mul_lt _ (by simp [pi_pos])
    apply sub_eq_zero.mp
    wlog xremnz : xrem ≠ 0
    . simp at xremnz
      rw [mul_comm]
      exact xremnz
    rw [mul_comm]
    apply eq_of_ge_of_not_gt xremnn
    contrapose! eq
    apply ne_of_lt
    have : re_cos x = re_cos xrem := by
      symm
      unfold xrem
      rw [sub_eq_add_neg, ← neg_mul, mul_comm]
      norm_cast
      apply re_cos_of_plus_k_times_two_pi
    rw [this]
    by_cases xremlepi : xrem ≤ π
    . rw [← re_cos_of_zero]
      apply re_cos_strict_anti_on <;> try simp
      . linarith [pi_pos]
      . exact ⟨xremnn, xremlepi⟩
      . exact lt_of_le_of_ne xremnn xremnz.symm
    . rw [← re_cos_of_two_pi]
      apply re_cos_strict_mono_on <;> try simp
      . refine' ⟨_, le_of_lt xremlt⟩
        simp at xremlepi
        linarith
      . linarith
      . exact xremlt
  . rintro ⟨k, rfl⟩
    rw [← zero_add (2*π*k), re_cos_of_plus_k_times_two_pi, re_cos_of_zero]

lemma re_cos_and_re_sin_eq_iff : re_cos x = re_cos y ∧ re_sin x = re_sin y ↔ (∃ (k : ℤ), y-x = 2*π*k) := by
  constructor
  . rintro ⟨cos_eq, sin_eq⟩
    apply ones_of_re_cos.mp
    rw [sub_eq_add_neg, re_cos_add, re_cos_neg, cos_eq, re_sin_neg, sin_eq]
    simp
    rw [← pow_two, ← pow_two, add_comm]
    exact re_sin_sq_plus_re_cos_sq
  . rintro ⟨k, yeq⟩
    rw [sub_eq_iff_eq_add] at yeq
    rw [yeq, add_comm]
    exact ⟨(re_cos_of_plus_k_times_two_pi k).symm, (re_sin_of_plus_k_times_two_pi k).symm⟩

open Set in
theorem exp_preimage (nz : z ≠ 0) :
    exp ⁻¹' {z} = { w | ∃ (k : ℤ), w = ⟨re_log ‖z‖, arg z + 2*π*k⟩ } := by
  ext w; simp; constructor
  . intro exp_w
    have znormpos : ‖z‖ > 0 := by
      apply lt_of_le_of_ne (norm_nonneg _) _
      symm
      exact (zero_if_norm_zero _).mt nz
    let w' : ℂ₁ := ⟨re_log ‖z‖, arg z⟩
    have exp_w' : exp w' = z :=  by
      rw [complex_decomp w', exp_add, mul_comm _ i]
      rw [exp_iz_eq_cos_plus_i_sin, ← re_exp_eq_exp, re_exp_re_log znormpos]
      rw [← exp_iz_eq_cos_plus_i_sin, ← polar_decomp nz]
    rw [complex_decomp w] at exp_w
    rw [complex_decomp w'] at exp_w'
    rw [exp_add, mul_comm _ i] at exp_w exp_w'
    rw [← exp_w'] at exp_w
    have re_eq : w.re = re_log ‖z‖ := by
      apply_fun (‖.‖) at exp_w
      have im : (i*w.im).re = 0 := by simp
      have im' : (i*w'.im).re = 0 := by simp
      rw [exp_w'] at exp_w
      simp [norm_mul, norm_exp_one_of_imaginary im, norm_exp_one_of_imaginary im', ← re_exp_eq_exp] at exp_w
      simp [re_exp_re_log znormpos, abs_of_pos (re_exp_pos _)] at exp_w
      rw [← exp_w, re_log_re_exp]
    have exp_eq : exp (i*w.im) = exp (i*w'.im) := by
      rw [re_eq] at exp_w
      apply_fun ((exp (w'.re))*.); rotate_left
      . intro x y
        simp [← re_exp_eq_exp]
        intro h
        cases' h with h h
        . exact h
        . apply_fun (·.re) at h; simp at h
          have : re_exp (re_log ‖z‖) ≠ 0 := ne_of_gt (re_exp_pos _)
          contradiction
      exact exp_w
    simp_rw [exp_iz_eq_cos_plus_i_sin, ← re_cos_eq_cos, ← re_sin_eq_sin, mul_comm i] at exp_eq
    apply complex_decomp_unique at exp_eq
    obtain ⟨k, keq⟩ := re_cos_and_re_sin_eq_iff.mp exp_eq
    use (-k); ext <;> simp [re_eq]
    linarith
  . rintro ⟨k, rfl⟩
    simp [complex_decomp ⟨re_log _, _⟩, exp_add, ← re_exp_eq_exp]
    rw [re_exp_re_log]; rotate_left
    . apply lt_of_le_of_ne (norm_nonneg _)
      symm
      exact (zero_if_norm_zero _).mt nz
    rw [mul_comm _ i, exp_iz_eq_cos_plus_i_sin, ← re_cos_eq_cos, ← re_sin_eq_sin]
    rw [re_cos_of_plus_k_times_two_pi, re_sin_of_plus_k_times_two_pi]
    rw [re_cos_eq_cos, re_sin_eq_sin, ← exp_iz_eq_cos_plus_i_sin]
    exact (polar_decomp nz).symm
