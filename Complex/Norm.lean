import Mathlib.Data.ENNReal.Basic
import Complex.Basic
import Complex.Conjugate

namespace Complex₁
open Real

noncomputable def norm (z : ℂ₁) : ℝ := sqrt (z.re^2 + z.im^2)

theorem norm_conj_stable (z : ℂ₁) : z.conj.norm = z.norm := by
  unfold norm
  rw [conj_re, conj_im, neg_sq]

theorem abs_re_diff_le_norm_diff (z w : ℂ₁) : |z.re - w.re| ≤ (z-w).norm := by
  apply le_sqrt_of_sq_le
  simp [sq_abs, sq_nonneg]

theorem abs_im_diff_le_norm_diff (z w : ℂ₁) : |z.im - w.im| ≤ (z-w).norm := by
  apply le_sqrt_of_sq_le
  simp [sq_abs, sq_nonneg]

theorem abs_re_le_norm (z : ℂ₁) : |z.re| ≤ z.norm := by
  convert abs_re_diff_le_norm_diff z 0 <;> simp

theorem abs_im_le_norm (z : ℂ₁) : |z.im| ≤ z.norm := by
  convert abs_im_diff_le_norm_diff z 0 <;> simp

theorem norm_sq_eq_prod_conj (z : ℂ₁) : z.re^2 + z.im^2 = z*z.conj := by
  ext
  . simp [pow_two]
  . simp [pow_two]
    ring

theorem norm_sq_eq_prod_conj' (z : ℂ₁) : ((norm z)^2 : ℂ₁) = (z*z.conj) := by
  unfold norm
  norm_cast
  rw [sq_sqrt (add_nonneg (sq_nonneg _) (sq_nonneg _)), ← norm_sq_eq_prod_conj]
  norm_cast

theorem norm_add_sq (z w : ℂ₁) : (norm (z + w))^2 = (norm z)^2 + (norm w)^2 + 2*(z*w.conj).re := by
  apply_fun r_inj_c
  push_cast
  rw [norm_sq_eq_prod_conj', conj_add, add_mul, mul_add, mul_add]
  have : z*w.conj + w*z.conj = 2*(z*w.conj).re := by
    have : w*z.conj = (z*w.conj).conj := by rw [conj_mul, conj_conj, mul_comm]
    rw [this, add_conj_self]
  rw [add_assoc, ← add_assoc (z*w.conj), this]
  rw [norm_sq_eq_prod_conj', norm_sq_eq_prod_conj']
  ring_nf
  congr
  . apply r_c_hom_injective

theorem norm_nonneg (z : ℂ₁) : z.norm ≥ 0 := by apply sqrt_nonneg

theorem norm_mul (z w : ℂ₁) : (z*w).norm = z.norm * w.norm := by
  apply (sqrt_eq_iff_eq_sq _ _).mpr
  apply_fun r_inj_c
  push_cast
  rw [norm_sq_eq_prod_conj (z*w), conj_mul]
  have : z*w*(z.conj*w.conj) = (z*z.conj)*(w*w.conj) := by ring
  rw [this, ← norm_sq_eq_prod_conj', ← norm_sq_eq_prod_conj', mul_pow]
  . apply r_c_hom_injective
  . apply add_nonneg <;> apply sq_nonneg
  . apply mul_nonneg <;> apply norm_nonneg

theorem norm_triangle_ineq (z w : ℂ₁) : norm (z + w) ≤ norm z + norm w := by
  apply sqrt_le_iff.mpr
  constructor
  . apply add_nonneg <;> apply sqrt_nonneg
  calc
    _ = norm (z+w)^2 := by
      unfold norm
      rw [sq_sqrt]
      apply add_nonneg <;> apply sq_nonneg
    _ ≤ z.norm^2 + w.norm^2 + 2*(z*w.conj).re := by
      rw [norm_add_sq]
    _ ≤ z.norm^2 + w.norm^2 + 2*(z.norm*w.norm) := by
      gcongr z.norm^2 + w.norm^2 + 2*?_
      rw [← norm_conj_stable w, ← norm_mul]
      exact le_of_abs_le (abs_re_le_norm _)
    _ = _ := by rw [add_pow_two]; ring

theorem zero_if_norm_zero (z : ℂ₁) : z.norm = 0 → z = 0 := by
  intro iszero
  apply_fun (.^2) at iszero
  rw [zero_pow (by norm_num)] at iszero
  apply_fun r_inj_c at iszero
  push_cast at iszero
  rw [norm_sq_eq_prod_conj'] at iszero
  rcases mul_eq_zero.mp iszero with (zz | zcz)
  . assumption
  . have h' := zcz
    apply_fun (·.re) at h'
    simp at h'
    have h'' := zcz
    apply_fun (·.im) at h''
    simp at h''
    ext <;> assumption

@[reducible]
noncomputable def dist (z w : ℂ₁) := norm (z-w)

noncomputable instance : NontriviallyNormedField ℂ₁ where
  norm := norm
  dist := dist
  dist_self := by
    intro x
    dsimp [dist, norm]
    field_simp
  dist_comm := by
    intro x y
    dsimp [dist, norm]
    rw [← neg_sub, neg_pow_two, ← neg_sub x.im, neg_pow_two]
  dist_triangle := by
    intro x y z
    simp [dist]
    have : x-z = (x-y)+(y-z) := by ring
    rw [this]
    apply norm_triangle_ineq
  edist_dist := by
    intro x y
    apply ENNReal.coe_nnreal_eq
  eq_of_dist_eq_zero := by
    intro x y distzero
    apply eq_of_sub_eq_zero
    apply zero_if_norm_zero
    assumption
  dist_eq := by simp
  norm_mul' := norm_mul
  non_trivial := ⟨⟨2, 0⟩, by simp [norm]⟩

theorem norm_def (z : ℂ₁) : ‖z‖ = norm z := rfl

@[simp, norm_cast]
theorem nat_norm_eq (n : ℕ) : ‖(n : ℂ₁)‖ = (n : ℝ) := by simp [norm_def, norm]
@[simp, norm_cast]
theorem r_norm_eq (r : ℝ) : ‖(r : ℂ₁)‖ = |(r : ℝ)| := by simp [norm_def, norm, sqrt_sq_eq_abs]

theorem norm_eq_re_abs_of_im_zero {z : ℂ₁} (imz : z.im = 0) : ‖z‖ = |z.re| := by
  simp [norm_def, norm, imz, sqrt_sq_eq_abs]

theorem norm_eq_im_abs_of_re_zero {z : ℂ₁} (rez : z.re = 0) : ‖z‖ = |z.im| := by
  simp [norm_def, norm, rez, sqrt_sq_eq_abs]

noncomputable instance : Algebra ℝ ℂ₁ := { r_c_hom with
  algebraMap := r_c_hom
  smul_def' := c_smul_eq_mul_cast
  commutes' := by
    intro r z
    simp [mul_comm]
}

instance normedSpaceC : NormedSpace ℝ ℂ₁ where
  norm_smul_le := by
    intro r z
    simp [c_smul_eq_mul_cast]

noncomputable instance : NormedAlgebra ℝ ℂ₁ where
  norm_smul_le := normedSpaceC.norm_smul_le

theorem inv_of_conj_div_norm_sq {z : ℂ₁} (hz : z ≠ 0) : z⁻¹ = z.conj / ‖z‖^2 := by
  apply inv_eq_of_mul_eq_one_right
  rw [mul_div]
  apply (div_eq_iff _).mpr
  symm
  rw [one_mul, norm_def, norm_sq_eq_prod_conj']
  . intro contra
    have contra : ‖z‖ = (0 : ℂ₁) := by rwa [pow_eq_zero_iff (by norm_num)] at contra
    have contra : ‖z‖ = (0 : ℝ) := r_c_hom_injective contra
    have contra : z = 0 := norm_eq_zero.mp contra
    contradiction

theorem norm_i : ‖i‖ = 1 := by simp [norm_def, norm]

theorem norm_le_abs_re_plus_abs_im (z : ℂ₁) : ‖z‖ ≤ |z.re|+|z.im| := by
  nth_rw 1 [complex_decomp z]
  convert norm_triangle_ineq _ _
  . simp [← norm_eq_abs, norm, sqrt_sq_eq_abs]
  . simp [← norm_eq_abs, norm_mul, norm, sqrt_sq_eq_abs]

theorem c_norm_extends_r_norm (a : ℝ) : ‖(a : ℝ)‖ = ‖(a : ℂ₁)‖ := by
  simp [Norm.norm, norm]
  rw [sqrt_sq_eq_abs]

theorem c_no_order [LinearOrderedField F] : IsEmpty (ℂ₁ ≃+* F) := by
  apply isEmpty_iff.mpr
  intro iso

  have sq_pos : ∀ z, z ≠ 0 → iso (z^2) > 0 := by
    intro z znez
    have fznez : iso z ≠ 0 := by
      intro contra
      have zez : z = 0 := by
        apply iso.injective
        rw [iso.map_zero]
        exact contra
      contradiction
    rw [iso.map_pow, pow_two]
    rcases lt_trichotomy 0 (iso z) with gt | eq | lt
    . exact mul_pos gt gt
    . exfalso
      exact fznez eq.symm
    . exact mul_pos_of_neg_of_neg lt lt

  have isq_pos : iso (i^2) > 0 := by
    apply sq_pos
    intro contra
    apply_fun (·.im) at contra
    simp [i] at contra

  have onesq_pos : iso (1^2) > 0 := by
    apply sq_pos
    norm_num

  have : (0 : F)  < 0 := by
    calc
      0 = iso (i^2) + iso (1^2) := by simp [i_sq]
      _ > 0 := by linarith

  exact @lt_irrefl F _ 0 this
