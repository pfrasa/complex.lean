import Mathlib.Topology.Basic
import Mathlib.Topology.Defs.Filter
import Mathlib.Topology.UniformSpace.UniformEmbedding
import Mathlib.Topology.MetricSpace.Sequences
import Complex.Basic
import Complex.Norm

namespace Complex₁

open Topology Filter Homeomorph

@[reducible]
def c_r2_map : ℂ₁ → ℝ×ℝ := fun ⟨a, b⟩ ↦ ⟨a, b⟩
@[reducible]
def c_r2_map_inv : ℝ×ℝ → ℂ₁ := fun ⟨a, b⟩ ↦ ⟨a, b⟩

def c_r2_map_uniform_continuous : UniformContinuous c_r2_map := by
  apply Metric.uniformContinuous_iff.mpr
  intro ε ε0
  refine' ⟨ε, ε0, _⟩
  intro z w dzw
  simp [Dist.dist, dist] at *
  constructor
  . exact lt_of_le_of_lt (abs_re_diff_le_norm_diff _ _) dzw
  . exact lt_of_le_of_lt (abs_im_diff_le_norm_diff _ _) dzw

def c_r2_map_inv_uniform_continuous : UniformContinuous c_r2_map_inv := by
  apply Metric.uniformContinuous_iff.mpr
  intro ε ε0
  simp [Dist.dist, dist, norm]
  let δ := ε / Real.sqrt 2
  have δpos : δ > 0 := div_pos ε0 (by norm_num)
  have δsqeq : ε^2 / 2 = δ^2 := by rw [div_pow, Real.sq_sqrt (by norm_num)]
  refine' ⟨δ, δpos, _⟩
  intro x1 x2 y1 y2 dxyre dxyim
  apply (Real.sqrt_lt _ _).mpr
  . have ineqre : (x1-y1)^2 < ε^2 / 2 := by
      rw [δsqeq]
      apply sq_lt_sq.mpr
      rw [abs_of_pos δpos]
      exact dxyre
    have ineqim : (x2-y2)^2 < ε^2 / 2 := by
      rw [δsqeq]
      apply sq_lt_sq.mpr
      rw [abs_of_pos δpos]
      exact dxyim
    linarith
  . linarith [sq_nonneg (x1 - y1), sq_nonneg (x2 - y2)]
  . linarith

def c_r2_map_uniform : IsUniformInducing c_r2_map := by
  apply Metric.isUniformInducing_iff.mpr
  refine' ⟨c_r2_map_uniform_continuous, _⟩
  have h := c_r2_map_inv_uniform_continuous
  simp [Metric.uniformContinuous_iff, c_r2_map_inv] at h
  intro ε ε0
  obtain ⟨δ, δ0, h'⟩ := h ε ε0
  refine' ⟨δ, δ0, _⟩
  intro x y
  exact h' x.re x.im y.re y.im

def c_hom_r2 : ℂ₁ ≃ₜ ℝ×ℝ where
  toFun := c_r2_map
  invFun := c_r2_map_inv
  left_inv := by intro z; simp
  right_inv := by intro z; simp
  continuous_toFun := c_r2_map_uniform_continuous.continuous
  continuous_invFun := c_r2_map_inv_uniform_continuous.continuous

lemma re_eq_fst_comp_c_hom_r2 : re = Prod.fst ∘ c_hom_r2 := by rfl
lemma im_eq_snd_comp_c_hom_r2 : im = Prod.snd ∘ c_hom_r2 := by rfl

theorem tendsto_iff_tendsto_re_and_im (f : Filter X) (m : X → ℂ₁) (z : ℂ₁) :
    Tendsto m f (𝓝 z) ↔ Tendsto (Complex₁.re ∘ m) f (𝓝 z.re) ∧ Tendsto (Complex₁.im ∘ m) f (𝓝 z.im) := by
  rw [nhds_eq_comap c_hom_r2, tendsto_comap_iff, nhds_prod_eq]
  simp_rw [Tendsto, SProd.sprod, re_eq_fst_comp_c_hom_r2, im_eq_snd_comp_c_hom_r2, ← map_map]
  constructor
  . intro h
    obtain ⟨le_fst, le_snd⟩ := le_inf_iff.mp h
    exact ⟨le_trans (map_mono le_fst) map_comap_le, le_trans (map_mono le_snd) map_comap_le⟩
  . intro h
    apply le_inf_iff.mpr
    exact ⟨map_le_iff_le_comap.mp h.left, map_le_iff_le_comap.mp h.right⟩

theorem tendsto_iff_tendsto_conj (f : Filter X) (m : X → ℂ₁) (z : ℂ₁) :
    Tendsto m f (𝓝 z) ↔ Tendsto (conj ∘ m) f (𝓝 z.conj) := by
  have re_conj : re ∘ conj ∘ m = re ∘ m := by rfl
  have im_conj : im ∘ conj ∘ m = Neg.neg ∘ im ∘ m := by rfl
  simp_rw [tendsto_iff_tendsto_re_and_im, re_conj, im_conj]
  constructor
  . intro ⟨tre, tim⟩
    exact ⟨tre, (tendsto_neg z.im).comp tim⟩
  . intro ⟨tre, tim⟩
    refine' ⟨tre, _⟩
    have : im ∘ m = Neg.neg ∘ Neg.neg ∘ im ∘ m := by ext; simp
    rw [this]
    simp at tim
    exact (neg_neg z.im ▸ tendsto_neg (-z.im)).comp tim

@[continuity]
theorem re_continuous : Continuous re := by
  apply SeqContinuous.continuous
  intro s z tt
  exact ((tendsto_iff_tendsto_re_and_im _ _ _).mp tt).left

@[continuity]
theorem im_continuous : Continuous im := by
  apply SeqContinuous.continuous
  intro s z tt
  exact ((tendsto_iff_tendsto_re_and_im _ _ _).mp tt).right

@[continuity]
theorem conj_continuous : Continuous conj := by
  apply SeqContinuous.continuous
  intro s z
  apply (tendsto_iff_tendsto_conj _ _ _).mp

@[continuity]
theorem r_inj_c_continuous : Continuous r_inj_c := by
  apply SeqContinuous.continuous
  intro s z tt
  apply Metric.tendsto_atTop.mpr
  intro ε ε0
  obtain ⟨N, dist_lt⟩ := Metric.tendsto_atTop.mp tt ε ε0
  use N
  intro n nge
  convert dist_lt n nge
  simp [dist_eq_norm]
  norm_cast

theorem cauchy_iff_r2_cauchy (f : Filter ℂ₁) : Cauchy f ↔ Cauchy (map c_hom_r2 f) :=
  c_r2_map_uniform.cauchy_map_iff.symm

theorem cauchy_iff_re_and_im_cauchy (f : Filter ℂ₁) : Cauchy f ↔ Cauchy (map re f) ∧ Cauchy (map im f) := by
  simp_rw [cauchy_iff_r2_cauchy, re_eq_fst_comp_c_hom_r2, im_eq_snd_comp_c_hom_r2, ← map_map]
  exact cauchy_prod_iff

theorem cauchy_seq_iff_re_and_im_cauchy_seq (s : ℕ → ℂ₁) : CauchySeq s ↔ CauchySeq (re ∘ s) ∧ CauchySeq (im ∘ s) := by
  apply cauchy_iff_re_and_im_cauchy

instance : CompleteSpace ℂ₁ := by
  apply (completeSpace_iff_isComplete_range c_r2_map_uniform).mpr
  have : c_r2_map = c_hom_r2 := by rfl
  rw [this, Set.range_eq_univ.mpr c_hom_r2.surjective]
  exact complete_univ

instance : ProperSpace ℂ₁ where
  isCompact_closedBall := by
    intro z r
    apply IsSeqCompact.isCompact
    intro s sInBall
    let re_interval := Set.Icc (z.re - r) (z.re + r)
    let im_interval := Set.Icc (z.im - r) (z.im + r)
    have re_in_bound : ∀ n, (re ∘ s) n ∈ re_interval := by
      intro n
      have ineq_c := sInBall n
      simp [Dist.dist, dist] at ineq_c
      have : |(s n).re - z.re| ≤ r := le_trans (abs_re_diff_le_norm_diff _ _) ineq_c
      rw [abs_le] at this
      simp
      constructor <;> linarith
    obtain ⟨x, _, ⟨φ, φmono, xconv⟩⟩ := tendsto_subseq_of_bounded (Metric.isBounded_Icc _ _) re_in_bound
    have im_in_bound : ∀ n, (im ∘ s ∘ φ) n ∈ im_interval := by
      intro n
      have ineq_c := sInBall (φ n)
      simp [Dist.dist, dist] at ineq_c
      have : |(s (φ n)).im - z.im| ≤ r := le_trans (abs_im_diff_le_norm_diff _ _) ineq_c
      rw [abs_le] at this
      simp
      constructor <;> linarith
    obtain ⟨y, _, ⟨ψ, ψmono, yconv⟩⟩ := tendsto_subseq_of_bounded (Metric.isBounded_Icc _ _) im_in_bound
    have conv : Tendsto (s ∘ φ ∘ ψ) atTop (𝓝 ⟨x, y⟩) := by
      simp [tendsto_iff_tendsto_re_and_im]
      exact ⟨xconv.comp ψmono.tendsto_atTop, yconv⟩
    refine' ⟨⟨x, y⟩, _, ⟨φ ∘ ψ, φmono.comp ψmono, conv⟩⟩
    have scl : IsSeqClosed (Metric.closedBall z r) := Metric.isClosed_ball.isSeqClosed
    have sInBall' : ∀ n, (s ∘ φ ∘ ψ) n ∈ Metric.closedBall z r := by
      intro n
      exact sInBall (φ (ψ n))
    exact scl sInBall' conv

theorem r_closed : IsClosed (ℝ' : Set ℂ₁) where
  isOpen_compl := by
    apply Metric.isOpen_iff.mpr
    intro x xnr
    simp at xnr
    apply r_mem_iff.mpr.mt at xnr
    let ε := Dist.dist x.im 0
    refine' ⟨ε, dist_pos.mpr xnr, _⟩
    intro z zball
    simp at *
    rw [r_mem_iff]
    suffices Dist.dist z.im x.im < ε by
      intro zimz
      rw [zimz, dist_comm] at this
      linarith
    rw [Real.dist_eq]
    rw [dist_eq_norm] at zball
    exact lt_of_le_of_lt (abs_im_diff_le_norm_diff _ _) zball

theorem rnn_closed : IsClosed (ℝnn' : Set ℂ₁) where
  isOpen_compl := by
    apply Metric.isOpen_iff.mpr
    intro x xnrpos
    by_cases xnr : x ∈ (ℝ' : Set ℂ₁)ᶜ
    . obtain ⟨ε, εpos, ball_sub⟩ := Metric.isOpen_iff.mp r_closed.isOpen_compl x xnr
      refine' ⟨ε, εpos, _⟩
      apply subset_trans ball_sub
      intro y ynr
      simp at ynr ⊢
      contrapose! ynr
      obtain ⟨y', _, rfl⟩ := ynr
      use y'
    . simp at xnr xnrpos
      apply r_mem_iff.mp at xnr
      apply rnn_mem_iff.mpr.mt at xnrpos
      push_neg at xnrpos
      have xneg := xnrpos xnr
      let ε := Dist.dist x.re 0
      refine' ⟨ε, dist_pos.mpr (ne_of_lt xneg), _⟩
      intro z zball
      simp [rnn_mem_iff] at zball ⊢
      intro zimz
      suffices Dist.dist z.re x.re < ε by
        have xreeq : x.re = -ε := by
          have : ε = Dist.dist x.re 0 := rfl
          simp [this, dist, abs_of_neg xneg]
        rw [Real.dist_eq, abs_lt, xreeq, sub_neg_eq_add] at this
        apply lt_of_not_ge
        intro zregez
        have : ε > ε := lt_of_le_of_lt (le_add_of_nonneg_left zregez) this.2
        linarith
      rw [Real.dist_eq]
      rw [dist_eq_norm] at zball
      exact lt_of_le_of_lt (abs_re_diff_le_norm_diff _ _) zball
