import Mathlib.Data.Real.StarOrdered
import Complex.Norm

namespace Complex₁.Differentiation

-- differentiation of functions ℝ → ℂ₁

open Filter in
theorem hasDerivAt_r_c_of_components_hasDerivAt {f g : ℝ → ℝ}
    (hf : HasDerivAt f f' x) (hg : HasDerivAt g g' x) :
    HasDerivAt (fun x ↦ (⟨f x, g x⟩ : ℂ₁)) ⟨f', g'⟩ x := by
  simp_rw [hasDerivAt_iff_tendsto, sub_def, c_smul_def, c_smul]
  have gt : Function.const ℝ 0 ≤
      fun x' ↦ ‖x'-x‖⁻¹*‖(⟨f x'-f x-(x'-x)*f', g x'-g x-(x'-x)*g'⟩ : ℂ₁)‖ := by
    intro t; simp
    apply mul_nonneg
    apply inv_nonneg.mpr (abs_nonneg _)
    apply norm_nonneg
  have lt : (fun x' ↦ ‖x'-x‖⁻¹*‖(⟨f x'-f x-(x'-x)*f', g x'-g x-(x'-x)*g'⟩ : ℂ₁)‖)
      ≤ fun x' ↦ ‖x'-x‖⁻¹*‖f x'-f x-(x'-x)*f'‖ + ‖x'-x‖⁻¹*‖g x'-g x-(x'-x)*g'‖:= by
    intro t; simp
    rw [← mul_add]
    apply mul_le_mul_of_nonneg_left; rotate_left
    . exact inv_nonneg.mpr (abs_nonneg _)
    exact norm_le_abs_re_plus_abs_im _
  apply tendsto_of_tendsto_of_tendsto_of_le_of_le _ _ gt lt
  . apply tendsto_const_nhds
  have : (0 : ℝ) = 0 + 0 := by ring
  rw [this]
  exact (hasDerivAt_iff_tendsto.mp hf).add (hasDerivAt_iff_tendsto.mp hg)

theorem hasDerivAt_components_of_hasDerivAt_r_c {f : ℝ → ℂ₁} (hf : HasDerivAt f f' x) :
    HasDerivAt (re ∘ f) f'.re x ∧ HasDerivAt (im ∘ f) f'.im x := by
  simp_rw [hasDerivAt_iff_tendsto, smul_eq_mul]
  simp_rw [hasDerivAt_iff_tendsto, sub_def, c_smul_def, c_smul] at hf
  constructor
  . have gt : Function.const ℝ 0 ≤ fun x' ↦ ‖x'-x‖⁻¹*‖(re ∘ f) x' - (re ∘ f) x - (x' - x)*f'.re‖ := by
      intro t; simp
      apply mul_nonneg
      apply inv_nonneg.mpr (abs_nonneg _)
      apply abs_nonneg
    have lt : (fun x' ↦ ‖x'-x‖⁻¹*‖(re ∘ f) x' - (re ∘ f) x - (x' - x)*f'.re‖) ≤
        fun x' ↦ ‖x'-x‖⁻¹*‖(⟨(f x').re-(f x).re-(x'-x)*f'.re, (f x').im-(f x).im-(x'-x)*f'.im⟩ : ℂ₁)‖ := by
      intro t; simp
      apply mul_le_mul_of_nonneg_left; rotate_left
      . exact inv_nonneg.mpr (abs_nonneg _)
      convert abs_re_le_norm _
      simp
    exact tendsto_of_tendsto_of_tendsto_of_le_of_le tendsto_const_nhds hf gt lt
  . have gt : Function.const ℝ 0 ≤ fun x' ↦ ‖x'-x‖⁻¹*‖(im ∘ f) x' - (im ∘ f) x - (x' - x)*f'.im‖ := by
      intro t; simp
      apply mul_nonneg
      apply inv_nonneg.mpr (abs_nonneg _)
      apply abs_nonneg
    have lt : (fun x' ↦ ‖x'-x‖⁻¹*‖(im ∘ f) x' - (im ∘ f) x - (x' - x)*f'.im‖) ≤
        fun x' ↦ ‖x'-x‖⁻¹*‖(⟨(f x').re-(f x).re-(x'-x)*f'.re, (f x').im-(f x).im-(x'-x)*f'.im⟩ : ℂ₁)‖ := by
      intro t; simp
      apply mul_le_mul_of_nonneg_left; rotate_left
      . exact inv_nonneg.mpr (abs_nonneg _)
      convert abs_im_le_norm _
      simp
    exact tendsto_of_tendsto_of_tendsto_of_le_of_le tendsto_const_nhds hf gt lt

theorem hasDerivAt_r_inj_c : HasDerivAt r_inj_c 1 x :=
  hasDerivAt_r_c_of_components_hasDerivAt (hasDerivAt_id _) (hasDerivAt_const _ _)

end Complex₁.Differentiation

open Complex₁ Complex₁.Differentiation

theorem HasDerivAt.rccomp {f : ℝ → ℝ} {g : ℝ → ℂ₁} (hg : HasDerivAt g g' (f x)) (hf : HasDerivAt f f' x) :
    HasDerivAt (g ∘ f) (g' * f') x := by
  apply hasDerivAt_r_c_of_components_hasDerivAt <;> simp [r_cast_im]
  . exact (hasDerivAt_components_of_hasDerivAt_r_c hg).left.comp x hf
  . exact (hasDerivAt_components_of_hasDerivAt_r_c hg).right.comp x hf

theorem deriv_rccomp {f : ℝ → ℝ} {g : ℝ → ℂ₁} (hg : ∃ g', HasDerivAt g g' (f x))
    (hf : ∃ f', HasDerivAt f f' x) : deriv (g ∘ f) x = deriv g (f x) * deriv f x := by
  obtain ⟨g', geq⟩ := hg
  obtain ⟨f', feq⟩ := hf
  rw [geq.deriv, feq.deriv, (geq.rccomp feq).deriv]

theorem HasDerivWithinAt.rccomp {f : ℝ → ℝ} {g : ℝ → ℂ₁} (hg : HasDerivWithinAt g g' (f '' S) (f x))
    (hf : HasDerivWithinAt f f' S x) : HasDerivWithinAt (g ∘ f) (g' * f') S x := by
  let L := (ContinuousLinearMap.id ℝ ℝ).smulRight (g' * f')
  have : g' * f' = L 1 := by simp [L]
  rw [this]
  apply HasFDerivWithinAt.hasDerivWithinAt
  convert HasFDerivWithinAt.comp x hg hf (Set.mapsTo_image f S)
  simp [L]
  apply ContinuousLinearMap.ext_ring
  simp [c_smul_def, c_smul]; ext; simp; ring; simp; ring

theorem derivWithin_rccomp {f : ℝ → ℝ} {g : ℝ → ℂ₁}
    (hg : ∃ g', HasDerivWithinAt g g' (f '' S) (f x)) (hf : ∃ f', HasDerivWithinAt f f' S x)
    (uqS : UniqueDiffWithinAt ℝ S x) (uqfS : UniqueDiffWithinAt ℝ (f '' S) (f x)) :
    derivWithin (g ∘ f) S x = derivWithin g (f '' S) (f x) * derivWithin f S x := by
  obtain ⟨g', geq⟩ := hg
  obtain ⟨f', feq⟩ := hf
  rw [geq.derivWithin uqfS, feq.derivWithin uqS, (geq.rccomp feq).derivWithin uqS]
