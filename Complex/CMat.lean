import Mathlib.Tactic
import Mathlib.Algebra.Field.Basic
import Complex.Basic

namespace CMat

-- C as matrices
def c_mat : Subring (Matrix (Fin 2) (Fin 2) ℝ) where
  carrier := fun m ↦ m 0 0 = m 1 1 ∧ m 0 1 = -(m 1 0)
  mul_mem' := by
    intro a b aeq beq
    constructor <;>
      (simp [Matrix.mul_apply]
       rw [aeq.right, beq.right, ← aeq.left, ← beq.left]
       linarith)
  one_mem' := by constructor <;> simp
  add_mem' := by
    intro a b aeq beq
    constructor <;> simp
    . rw [← aeq.left, ← beq.left]
    . rw [aeq.right, beq.right]
      linarith
  zero_mem' := by constructor <;> simp
  neg_mem' := by
    intro a aeq
    constructor <;> simp
    . rw [aeq.left]
    . simp [aeq.right]

def c_bi_mat (z : ℂ₁) : c_mat where
  val := fun
    | 0, 0 => z.re
    | 0, 1 => z.im
    | 1, 0 => -z.im
    | 1, 1 => z.re
  property := by constructor <;> simp

theorem c_mat_prop (m : c_mat) : m.val 0 0 = m.val 1 1 ∧ m.val 0 1 = -(m.val 1 0) := by
  constructor
  . rw [m.property.left]
  . rw [m.property.right]

def c_bi_mat_hom : RingHom ℂ₁ c_mat where
  toFun := c_bi_mat
  map_one' := by
    simp [c_bi_mat]
    ext i j
    rcases i with ⟨i', _⟩
    rcases j with ⟨j', _⟩
    interval_cases i' <;> (interval_cases j' <;> simp)
  map_mul' := by
    intro z w
    simp [c_bi_mat, Subtype.ext_iff]
    ext i j
    rcases i with ⟨i', _⟩
    rcases j with ⟨j', _⟩
    interval_cases i' <;> (interval_cases j' <;> simp [Matrix.mul_apply]; linarith)
  map_zero' := by
    simp [c_bi_mat, Subtype.ext_iff]
    ext i j
    rcases i with ⟨i', _⟩
    rcases j with ⟨j', _⟩
    interval_cases i' <;> (interval_cases j' <;> simp)
  map_add' := by
    intro z w
    simp [c_bi_mat, Subtype.ext_iff]
    ext i j
    rcases i with ⟨i', _⟩
    rcases j with ⟨j', _⟩
    interval_cases i' <;> (interval_cases j' <;> simp)
    linarith

theorem c_bi_mat_iso : Function.Bijective c_bi_mat_hom where
  left := by
    intro z w feq
    simp [c_bi_mat_hom, c_bi_mat] at feq
    ext
    . rw [funext_iff] at feq
      have := feq 0
      rw [funext_iff] at this
      have := this 0
      simp at this
      assumption
    . rw [funext_iff] at feq
      have := feq 0
      rw [funext_iff] at this
      have := this 1
      simp at this
      assumption
  right := by
    intro m
    use ⟨m.val 0 0, m.val 0 1⟩
    rw [c_bi_mat_hom, Subtype.ext_iff]
    ext i j
    simp [c_bi_mat]
    rcases i with ⟨i', _⟩
    rcases j with ⟨j', _⟩
    interval_cases i' <;> (interval_cases j' <;> simp [c_mat_prop])

theorem is_field_of_ring_iso_from_field [Field F] [Ring R] (f : RingHom F R) (is_iso : Function.Bijective f) : IsField R where
  exists_pair_ne := by
    obtain ⟨zero, one, neq⟩ := exists_pair_ne F
    use f zero, f one
    contrapose! neq
    apply is_iso.left neq
  mul_comm := by
    intro x y
    have is_iso_f : Function.Bijective f.toFun := by simp [is_iso]
    obtain ⟨f₁x, rfl⟩ := is_iso_f.right x
    obtain ⟨f₁y, rfl⟩ := is_iso_f.right y
    have : f₁x * f₁y = f₁y * f₁x := mul_comm _ _
    rw [← f.map_mul', ← f.map_mul', this]
  mul_inv_cancel := by
    intro x xnez
    have is_iso_f : Function.Bijective f.toFun := by simp [is_iso]
    obtain ⟨f₁x, rfl⟩ := is_iso_f.right x
    have f₁xnez : f₁x ≠ 0 := by
      contrapose! xnez
      rw [xnez, f.map_zero']
    obtain ⟨c, f₁eq⟩ := IsField.mul_inv_cancel (Field.toIsField F) f₁xnez
    use f.toFun c
    rw [← f.map_mul', f₁eq, f.map_one']

noncomputable instance : Field c_mat := IsField.toField (is_field_of_ring_iso_from_field c_bi_mat_hom c_bi_mat_iso)
