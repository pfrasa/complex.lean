import General.PowerSeries
import Complex.Norm
import Complex.Topology

namespace Complex₁.PowerSeries

open Complex₁ General General.Series General.PowerSeries

theorem powerSeries_conj_eq_conj_powerSeries {ps : General.PowerSeries ℂ₁} (coeff_real : ∀ n, ps.coeff n ∈ ℝ') (center_real : ps.center ∈ ℝ') :
    ps.series_at ∘ conj = fun z n ↦ conj (ps.series_at z n) := by
  funext z n
  simp [series_at, conj_eq_conj_auto]
  rw [← conj_eq_conj_auto, (r_fixpoint_set_of_conj _).mpr (coeff_real n), (r_fixpoint_set_of_conj _).mpr center_real]

theorem powerSeries_conj_convergesTo {ps : General.PowerSeries ℂ₁} (coeff_real : ∀ n, ps.coeff n ∈ ℝ') (center_real : ps.center ∈ ℝ') :
    ∀ z w, SeriesConvergesTo (ps.series_at z) w ↔ SeriesConvergesTo (ps.series_at (conj z)) (conj w) := by
  suffices ∀ z w, SeriesConvergesTo (ps.series_at z) w → SeriesConvergesTo (ps.series_at (conj z)) (conj w) by
    intro z w
    refine' ⟨this z w, _⟩
    intro h
    rw [← conj_conj z, ← conj_conj w]
    exact this _ _ h
  intro z w h
  have : ps.series_at (conj z) = (ps.series_at ∘ conj) z := rfl
  rw [this, powerSeries_conj_eq_conj_powerSeries coeff_real center_real]
  simp
  rw [SeriesConvergesTo] at *
  nth_rw 1 [conj_eq_conj_auto]
  simp_rw [← map_sum, ← conj_eq_conj_auto]
  exact conj_continuous.seqContinuous h

theorem powerSeries_conj_isum {ps : General.PowerSeries ℂ₁} (coeff_real : ∀ n, ps.coeff n ∈ ℝ') (center_real : ps.center ∈ ℝ') :
    (∑ᵢ ps.series_at z.conj) = (∑ᵢ ps.series_at z).conj := by
  by_cases conv : SeriesConvergent (ps.series_at z)
  . obtain ⟨w, convto_w⟩ := conv
    have convto_conj : SeriesConvergesTo (ps.series_at (conj z)) (conj w) :=
      (powerSeries_conj_convergesTo coeff_real center_real _ _).mp convto_w
    rw [convto_w.get_isum, convto_conj.get_isum]
  . have conv' : SeriesDivergent (ps.series_at (conj z)) := by
      rintro ⟨w, convto⟩
      rw [← conj_conj w] at convto
      have : SeriesConvergent (ps.series_at z ) := ⟨_, (powerSeries_conj_convergesTo coeff_real center_real _ _).mpr convto⟩
      contradiction
    rw [SeriesDivergent.get_isum conv, SeriesDivergent.get_isum conv', conj]
    ext <;> simp
