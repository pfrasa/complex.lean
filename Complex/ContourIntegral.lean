import General.PowerSeries
import Complex.IntervalIntegral
import Complex.Contour
import Complex.Topology

open Complex₁ Set

open scoped unitInterval

def Complex₁.CurveIntegrable (f : ℂ₁ → ℂ₁) (γ : Curve a b) :=
  IntervalIntegrableC ((f ∘ γ.path.extend) * γ.deriv) 0 1

theorem ContinuousOn.curveIntegrable
    {γ : Curve a b} (hf : ContinuousOn f (Set.range γ)) : CurveIntegrable f γ := by
  apply ContinuousOn.intervalIntegrableC
  apply ContinuousOn.mul _ γ.derivContinuous'.continuousOn
  apply hf.comp
  . rw [continuousOn_iff_continuous_restrict]
    have : uIcc 0 1 = I := by simp
    rw [this]
    convert γ.path.continuous_toFun
    funext x; simp
  . rw [MapsTo]
    intro x xIn; simp at xIn
    refine' ⟨⟨x, xIn⟩, _⟩
    rw [Path.extend_extends]
    rfl

theorem Continuous.curveIntegrable {γ : Curve a b} (hf : Continuous f) : CurveIntegrable f γ :=
  hf.continuousOn.curveIntegrable

-- no need to add theorems for add etc., as we'll only deal with continuous functions,
-- so what we have is enough
theorem Complex₁.CurveIntegrable.curveIntegrable_const : CurveIntegrable (fun _ ↦ c) γ :=
  continuous_const.curveIntegrable

def Complex₁.ContourIntegrable (f : ℂ₁ → ℂ₁) (γ : Contour a b) := ∀ i, CurveIntegrable f (γ.curves i)

theorem ContinuousOn.contourIntegrable
    {γ : Contour a b} (hf : ∀ i, ContinuousOn f (Set.range (γ.curves i))) : ContourIntegrable f γ :=
  fun i ↦ (hf i).curveIntegrable

theorem Continuous.contourIntegrable {γ : Contour a b} (hf : Continuous f) : ContourIntegrable f γ :=
  fun _ ↦ hf.curveIntegrable

theorem Complex₁.ContourIntegrable.contourIntegrable_const : ContourIntegrable (fun _ ↦ c) γ :=
  continuous_const.contourIntegrable

namespace Complex₁

noncomputable def curveIntegral (f : ℂ₁ → ℂ₁) (γ : Curve a b) :=
  intervalIntegralC ((f ∘ γ.path.extend) * γ.deriv) 0 1

notation "∮'" γ ", " f => curveIntegral f γ

namespace curveIntegral

@[simp]
theorem integral_cast {γ : Curve a b} : (∮'γ.cast ha hb, f) = ∮'γ, f := rfl

@[simp]
theorem integral_zero : (∮'γ, 0) = 0 := by
  simp [curveIntegral]
  exact intervalIntegralC.integral_zero

open Topology intervalIntegralC in
@[simp]
theorem integral_reverse {γ : Curve a b} (hf : ContinuousOn f (range γ)) : (∮'γ.reverse, f) = -(∮'γ, f) := by
  let φ : ℝ → ℝ := fun t ↦ 1-t

  have φderiv : ∀ x, HasDerivAt φ (-1) x := by
    intro x
    have : (-1 : ℝ) = 0-1 := by ring
    rw [this]
    apply (hasDerivAt_const _ _).sub (hasDerivAt_id _)

  have φmap : φ '' I = I := by funext; simp [φ]

  have negone : ((-1 : ℝ) : ℂ₁) = (-1 : ℂ₁) := by push_cast; congr

  have eqOn : EqOn (f ∘ γ.reverse.path.extend * γ.reverse.deriv)
      (fun x ↦ f (γ.path.extend (φ x)) * γ.deriv (φ x) * (-1)) (uIcc 0 1) := by
    intro x xIn
    dsimp
    rw [mul_assoc]
    congr 1
    . simp [Curve.reverse, Path.symm, φ]
      rw [Path.extend_extends, Path.extend_extends]
      simp [unitInterval.symm]
      . convert (xSymmIn _) using 1
        convert xIn using 1
        simp
      . convert xIn using 1
        simp
    . simp [Curve.reverse, Path.symm, φ, Curve.deriv]
      rw [IccExtend_of_mem, IccExtend_of_mem]; rotate_left
      . apply xSymmIn
        convert xIn
        simp
      . convert xIn using 1
        simp
      simp
      rw [derivWithin_restrict_path_extend_eq (f := γ.path.extend ∘ φ)]; rotate_left
      . funext x
        simp [unitInterval.symm]
        rw [Path.extend_extends]
      rw [derivWithin_rccomp]
      . rw [(φderiv x).hasDerivWithinAt.derivWithin, negone]
        simp [φmap, φ]
        convert uniqueDiffOn_Icc_zero_one.uniqueDiffWithinAt _
        simp [φ] at xIn ⊢; tauto
      . use γ.deriv (φ x)
        rw [φmap]
        convert γ.hasDerivWithinAt _
        simp [φ] at xIn ⊢; tauto
      . use (-1)
        exact (φderiv x).hasDerivWithinAt
      all_goals
        try rw [φmap]
        convert uniqueDiffOn_Icc_zero_one.uniqueDiffWithinAt _
        simp [φ] at xIn ⊢; tauto

  simp [curveIntegral]
  rw [integral_congr eqOn, ← integral_symm]
  symm
  convert integral_by_substitution (φ := φ) (φ' := fun _ ↦ -1) (a := 0) (b := 1) (c := 0) (d := 1) _ _ _ _
  . simp [φ]
  . simp [φ]
  . push_cast; congr
  . apply (hf.comp γ.path.continuous_extend.continuousOn _).mul γ.derivContinuous'.continuousOn
    intro x xIn
    have xIn' : x ∈ Icc 0 1 := by simp at xIn ⊢; tauto
    rw [Path.extend_extends _ xIn', mem_range]
    use ⟨x, xIn'⟩
    rfl
  . intro x xIn
    simp [φ]
    simp at xIn
    tauto
  . intro x _
    exact φderiv x
  . exact continuousOn_const

@[simp]
theorem integral_add (hf : CurveIntegrable f γ) (hg : CurveIntegrable g γ) :
    (∮'γ, (f+g)) = (∮'γ, f) + (∮'γ, g) := by
  simp [curveIntegral]
  convert intervalIntegralC.integral_add hf hg
  simp; ring

theorem integral_finset_sum [DecidableEq ι] {f : ι → ℂ₁ → ℂ₁} {s : Finset ι}
    (h : ∀ i ∈ s, CurveIntegrable (f i) γ) :
    (∮'γ, (∑ i ∈ s, f i)) = ∑ i ∈ s, ∮'γ, f i := by
  simp [curveIntegral]
  convert intervalIntegralC.integral_finset_sum h
  simp [Finset.sum_mul]

@[simp]
theorem integral_neg : (∮'γ, -f) = -(∮'γ, f) := by
  simp [curveIntegral]
  convert intervalIntegralC.integral_neg
  simp

@[simp]
theorem integral_sub (hf : CurveIntegrable f γ) (hg : CurveIntegrable g γ) :
    (∮'γ, f-g) = (∮'γ, f) - (∮'γ, g) := by
  simp [curveIntegral]
  convert intervalIntegralC.integral_sub hf hg
  simp; ring

@[simp]
theorem integral_const_mul (c : ℂ₁) (hf : CurveIntegrable f γ) :
    (∮'γ, fun x ↦ c*(f x)) = c*(∮'γ, f) := by
  simp [curveIntegral]
  convert intervalIntegralC.integral_const_mul c hf
  simp; ring

@[simp]
theorem integral_mul_const (c : ℂ₁) (hf : CurveIntegrable f γ) :
    (∮'γ, fun x ↦ (f x)*c) = (∮'γ, f)*c := by
  simp_rw [mul_comm _ c]
  exact integral_const_mul c hf

@[simp]
theorem integral_div (c : ℂ₁) (hf : CurveIntegrable f γ) :
    (∮'γ, fun x ↦ f x/c) = (∮'γ, f)/c := by
  convert integral_mul_const (1/c) hf using 1 <;> field_simp

theorem norm_integral_leq_length_times_max {γ : Curve a b} (cont : ContinuousOn f (range γ)) :
    ‖∮'γ, f‖ ≤ γ.length * sSup ((norm ∘ f) '' (range γ)) := by
  simp [curveIntegral]
  have mapsto : MapsTo γ.path.extend (Set.uIcc 0 1) (range γ) := by
    intro x xIn
    have xIn' : x ∈ Icc 0 1 := by simp at xIn ⊢; tauto
    rw [Path.extend_extends _ xIn']
    use ⟨x, xIn'⟩
    rfl

  calc
    _ ≤ ∫ x in (0:ℝ)..1, ‖f (γ.path.extend x)‖*‖γ.deriv x‖ := by
      convert intervalIntegralC.norm_integral_leq_integral_norm _ _
      simp
      norm_num
      exact (cont.comp γ.path.continuous_extend.continuousOn mapsto).mul
        γ.derivContinuous'.continuousOn
    _ ≤ ∫ x in (0:ℝ)..1, (sSup ((norm ∘ f) '' (range γ)))*‖γ.deriv x‖ := by
      apply intervalIntegral.integral_mono_on (by norm_num)
      . apply ContinuousOn.intervalIntegrable
        apply ContinuousOn.mul
        exact continuous_norm.comp_continuousOn (cont.comp γ.path.continuous_extend.continuousOn mapsto)
        exact continuous_norm.comp_continuousOn γ.derivContinuous'.continuousOn
      . apply Continuous.intervalIntegrable
        exact continuous_const.mul (continuous_norm.comp γ.derivContinuous')
      . intro x xIn; simp
        gcongr
        apply le_csSup
        . apply Bornology.IsBounded.bddAbove
          apply IsCompact.isBounded
          exact IsCompact.image_of_continuousOn γ.range_compact (continuous_norm.comp_continuousOn cont)
        refine' ⟨γ.path.extend x, _, _⟩
        . use ⟨x, xIn⟩
          rw [Path.extend_extends _ xIn]
          rfl
        . simp [Norm.norm]
    _ = _ := by
      rw [intervalIntegral.integral_const_mul, mul_comm]
      rfl

end curveIntegral

noncomputable def contourIntegral (f : ℂ₁ → ℂ₁) (γ : Contour a b) :=
  ∑ i : Fin (γ.n + 1), curveIntegral f (γ.curves i)

notation "∮" γ ", " f => contourIntegral f γ

namespace contourIntegral

@[simp]
theorem integral_zero : (∮γ, 0) = 0 := by simp [contourIntegral]

@[simp]
theorem integral_reverse {γ : Contour a b} (hf : ContinuousOn f γ.range) : (∮γ.reverse, f) = -(∮γ, f) := by
  simp [contourIntegral, Contour.reverse]
  conv =>
    lhs; arg 2; ext i
    rw [curveIntegral.integral_reverse (hf.mono Contour.segment_range_subset_contour_range)]
  rw [Finset.sum_neg_distrib]
  congr 1
  rw [Function.Bijective.sum_comp (g := fun x ↦ ∮'γ.curves x, f) Fin.rev_bijective]

@[simp]
theorem integral_add (hf : ContourIntegrable f γ) (hg : ContourIntegrable g γ) :
    (∮γ, (f+g)) = (∮γ, f) + (∮γ, g) := by
  simp [contourIntegral, ← Finset.sum_add_distrib]
  conv => rhs; arg 2; ext i; rw [← curveIntegral.integral_add (hf i) (hg i)]

theorem integral_finset_sum [DecidableEq ι] {f : ι → ℂ₁ → ℂ₁} {s : Finset ι}
    (h : ∀ i ∈ s, ContourIntegrable (f i) γ) :
    (∮γ, (∑ i ∈ s, f i)) = ∑ i ∈ s, ∮γ, f i := by
  simp [contourIntegral]
  conv => lhs; arg 2; ext i; rw [curveIntegral.integral_finset_sum (fun j jIn ↦ h j jIn i)]
  exact Eq.symm Finset.sum_comm

@[simp]
theorem integral_neg : (∮γ, -f) = -(∮γ, f) := by simp [contourIntegral]

@[simp]
theorem integral_sub (hf : ContourIntegrable f γ) (hg : ContourIntegrable g γ) :
    (∮γ, f-g) = (∮γ, f) - (∮γ, g) := by
  simp [contourIntegral]
  conv => lhs; arg 2; ext i; rw [curveIntegral.integral_sub (hf i) (hg i)]
  simp

@[simp]
theorem integral_const_mul (c : ℂ₁) (hf : ContourIntegrable f γ) :
    (∮γ, fun x ↦ c*(f x)) = c*(∮γ, f) := by
  simp [contourIntegral]
  conv => lhs; arg 2; ext i; rw [curveIntegral.integral_const_mul _ (hf i)]
  rw [Finset.mul_sum]

@[simp]
theorem integral_mul_const (c : ℂ₁) (hf : ContourIntegrable f γ) :
    (∮γ, fun x ↦ (f x)*c) = (∮γ, f)*c := by
  simp_rw [mul_comm _ c]
  exact integral_const_mul c hf

@[simp]
theorem integral_div (c : ℂ₁) (hf : ContourIntegrable f γ) :
    (∮γ, fun x ↦ f x/c) = (∮γ, f)/c := by
  convert integral_mul_const (1/c) hf using 1 <;> field_simp

@[simp]
theorem integral_join {γ : Contour a b} {η : Contour b c} : (∮γ.join η, f) = (∮γ, f) + (∮η, f) := by
  simp only [contourIntegral, Contour.join]
  have : (γ.n + 1) + (η.n + 1) = γ.n + 1 + η.n + 1 := rfl
  simp [← Fin.sum_congr' _ this, Fin.sum_univ_add (a := γ.n+1)]

theorem norm_integral_leq_length_times_max (cont : ContinuousOn f γ.range) :
    ‖∮γ, f‖ ≤ γ.length * sSup ((norm ∘ f) '' γ.range) := by
  simp [contourIntegral]
  calc
    _ ≤ ∑ i : Fin (γ.n+1), ‖∮'γ.curves i, f‖ := norm_sum_le _ _
    _ ≤ ∑ i : Fin (γ.n+1), (γ.curves i).length * sSup ((norm ∘ f) '' range (γ.curves i)) := by
      gcongr with i
      have cont' : ContinuousOn f (range (γ.curves i)) :=
        cont.mono Contour.segment_range_subset_contour_range
      exact curveIntegral.norm_integral_leq_length_times_max cont'
    _ ≤ ∑ i : Fin (γ.n+1), (γ.curves i).length * sSup ((norm ∘ f) '' γ.range) := by
      gcongr with i
      . exact (γ.curves i).length_nonneg
      apply csSup_le_csSup
      . apply Bornology.IsBounded.bddAbove
        apply IsCompact.isBounded
        apply IsCompact.image_of_continuousOn γ.range_compact (continuous_norm.comp_continuousOn cont)
      . refine' ⟨‖f (γ.sources i)‖, γ.sources i, _, _⟩
        . use 0
          show (γ.curves i).path 0 = γ.sources i
          simp
        . rfl
      . gcongr
        exact Contour.segment_range_subset_contour_range
    _ = _ := by
      rw [← Finset.sum_mul]
      congr

end contourIntegral

-- exchange limits and integration

open Filter Topology General.Topology in
theorem curveIntegral.integral_sequence_convto_of_sequence_uniform_convergent
    {f : ℕ → ℂ₁ → ℂ₁} {γ : Curve a b} (cont : ∀i, ContinuousOn (f i) (range γ))
    (unf : TendstoUniformlyOn f F atTop (range γ)) :
    Tendsto (fun i ↦ ∮'γ, f i) atTop (𝓝 (∮'γ, F)) := by
  rw [tendsto_iff_dist_tendsto_zero]
  simp_rw [dist_eq_norm_sub]
  have Fcont : ContinuousOn F (range γ) := unf.continuousOn (Filter.Eventually.of_forall cont)
  conv =>
    arg 1; ext i
    rw [← integral_sub (cont i).curveIntegrable Fcont.curveIntegrable]
  apply squeeze_zero (fun _ ↦ norm_nonneg _) (fun _ ↦ norm_integral_leq_length_times_max _); rotate_left
  . intro i
    exact (cont i).sub Fcont
  conv => arg 3; arg 1; rw [← mul_zero γ.length]
  apply Tendsto.const_mul
  exact supNorm_tendsto_zero_of_tendstoUniformlyOn unf

open General General.PowerSeries Filter Topology in
theorem curveIntegral.powerSeries_integral {ps : PowerSeries ℂ₁} {γ : Curve a b}
    (radpos : ps.RadiusOfConvergence ≠ 0) (convOn : range γ ⊆ ps.DiskOfConvergence) :
    Tendsto (fun i ↦ (∑ k in Finset.range (i+1), (∮'γ, flip ps.series_at k))) atTop
      (𝓝 (∮'γ, fun x ↦ ∑ᵢ ps.series_at x)) := by
  have cont : ∀i, Continuous (flip ps.series_at i) :=
    fun i ↦ continuous_const.mul ((continuous_pow i).comp (continuous_id.sub continuous_const))
  have int : ∀i, CurveIntegrable (flip ps.series_at i) γ :=
    fun i ↦ (cont i).curveIntegrable
  conv =>
    arg 1; ext i;
    rw [← integral_finset_sum (fun i _ ↦ int i)]
  apply integral_sequence_convto_of_sequence_uniform_convergent
  . intro i
    apply Continuous.continuousOn
    convert continuous_finset_sum (Finset.range (i+1)) (fun i _ ↦ cont i)
    simp
  . obtain ⟨range', range'coe⟩ : ∃ s : Set (ps.DiskOfConvergence), s = range γ :=
      CanLift.prf (range ⇑γ) convOn
    have range'compact : IsCompact range' := by
      rw [Subtype.isCompact_iff, range'coe]
      exact γ.range_compact
    have ttu' := ps.powerSeries_compactly_convergent radpos range' range'compact
    rw [tendstoUniformlyOn_iff_tendsto, tendsto_prod_iff] at ttu' ⊢
    intro W Wunif
    obtain ⟨U, Utop, ⟨V, Vrange, h⟩⟩ := ttu' W Wunif
    refine' ⟨U, Utop, V, _, _⟩
    . rw [← range'coe]
      simp at Vrange ⊢
      exact Vrange
    . intro i z iU zV
      obtain ⟨z', z'V, z'coe⟩ := zV
      convert h i z' iU z'V
      . simp [z'coe]
      . simp; congr; funext x
        rw [← z'coe]
        rfl

open Filter Topology Contour in
theorem contourIntegral.integral_sequence_convto_of_sequence_uniform_convergent
    {f : ℕ → ℂ₁ → ℂ₁} {γ : Contour a b} (cont : ∀i, ContinuousOn (f i) γ.range)
    (unf : TendstoUniformlyOn f F atTop γ.range) :
    Tendsto (fun i ↦ ∮γ, f i) atTop (𝓝 (∮γ, F)) := by
  simp [contourIntegral]
  apply tendsto_finset_sum
  intro i _
  apply curveIntegral.integral_sequence_convto_of_sequence_uniform_convergent
  . intro j
    exact (cont j).mono segment_range_subset_contour_range
  . exact unf.mono segment_range_subset_contour_range

open General General.PowerSeries Filter Topology Contour in
theorem contourIntegral.powerSeries_integral {ps : PowerSeries ℂ₁} {γ : Contour a b}
    (radpos : ps.RadiusOfConvergence ≠ 0) (convOn : γ.range ⊆ ps.DiskOfConvergence) :
    Tendsto (fun i ↦ (∑ k in Finset.range (i+1), (∮γ, flip ps.series_at k))) atTop
      (𝓝 (∮γ, fun x ↦ ∑ᵢ ps.series_at x)) := by
  simp [contourIntegral]
  conv => arg 1; ext i; rw [Finset.sum_comm]
  apply tendsto_finset_sum
  intro i _
  apply curveIntegral.powerSeries_integral radpos
  exact subset_trans segment_range_subset_contour_range convOn

-- existence of antiderivatives and their relation to curve integrals

def IsAntiderivOn (F f : ℂ₁ → ℂ₁) (U : Set ℂ₁) := ∀x ∈ U, HasDerivAt F (f x) x
def HasAntiderivOn (f : ℂ₁ → ℂ₁) (U : Set ℂ₁) := ∃F, IsAntiderivOn F f U

namespace curveIntegral

open unitInterval in
theorem integral_eq_sub_of_isAntiderivativeOn {γ : Curve a b} (rangeU : range γ ⊆ U)
    (anti : IsAntiderivOn F f U) (cont : ContinuousOn f U) : (∮'γ, f) = F b - F a := by
  simp [curveIntegral]
  let γ_ext := γ.path.extend
  have Faeq : F a = (F ∘ γ_ext) 0 := by
    show F a = F (γ_ext 0)
    simp [γ_ext, Path.extend_extends]
  have Fbeq : F b = (F ∘ γ_ext) 1 := by
    show F b = F (γ_ext 1)
    simp [γ_ext, Path.extend_extends]
  rw [Faeq, Fbeq]
  have mapsTo : MapsTo γ_ext I (range γ_ext) := by intro _ _; simp
  have compDeriv : ∀x ∈ I, HasDerivWithinAt (F ∘ γ_ext) (f (γ_ext x) * γ.deriv x) I x := by
    intro x xI
    apply (anti _ _).hasDerivWithinAt.comp
    . exact γ.hasDerivWithinAt xI
    . exact mapsTo
    . apply rangeU
      use ⟨x, xI⟩
      unfold γ_ext
      rw [Path.extend_extends]
      rfl
  apply intervalIntegralC.integral_eq_sub_of_hasDerivWithinAt
  . convert ContinuousOn.comp _ _ mapsTo; simp
    . rw [γ.path_extend_range_eq_range]
      apply ContinuousOn.mono _ rangeU
      intro x xU
      exact (anti x xU).continuousAt.continuousWithinAt
    . exact γ.path.continuous_extend.continuousOn
  . convert compDeriv
    all_goals simp
  . apply ContinuousOn.intervalIntegrableC
    apply ContinuousOn.mul _ γ.derivContinuous'.continuousOn
    convert ContinuousOn.comp _ γ.path.continuous_extend.continuousOn mapsTo; simp
    rw [γ.path_extend_range_eq_range]
    exact cont.mono rangeU

theorem closedIntegral_eq_zero_of_hasAntiderivativeOn {γ : Curve a a} (rangeU : range γ ⊆ U)
    (hasAnti : HasAntiderivOn f U) (cont : ContinuousOn f U) : (∮'γ, f) = 0 := by
  obtain ⟨F, Fderiv⟩ := hasAnti
  rw [integral_eq_sub_of_isAntiderivativeOn rangeU Fderiv cont]
  ring

namespace isAntiderivativeOn_of_integral_eq_sub_helper

open Classical in
private noncomputable def F₁_mk (F f : ℂ₁ → ℂ₁) (z : ℂ₁) (r : ℝ) := fun w ↦
  if w = z then f z
  else if w ∈ Metric.ball z r then (w-z)⁻¹*∮'(Curve.line z w), f
  else (w-z)⁻¹*(F w - F z)

private lemma F₁_eq_all {F f : ℂ₁ → ℂ₁} (ballU : Metric.ball z r ⊆ U)
    (eq_sub : ∀ a b, ∀ (γ : Curve a b), range γ ⊆ U → (∮'γ, f) = F b - F a) :
    ∀w, F w = F z + (w-z)*(F₁_mk F f z r w) := by
  intro w
  by_cases wz : w = z
  . rw [wz]
    ring
  unfold F₁_mk
  by_cases wball : w ∈ Metric.ball z r
  . rw [if_neg wz, if_pos wball, ← mul_assoc, mul_inv_cancel₀ (sub_ne_zero_of_ne wz), one_mul, add_comm]
    apply sub_eq_iff_eq_add.mp
    symm
    apply eq_sub
    exact (Curve.line_range_memBall wball).trans ballU
  . rw [if_neg wz, if_neg wball, ← mul_assoc, mul_inv_cancel₀ (sub_ne_zero_of_ne wz), one_mul]
    ring

private lemma F₁_eq_z : F₁_mk F f z r z = f z := by simp [F₁_mk]

private lemma F₁_sub_norm_le (F f : ℂ₁ → ℂ₁) (cont : ContinuousOn f U) (ballU : Metric.ball z r ⊆ U) :
    ∀w ∈ Metric.ball z r, ‖F₁_mk F f z r w - F₁_mk F f z r z‖ ≤
      ‖(w-z)⁻¹‖*‖w-z‖*(sSup ((fun x ↦ ‖f x- f z‖) '' range (Curve.line z w))) := by
  intro w wball
  by_cases wz : w = z
  . simp [F₁_mk, if_pos wz]
    apply mul_nonneg
    . apply mul_nonneg
      . rw [inv_nonneg]
        exact norm_nonneg _
      . exact norm_nonneg _
    apply Real.sSup_nonneg
    simp
    rintro x _ _ _ _ rfl
    exact norm_nonneg _
  . unfold F₁_mk
    simp_rw [if_neg wz, if_pos wball, if_pos]
    have fzeq : f z = (w-z)⁻¹*∮'(Curve.line z w), fun _ ↦ f z := by
      conv => rhs; arg 2; arg 1; ext u; rw [← mul_one (f z)]
      rw [integral_const_mul]
      simp [curveIntegral, Curve.line]
      let g : ℝ → ℂ₁ := fun t ↦ (w-z)
      rw [intervalIntegralC.integral_congr_Ioo (g := g)]; rotate_left
      . rintro t tI
        simp
        rw [Curve.deriv_eq (by simp at tI ⊢; constructor <;> linarith),
            derivWithin_congr (f := fun (t : ℝ) ↦ (1-t)*z+t*w)]; rotate_left
        . intro t tI
          rw [Path.extend_extends _ tI]
          simp
        . rw [Path.extend_extends]
          simp
          simp at tI ⊢; constructor <;> linarith
        apply (Curve.lineFun_hasDerivAt z w t).hasDerivWithinAt.derivWithin
        exact uniqueDiffOn_Icc_zero_one.uniqueDiffWithinAt (by simp at tI ⊢; constructor <;> linarith)
      . exact CurveIntegrable.curveIntegrable_const
      simp [g]
      field_simp [sub_ne_zero_of_ne wz]
    nth_rw 1 [fzeq]
    rw [← mul_sub, ← integral_sub _ CurveIntegrable.curveIntegrable_const, NormedField.norm_mul']; rotate_left
    . apply (cont.mono _).curveIntegrable
      exact (Curve.line_range_memBall wball).trans ballU
    rw [mul_assoc]
    gcongr
    convert norm_integral_leq_length_times_max _ using 2
    . exact Curve.line_length.symm
    . apply (cont.mono _).sub continuousOn_const
      exact (Curve.line_range_memBall wball).trans ballU

open Filter Topology in
private lemma F₁_dom_tt0 (f : ℂ₁ → ℂ₁) (cont : ContinuousOn f U) (openU : IsOpen U) (zU : z ∈ U) :
    Tendsto (fun w ↦ ‖(w-z)⁻¹‖*‖w-z‖*(sSup ((fun x ↦ ‖f x - f z‖) '' range (Curve.line z w)))) (𝓝 z) (𝓝 0) := by
  have contAt := cont.continuousAt (mem_nhds_iff.mpr ⟨U, subset_refl _, openU, zU⟩)
  rw [ContinuousAt, Metric.tendsto_nhds] at contAt
  simp_rw [norm_inv]
  rw [Metric.tendsto_nhds]
  intro ε εpos
  specialize contAt (ε/2) (by linarith)
  rw [Metric.eventually_nhds_iff] at contAt ⊢
  obtain ⟨δ, δpos, contlt⟩ := contAt
  refine' ⟨δ, δpos, _⟩
  intro w wlt
  by_cases wz : w = z
  . simpa [wz]
  field_simp [sub_ne_zero_of_ne wz]
  calc
    _ ≤ ε/2 := by
      rw [abs_of_nonneg]; rotate_left
      . apply Real.sSup_nonneg
        simp
        rintro x _ _ _ _ rfl
        exact norm_nonneg _
      apply csSup_le
      . simp
        use z
        use 0
        simp [Curve.line]
        show (fun t ↦ (1-t)*z + t*w) 0 = z
        simp
      intro x
      simp
      rintro u t ⟨tge, tle⟩ rfl rfl
      rw [← NormedField.dist_eq]
      apply le_of_lt
      apply contlt
      apply lt_of_le_of_lt _ wlt
      apply Curve.line_sub_le
    _ < ε := by linarith

private lemma F₁_cont_z (cont : ContinuousOn f U) (openU : IsOpen U) (ballU : Metric.ball z r ⊆ U) (zU : z ∈ U) (rpos: r > 0) :
    ContinuousAt (F₁_mk F f z r) z := by
  rw [ContinuousAt, tendsto_iff_norm_sub_tendsto_zero]
  apply squeeze_zero' _ _ (F₁_dom_tt0 f cont openU zU)
  . apply Filter.Eventually.of_forall
    intro w
    exact norm_nonneg _
  . rw [Metric.eventually_nhds_iff]
    refine' ⟨r, rpos, _⟩
    intro w wlt
    apply F₁_sub_norm_le F f cont ballU
    simpa

end isAntiderivativeOn_of_integral_eq_sub_helper

open isAntiderivativeOn_of_integral_eq_sub_helper in
theorem isAntiderivativeOn_of_integral_eq_sub (cont : ContinuousOn f U) (openU : IsOpen U)
    (eq_sub : ∀a b, ∀ (γ : Curve a b), range γ ⊆ U → (∮'γ, f) = F b - F a) :
    IsAntiderivOn F f U := by
  intro z zU
  rw [General.Differentiation.hasDerivAt_iff_exists_continuous]
  obtain ⟨r, rpos, ballU⟩ := Metric.isOpen_iff.mp openU z zU
  let F₁ := F₁_mk F f z r
  exact ⟨F₁, F₁_eq_all ballU eq_sub, F₁_eq_z, F₁_cont_z cont openU ballU zU rpos⟩

end curveIntegral

namespace contourIntegral

theorem integral_eq_sub_of_isAntiderivativeOn {γ : Contour a b} (rangeU : γ.range ⊆ U)
    (anti : IsAntiderivOn F f U) (cont : ContinuousOn f U) : (∮γ, f) = F b - F a := by
  simp [contourIntegral]
  conv =>
    lhs; arg 2; ext i
    rw [curveIntegral.integral_eq_sub_of_isAntiderivativeOn
       (Contour.segment_range_subset_contour_range.trans rangeU) anti cont]
  rw [Fin.sum_univ_add]
  conv =>
    lhs; arg 1; arg 2; ext i
    equals F (γ.targets i) - F (γ.sources i) => congr <;> norm_cast
  simp_rw [γ.adjacent]
  rw [Fin.sum_univ_eq_sum_range (f := fun n ↦ F (γ.sources (n+1)) - F (γ.sources n))]
  conv =>
    lhs; arg 1; arg 2; ext i
    norm_cast
  rw [Finset.sum_range_sub (f := fun n ↦ F (γ.sources n))]
  simp [← γ.sourceEq, Fin.natAdd, Fin.last, γ.targetEq]

theorem closedIntegral_eq_zero_of_hasAntiderivativeOn {γ : Contour a a} (rangeU : γ.range ⊆ U)
    (hasAnti : HasAntiderivOn f U) (cont : ContinuousOn f U) : (∮γ, f) = 0 := by
  obtain ⟨F, Fderiv⟩ := hasAnti
  rw [integral_eq_sub_of_isAntiderivativeOn rangeU Fderiv cont]
  ring

theorem isAntiderivativeOn_of_integral_eq_sub (cont : ContinuousOn f U) (openU : IsOpen U)
    (eq_sub : ∀a b, ∀ (γ : Contour a b), γ.range ⊆ U → (∮γ, f) = F b - F a) :
    IsAntiderivOn F f U := by
  apply curveIntegral.isAntiderivativeOn_of_integral_eq_sub cont openU
  intro a b γ γU
  specialize eq_sub a b (Contour.single γ) (by simp [Contour.single, Contour.range, γU])
  rw [← eq_sub]
  simp [curveIntegral, contourIntegral, Contour.single]

-- explicit "construction" of an antiderivative if every closed path has integral 0
-- usually not very useful except for knowing that it exists

noncomputable def antideriv_mk₀ (f : ℂ₁ → ℂ₁) (U : Set ℂ₁)
    (choose_centre : Set ℂ₁ → ℂ₁) (choose_contour : (a : ℂ₁) → (b : ℂ₁) → Contour a b) : ℂ₁ → ℂ₁ :=
  fun z ↦
    let centre := choose_centre (connectedComponentIn U z)
    let contour := choose_contour centre z
    ∮contour, f

theorem isAntiderivativeOn_of_closedIntegrals_zero (openU : IsOpen U) (cont : ContinuousOn f U)
    (int0 : ∀a, ∀ (γ : Contour a a), γ.range ⊆ U → (∮γ, f) = 0)
    (choose_centre : Set ℂ₁ → ℂ₁) (choose_contour : (a : ℂ₁) → (b : ℂ₁) → Contour a b)
    (centreIn : ∀S, S ⊆ U → S.Nonempty → choose_centre S ∈ S)
    (contourIn : ∀a b, b ∈ U → a ∈ connectedComponentIn U b → (choose_contour a b).range ⊆ U) :
    IsAntiderivOn (antideriv_mk₀ f U choose_centre choose_contour) f U := by
  apply isAntiderivativeOn_of_integral_eq_sub cont openU
  intro z w γ γU
  have cont' : ContinuousOn f γ.range := cont.mono γU
  have compIn : w ∈ connectedComponentIn U z := by
    rw [connectedComponentIn_eq_image (γU γ.source_mem_range)]
    simp
    use γU γ.target_mem_range
    apply pathComponent_subset_component
    rw [mem_pathComponent_iff]
    apply JoinedIn.joined_subtype
    use γ.path
    intro t
    apply γU
    simp [← Contour.path_range_eq_range]
  have compEq := connectedComponentIn_eq compIn
  simp [antideriv_mk₀]
  rw [compEq]
  set y := choose_centre (connectedComponentIn U w)
  have yUw : y ∈ connectedComponentIn U w := by
    apply centreIn
    exact connectedComponentIn_subset U w
    use w
    apply mem_connectedComponentIn
    exact connectedComponentIn_subset U z compIn
  symm
  apply eq_of_sub_eq_zero
  rw [sub_eq_add_neg, sub_eq_add_neg]
  rw [← contourIntegral.integral_reverse, ← contourIntegral.integral_reverse cont']
  rotate_left
  . apply cont.mono
    apply contourIn _ _ _
    . rw [compEq]
      exact yUw
    . apply γU
      exact γ.source_mem_range
  rw [add_comm]; nth_rw 2 [add_comm]
  rw [← integral_join, ← integral_join]
  apply int0
  simp; constructor
  . exact γU
  have yU : y ∈ U := by
    apply connectedComponentIn_subset U w
    apply centreIn _ (connectedComponentIn_subset U w)
    rw [connectedComponentIn_nonempty_iff]
    apply γU
    exact γ.target_mem_range
  constructor
  . apply contourIn _ _
    . apply γU
      exact γ.source_mem_range
    . rwa [compEq]
  . apply contourIn _ _ _ yUw
    apply γU
    apply γ.target_mem_range

-- prove existence of antiderivatives on any open set

def contourConnectedSet (U : Set ℂ₁) (z : ℂ₁) :=
  { w : U | ∃ (γ : Contour z w), γ.range ⊆ U }

lemma contourConnectedSet_open {U : Set ℂ₁} (openU : IsOpen U) : IsOpen (contourConnectedSet U z) := by
  rw [Metric.isOpen_iff]
  intro x xIn
  obtain ⟨ε, εpos, ballU⟩ := Metric.isOpen_iff.mp openU x x.coe_prop
  obtain ⟨γ, rangeU⟩ := xIn
  refine' ⟨ε, εpos, _⟩
  intro w wIn
  refine' ⟨γ.join (Contour.single (Curve.line x w)), _⟩
  rw [Contour.join_range, Contour.single_range, union_subset_iff]
  refine' ⟨rangeU, _⟩
  simp [Curve.line]
  intro s ⟨t, seq⟩
  simp [← seq]
  apply ballU
  simp_rw [Metric.mem_ball, sub_mul, one_mul, dist_eq_norm]
  show ‖x-(t : ℂ₁)*x+t*w-x‖ < ε
  conv =>
    lhs; arg 1
    equals (t : ℂ₁)*(w-x) => ring
  rw [NormedField.norm_mul']
  calc
    _ ≤ 1*‖(w-x : ℂ₁)‖ := by
      apply mul_le_mul_of_nonneg_right _ (norm_nonneg _)
      norm_cast
      obtain ⟨tpos, tle1⟩ := Set.mem_Icc.mp (Subtype.coe_prop t)
      rw [abs_le]
      constructor <;> linarith
    _ < ε := by
      rw [one_mul]
      exact Metric.mem_ball.mp wIn

lemma contourConnectedSet_closed {U : Set ℂ₁} (openU : IsOpen U) : IsClosed (contourConnectedSet U z) := by
  rw [isClosed_iff_nhds]
  intro x xnhds
  obtain ⟨ε, εpos, ballU⟩ := Metric.isOpen_iff.mp openU x x.coe_prop
  obtain ⟨w, wball, γ, rangeU⟩ := xnhds (Metric.ball x ε) (Metric.ball_mem_nhds _ εpos)
  refine' ⟨γ.join (Contour.single (Curve.line x w).reverse), _⟩
  rw [Contour.join_range, Contour.single_range, union_subset_iff]
  refine' ⟨rangeU, _⟩
  simp [Curve.line]
  intro s ⟨t, seq⟩
  simp [← seq]
  apply ballU
  simp_rw [Metric.mem_ball, sub_mul, one_mul, dist_eq_norm]
  show ‖x-(t : ℂ₁)*x+t*w-x‖ < ε
  conv =>
    lhs; arg 1
    equals (t : ℂ₁)*(w-x) => ring
  rw [NormedField.norm_mul']
  calc
    _ ≤ 1*‖(w-x : ℂ₁)‖ := by
      apply mul_le_mul_of_nonneg_right _ (norm_nonneg _)
      norm_cast
      obtain ⟨tpos, tle1⟩ := Set.mem_Icc.mp (Subtype.coe_prop t)
      rw [abs_le]
      constructor <;> linarith
    _ < ε := by
      rw [one_mul]
      exact Metric.mem_ball.mp wball

lemma exists_contour_of_mem_connectedComponentIn (a b : ℂ₁) (openU : IsOpen U) (bIn : b ∈ U)
    (mem_comp : a ∈ connectedComponentIn U b) : ∃ (γ : Contour a b), γ.range ⊆ U := by
  rw [connectedComponentIn_eq_image bIn] at mem_comp
  obtain ⟨a', a'_comp, rfl⟩ := mem_comp
  suffices a' ∈ contourConnectedSet U b by
    obtain ⟨γ, rangeU⟩ := this
    refine' ⟨γ.reverse, _⟩
    simpa
  apply (IsClopen.connectedComponent_subset
    ⟨contourConnectedSet_closed openU, contourConnectedSet_open openU⟩ _) a'_comp
  refine' ⟨Contour.single (Curve.line b b), _⟩
  simp [Curve.line]
  rintro w ⟨t, rfl⟩
  conv =>
    arg 2
    equals b => ring_nf; rfl
  exact bIn

open Classical in
theorem hasAntiderivativeOn_of_closedIntegrals_zero (openU : IsOpen U) (cont : ContinuousOn f U)
    (int0 : ∀a, ∀ (γ : Contour a a), γ.range ⊆ U → (∮γ, f) = 0) :
    HasAntiderivOn f U := by
  let choose_centre : Set ℂ₁ → ℂ₁ := fun S ↦ if h : S.Nonempty then h.choose else 0
  let choose_contour : (a b : ℂ₁) → Contour a b :=
    fun a b ↦ if h : b ∈ U ∧ a ∈ connectedComponentIn U b
      then (exists_contour_of_mem_connectedComponentIn a b openU h.1 h.2).choose
      else Contour.single (Curve.line a b)
  use antideriv_mk₀ f U choose_centre choose_contour
  apply isAntiderivativeOn_of_closedIntegrals_zero openU cont int0
  . intro S SU Sne
    unfold choose_centre
    rw [dif_pos Sne]
    exact Sne.choose_spec
  . intro a b bU aConn
    unfold choose_contour
    rw [dif_pos ⟨bU, aConn⟩]
    exact (exists_contour_of_mem_connectedComponentIn a b openU bU aConn).choose_spec
