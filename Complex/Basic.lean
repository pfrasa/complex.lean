import Mathlib.Tactic
import Mathlib.Data.Real.Basic
import Mathlib.Data.Nat.ModEq
import Mathlib.Algebra.Field.Basic
import General.Algebra

-- Definition of C

@[ext]
structure Complex₁ where
  re : ℝ
  im : ℝ

notation "ℂ₁" => Complex₁

namespace Complex₁

instance : Zero ℂ₁ where
  zero := ⟨0, 0⟩

theorem zero_def : (0 : ℂ₁) = ⟨0, 0⟩ := rfl
@[simp]
theorem zero_re : (0 : ℂ₁).re = 0 := rfl
@[simp]
theorem zero_im : (0 : ℂ₁).im = 0 := rfl

instance : One ℂ₁ where
  one := ⟨1, 0⟩

theorem one_def : (1 : ℂ₁) = ⟨1, 0⟩ := rfl
@[simp]
theorem one_re : (1 : ℂ₁).re = 1 := rfl
@[simp]
theorem one_im : (1 : ℂ₁).im = 0 := rfl

instance : Neg ℂ₁ where
  neg := fun z ↦ ⟨-z.re, -z.im⟩

theorem neg_def (z: ℂ₁) : -z = ⟨-z.re, -z.im⟩ := rfl
@[simp]
theorem neg_re (z : ℂ₁) : (-z).re = -(z.re) := rfl
@[simp]
theorem neg_im (z : ℂ₁) : (-z).im = -(z.im) := rfl

instance : Add ℂ₁ where
  add := fun z w ↦ ⟨z.re + w.re, z.im + w.im⟩

theorem add_def (z w : ℂ₁) : (z + w) = ⟨z.re + w.re, z.im + w.im⟩ := rfl
@[simp]
theorem add_re (z w : ℂ₁) : (z + w).re = z.re + w.re := rfl
@[simp]
theorem add_im (z w : ℂ₁) : (z + w).im = z.im + w.im := rfl

instance : Mul ℂ₁ where
  mul := fun z w ↦ ⟨z.re * w.re - z.im * w.im, z.re * w.im + z.im * w.re⟩

theorem mul_def (z w : ℂ₁) : z * w = ⟨z.re * w.re - z.im * w.im, z.re * w.im + z.im * w.re⟩ := rfl
@[simp]
theorem mul_re (z w : ℂ₁) : (z * w).re = z.re * w.re - z.im * w.im := rfl
@[simp]
theorem mul_im (z w : ℂ₁) : (z * w).im = z.re * w.im + z.im * w.re := rfl

noncomputable instance : Inv ℂ₁ where
  inv := fun ⟨a, b⟩ ↦ ⟨a / (a^2 + b^2), -(b / (a^2 + b^2))⟩

theorem inv_def (z : ℂ₁) : z⁻¹ = ⟨z.re / (z.re^2 + z.im^2), -(z.im / (z.re^2 + z.im^2))⟩ := rfl
@[simp]
theorem inv_re (z: ℂ₁) : (z⁻¹).re = z.re / (z.re^2 + z.im^2) := rfl
@[simp]
theorem inv_im (z: ℂ₁) : (z⁻¹).im = -(z.im / (z.re^2 + z.im^2)) := rfl

-- C as R-module

def c_smul (r : ℝ) (z : ℂ₁) : ℂ₁ := ⟨r*z.re, r*z.im⟩

@[simp]
theorem c_one_smul (z : ℂ₁) : c_smul 1 z = z := by simp [c_smul]
@[simp]
theorem c_mul_smul (a b : ℝ) (z : ℂ₁) : c_smul (a*b) z = c_smul a (c_smul b z) := by simp [c_smul, mul_assoc]
@[simp]
theorem c_smul_zero (a : ℝ) : c_smul a 0 = 0 := by ext <;> simp [c_smul]
@[simp]
theorem c_smul_add (a : ℝ) (z w : ℂ₁) : c_smul a (z + w) = c_smul a z + c_smul a w := by ext <;> simp [c_smul, mul_add]
@[simp]
theorem c_add_smul (a b : ℝ) (z : ℂ₁) : c_smul (a + b) z = c_smul a z + c_smul b z := by ext <;> simp [c_smul, add_mul]
@[simp]
theorem c_zero_smul (z : ℂ₁) : c_smul 0 z = 0 := by ext <;> simp [c_smul]

instance : AddCommMonoid ℂ₁ where
  add := (. + .)
  add_assoc := by
    intro a b c
    ext <;> (simp; apply add_assoc)
  zero := 0
  zero_add := by
    intro a
    ext <;> simp
  add_zero := by
    intro a
    ext <;> simp
  nsmul := nsmulRec
  add_comm := by
    intro a b
    ext <;> simp [add_comm]

instance : Module ℝ ℂ₁ where
  smul := c_smul
  one_smul := c_one_smul
  mul_smul := c_mul_smul
  smul_zero := c_smul_zero
  smul_add := c_smul_add
  add_smul := c_add_smul
  zero_smul := c_zero_smul

theorem c_smul_def (r : ℝ) (z : ℂ₁) : r•z = c_smul r z := rfl

-- C is commutative ring

instance : CommRing ℂ₁ where
  natCast := fun n => ⟨n, 0⟩
  natCast_zero := by ext <;> simp
  natCast_succ := fun _ => by ext <;> simp
  mul := (. * .)
  left_distrib := by
    intro a b c
    ext <;> (simp; ring)
  right_distrib := by
    intro a b c
    ext <;> (simp; ring)
  zero_mul := by
    intro a
    ext <;> simp
  mul_zero := by
    intro a
    ext <;> simp
  mul_assoc := by
    intro a b c
    ext <;> (simp; ring)
  one := 1
  one_mul := by
    intro a
    ext <;> simp
  mul_one := by
    intro a
    ext <;> simp
  neg := (- .)
  zsmul := zsmulRec
  neg_add_cancel := by
    intro a
    ext <;> simp
  mul_comm := by
    intro a b
    ext <;> (simp; ring)

-- C is field

theorem add_re_sq_im_sq_nonneg (a : ℂ₁) : a.re^2 + a.im^2 ≥ 0 := by
  linarith [sq_nonneg a.re, sq_nonneg a.im]

theorem add_re_sq_im_sq_pos_of_nz {a : ℂ₁} (anz : a ≠ 0): a.re^2 + a.im^2 > 0 := by
  by_cases arez : a.re = 0
  . by_cases aimz : a.im = 0
    . exfalso
      have : a = 0 := by ext <;> assumption
      exact anz this
    . linarith [sq_nonneg a.re, sq_pos_of_ne_zero aimz]
  . linarith [sq_nonneg a.im, sq_pos_of_ne_zero arez]

noncomputable instance : Field ℂ₁ where
  inv := (.⁻¹)
  exists_pair_ne := by
    use 0, 1
    intro contra
    apply_fun (·.re) at contra
    simp at contra
  mul_inv_cancel := by
    intro a anz
    have : a.re^2 + a.im^2 > 0 := add_re_sq_im_sq_pos_of_nz anz
    ext <;> (simp; field_simp; linarith)
  inv_zero := by ext <;> simp
  qsmul := _
  nnqsmul := _

theorem sub_def (z w : ℂ₁) : (z - w) = ⟨z.re - w.re, z.im - w.im⟩ := rfl
@[simp]
theorem sub_re (z w : ℂ₁) : (z - w).re = z.re - w.re := rfl
@[simp]
theorem sub_im (z w : ℂ₁) : (z - w).im = z.im - w.im := rfl

-- N, Z, Q, R -> C

@[simp]
theorem nat_cast_eq (n : ℕ) : (n : ℂ₁) = ⟨n, 0⟩ := by rfl
@[simp, norm_cast]
theorem nat_cast_re (n : ℕ) : (n : ℂ₁).re = n := by rfl
@[simp, norm_cast]
theorem nat_cast_im (n : ℕ) : (n : ℂ₁).im = 0 := by rfl

theorem nat_cast_div {m n : ℕ} (h : m ∣ n) : ((n / m : ℕ) : ℂ₁) = n / m := by
  simp [div_eq_inv_mul, inv_def, mul_def, pow_two]
  field_simp

theorem nat_cast_ne_zero (n : ℕ) : (n : ℂ₁) ≠ 0 ↔ (n : ℕ) ≠ 0 := by
  constructor <;> (simp; intro h)
  . contrapose! h
    ext <;> simp [h]
  . contrapose! h
    apply_fun (·.re) at h
    simp at h
    exact h

@[simp]
theorem int_cast_eq (n : ℤ) : (n : ℂ₁) = ⟨n, 0⟩ := by
  obtain ⟨n, rfl | rfl⟩ := n.eq_nat_or_neg
  . simp
  . ext <;> simp
@[simp, norm_cast]
theorem int_cast_re (n : ℤ) : (n : ℂ₁).re = n := by simp
@[simp, norm_cast]
theorem int_cast_im (n : ℤ) : (n : ℂ₁).im = 0 := by simp

@[simp]
theorem neg_cast_eq (n : ℕ) : (-n : ℂ₁) = ⟨-n, 0⟩ := by ext <;> simp
@[simp, norm_cast]
theorem neg_cast_re (n : ℤ) : (-n : ℂ₁).re = -n := by simp
@[simp, norm_cast]
theorem neg_cast_im (n : ℤ) : (-n : ℂ₁).im = 0 := by simp

@[simp]
theorem rat_cast_eq (q : ℚ) : (q : ℂ₁) = ⟨q, 0⟩ := by
  rw [Field.ratCast_def, div_eq_mul_inv, inv_def, mul_def]
  ext <;> simp [pow_two]
  rw [inv_eq_one_div, mul_one_div, Field.ratCast_def]
@[simp, norm_cast]
theorem rat_cast_re (q : ℚ) : (q : ℂ₁).re = q := by simp
@[simp, norm_cast]
theorem rat_cast_im (q : ℚ) : (q : ℂ₁).im = 0 := by simp

@[coe]
def r_inj_c (r : ℝ) : ℂ₁ := ⟨r, 0⟩
instance : Coe ℝ ℂ₁ where
  coe := r_inj_c

@[norm_cast]
theorem r_inj_c_add (z w : ℝ): r_inj_c (z+w) = r_inj_c z + r_inj_c w := by
  simp [r_inj_c, add_def]

@[norm_cast]
theorem r_inj_c_mul (z w : ℝ): r_inj_c (z*w) = r_inj_c z * r_inj_c w := by
  simp [r_inj_c, mul_def]

def r_c_hom : ℝ →+* ℂ₁ where
  toFun := r_inj_c
  map_one' := by
    rw [r_inj_c]
    ext <;> simp
  map_mul' := r_inj_c_mul
  map_zero' := by
    simp [r_inj_c]
    ext <;> simp
  map_add' := r_inj_c_add

theorem r_c_hom_injective : Function.Injective r_c_hom := by
  intro x y feq
  simp [r_c_hom, r_inj_c] at feq
  assumption

lemma r_inj_c_to_hom : r_inj_c = r_c_hom := by
  ext x : 1
  norm_cast

@[norm_cast]
theorem r_inj_c_pow (r : ℝ) (n : ℕ) : r_inj_c (r^n) = (r_inj_c r)^n := by
  rw [r_inj_c_to_hom, map_pow]

@[norm_cast]
theorem r_inj_c_neg (r : ℝ) : r_inj_c (-r) = -(r_inj_c r) := by
  rw [r_inj_c_to_hom, map_neg]

@[norm_cast]
theorem r_inj_c_sub (a b : ℝ) : r_inj_c (a - b) = (r_inj_c a) - (r_inj_c b) := by
  rw [r_inj_c_to_hom, map_sub]

@[norm_cast]
theorem r_inj_c_inv (a : ℝ) : r_inj_c (a⁻¹) = (r_inj_c a)⁻¹ := by
  rw [r_inj_c_to_hom, map_inv₀]

@[norm_cast]
theorem r_inj_c_div (a b : ℝ) : r_inj_c (a / b) = (r_inj_c a) / (r_inj_c b) := by
  rw [r_inj_c_to_hom, map_div₀]

@[simp, norm_cast]
theorem r_cast_re (r : ℝ) : (r : ℂ₁).re = r := by rfl
@[simp, norm_cast]
theorem r_cast_im (r : ℝ) : (r : ℂ₁).im = 0 := by rfl

@[simp, norm_cast]
theorem r_cast_eq (r s : ℝ) : (r : ℂ₁) = s ↔ (r : ℝ) = s := by
  constructor
  . intro eq
    apply_fun (·.re) at eq; simp at eq
    exact eq
  . intro eq
    rw [eq]

theorem c_smul_eq_mul_cast (r : ℝ) (z : ℂ₁) : r•z = ↑r*z := by
  simp [c_smul_def, c_smul]
  ext <;> simp

-- Prove equality in R by proving it in C

theorem r_eq_by_c_eq {x y : ℝ} (eq_c : (x : ℂ₁) = (y : ℂ₁)) : x = y := by
  apply_fun r_inj_c
  exact eq_c
  rw [r_inj_c_to_hom]
  apply r_c_hom_injective

-- R as subfield of C + subsets

noncomputable def ℝ' : Subfield ℂ₁ where
  carrier := Set.range r_inj_c
  mul_mem' := by
    rintro a b ⟨a', acast⟩ ⟨b', bcast⟩
    use a'*b'
    push_cast
    rw [acast, bcast]
  one_mem' := by
    dsimp
    use 1
    norm_cast
  add_mem' := by
    rintro a b ⟨a', acast⟩ ⟨b', bcast⟩
    use a'+b'
    push_cast
    rw [acast, bcast]
  zero_mem' := by
    dsimp
    use 0
    norm_cast
  neg_mem' := by
    rintro x ⟨x', xcast⟩
    use -x'
    push_cast
    rw [xcast]
  inv_mem' := by
    rintro x ⟨x', xcast⟩
    use x'⁻¹
    push_cast
    rw [xcast]

theorem r_cast_mem (r : ℝ) : (r : ℂ₁) ∈ ℝ' := by use r

theorem r_mem_iff {z : ℂ₁} : z ∈ ℝ' ↔ z.im = 0 := by
  constructor
  . rintro ⟨r, rfl⟩
    simp
  . intro imz
    use z.re
    ext <;> simp [imz]

noncomputable def ℝnn' : Subsemiring ℂ₁ where
  carrier := r_inj_c '' { r : ℝ | r ≥ 0}
  mul_mem' := by
    rintro x y ⟨x', ⟨x'nn, rfl⟩⟩ ⟨y', ⟨y'nn, rfl⟩⟩
    simp at *
    exact ⟨x'*y', mul_nonneg x'nn y'nn, by norm_cast⟩
  one_mem' := by
    use 1
    simp
    norm_cast
  add_mem' := by
    rintro x y ⟨x', ⟨x'nn, rfl⟩⟩ ⟨y', ⟨y'nn, rfl⟩⟩
    simp at *
    exact ⟨x'+y', add_nonneg x'nn y'nn, by norm_cast⟩
  zero_mem' := by
    use 0
    simp
    norm_cast

theorem rnn_mem_iff {z : ℂ₁} : z ∈ ℝnn' ↔ z.im = 0 ∧ z.re ≥ 0 := by
  constructor
  . rintro ⟨r, rnn, rfl⟩
    simp at rnn
    simp [rnn]
  . rintro ⟨imz, renn⟩
    use z.re
    refine' ⟨renn, _⟩
    ext <;> simp [imz]

def ℝpos' := { z : ℂ₁ | z.im = 0 ∧ z.re > 0 }

theorem rpos_mem_of_rnn_nz {z : ℂ₁} : z ∈ ℝpos' ↔ z ∈ ℝnn' ∧ z ≠ 0 := by
  constructor
  . rintro ⟨imz, repos⟩
    constructor
    . use z.re
      refine' ⟨le_of_lt repos, _⟩
      ext <;> simp [imz]
    . intro zz
      apply_fun (·.re) at zz
      simp at zz
      linarith
  . rintro ⟨⟨r, rnn, rfl⟩, znz⟩
    constructor
    repeat simp at *
    apply lt_of_le_of_ne rnn
    intro rz
    rw [← rz] at znz
    norm_cast at znz

-- R-linearity of Re and Im
theorem re_r_linear (a b : ℝ) (z w : ℂ₁) : (a*z + b*w).re = a*z.re + b*w.re := by
  simp [r_inj_c]

theorem im_r_linear (a b : ℝ) (z w : ℂ₁) : (a*z + b*w).im = a*z.im + b*w.im := by
  simp [r_inj_c]

def re_linear_map : ℂ₁ →ₗ[ℝ] ℝ where
  toFun := re
  map_add' := by
    intro z w
    simp
  map_smul' := by
    intro a z
    simp [c_smul_def, c_smul]

theorem re_eq_re_linear_map : z.re = re_linear_map z := rfl

def im_linear_map : ℂ₁ →ₗ[ℝ] ℝ where
  toFun := im
  map_add' := by
    intro z w
    simp
  map_smul' := by
    intro a z
    simp [c_smul_def, c_smul]

theorem im_eq_im_linear_map : z.im = im_linear_map z := rfl

-- complex unit i; powers of i
def i : ℂ₁ := ⟨0, 1⟩

@[simp]
theorem i_re : i.re = 0 := rfl

@[simp]
theorem i_im : i.im = 1 := rfl

@[simp]
theorem i_sq : i^2 = -1 := by
  ext <;> simp [pow_two, i]

@[simp]
theorem neg_i_sq : (-i)^2 = -1 := by
  ext <;> simp [pow_two, i]

theorem i_pow_mod_0 {n : ℕ} (nmod : n ≡ 0 [MOD 4]) : i^n = 1 := by
  obtain ⟨k, rfl⟩ := Nat.modEq_zero_iff_dvd.mp nmod
  have : 4 = 2*2 := by norm_num
  rw [pow_mul, this, pow_mul, i_sq, neg_one_sq, one_pow]

theorem i_pow_mod_1 {n : ℕ} (nmod : n ≡ 1 [MOD 4]) : i^n = i := by
  have four : 4 = 2*2 := by norm_num
  obtain ⟨k, keq⟩ := Nat.ModEq.dvd nmod.symm
  apply_fun (. + 1) at keq
  simp at keq
  induction' k with k' k'
  . simp at keq
    norm_cast at keq
    rw [keq, pow_add, pow_mul, pow_one, four, pow_mul, i_sq, neg_one_sq, one_pow, one_mul]
  . have : (n : ℤ) ≥ 0 := by norm_cast
    have : (n : ℤ ) < 0 := by
      rw [keq, ← sub_self 1, sub_eq_add_neg, add_comm 1]
      apply add_lt_add_of_lt_of_le _ (by norm_num)
      norm_cast
    contradiction

theorem i_pow_mod_2 {n : ℕ} (nmod : n ≡ 2 [MOD 4]) : i^n = -1 := by
  have four : 4 = 2*2 := by norm_num
  obtain ⟨k, keq⟩ := Nat.ModEq.dvd nmod.symm
  apply_fun (. + 2) at keq
  simp at keq
  induction' k with k' k'
  . simp at keq
    norm_cast at keq
    rw [keq, pow_add, pow_mul, four, pow_mul, i_sq, neg_one_sq, one_pow, one_mul]
  . have : (n : ℤ) ≥ 0 := by norm_cast
    have : (n : ℤ ) < 0 := by
      rw [keq, ← sub_self 2, sub_eq_add_neg, add_comm 2]
      apply add_lt_add_of_lt_of_le _ (by norm_num)
      norm_cast
    contradiction

theorem i_pow_mod_3 {n : ℕ} (nmod : n ≡ 3 [MOD 4]) : i^n = -i := by
  have four : 4 = 2*2 := by norm_num
  have three : 3 = 2+1 := by norm_num
  obtain ⟨k, keq⟩ := Nat.ModEq.dvd nmod.symm
  apply_fun (. + 3) at keq
  simp at keq
  induction' k with k' k'
  . simp at keq
    norm_cast at keq
    rw [keq, pow_add, pow_mul, four, pow_mul, i_sq, neg_one_sq, one_pow, one_mul, three, pow_add, i_sq, pow_one, neg_one_mul]
  . have : (n : ℤ) ≥ 0 := by norm_cast
    have : (n : ℤ ) < 0 := by
      rw [keq, ← sub_self 3, sub_eq_add_neg, add_comm 3]
      apply add_lt_add_of_lt_of_le _ (by norm_num)
      norm_cast
    contradiction

-- existence and uniqueness of a + bi representation; unique square roots of -1
theorem complex_decomp (z : ℂ₁) : z = z.re + z.im*i := by
  simp [r_inj_c, mul_def, add_def, i]

theorem complex_decomp' (a b : ℝ) : a + b*i = ⟨a, b⟩ := by
  simp [r_inj_c, mul_def, add_def, i]

theorem complex_decomp_unique : ∀ (a b c d : ℝ), a + b*i = c + d*i → a = c ∧ b = d := by
  intro a b c d eq_decomp
  simp [r_inj_c, mul_def, add_def, i] at eq_decomp
  assumption

theorem roots_of_neg_one : ∀ (z : ℂ₁), z^2 = -1 ↔ z = i ∨ z = -i := by
  intro z
  constructor
  . intro sq_eq
    let a := z.re
    let b := z.im
    simp [r_inj_c, add_def, pow_two, mul_def] at *
    have re_eq : a*a - b*b = -1 := by
      apply_fun (·.re) at sq_eq
      simp [i] at sq_eq
      assumption
    have im_eq : 2*a*b = 0 := by
      apply_fun (·.im) at sq_eq
      simp [i] at sq_eq
      linarith
    by_cases az : a = 0
    . simp [az] at re_eq
      rw [← pow_two] at re_eq
      rcases sq_eq_one_iff.mp re_eq with bpos | bneg
      . left
        ext <;> (simp [i]; assumption)
      . right
        ext <;> (simp [i]; assumption)
    . push_neg at az
      have bz : b = 0 := by
        rcases mul_eq_zero.mp im_eq with two_a_z | bz
        . rcases mul_eq_zero.mp two_a_z
          . linarith
          . contradiction
        . assumption
      simp [← pow_two, bz] at re_eq
      linarith [sq_nonneg a]
  . rintro (zeq | zeq) <;>
      (ext <;> simp [i, zeq, pow_two])

-- divide by real number: split real and imaginary parts
theorem div_re_split (z : ℂ₁) (r : ℝ) : z / r = z.re / r + z.im*i / r := by
  by_cases rz : r = 0
  . simp [rz, r_inj_c, ← zero_def, div_zero]
  . have : (r : ℂ₁) ≠ 0 := by
      intro contra
      apply_fun (·.re) at contra
      simp at contra
      contradiction
    rw [div_add_div, mul_comm, ← mul_add, ← complex_decomp, mul_div_mul_left] <;> assumption
