import Mathlib
import General.Algebra
import General.Differentiation
import Complex.Basic
import Complex.Norm
import Complex.IntervalIntegral

open Complex₁

-- example calculations

example : (Complex₁.mk 2 3).re = (2 : ℝ) := by rfl
example (a : ℂ₁) : a + 3 = 3 + a := by ring
example (a : ℂ₁) : a + Real.pi = Real.pi + a := by ring
example : (1 : ℂ₁) = ⟨1, 0⟩ := by rfl
example : (-1 : ℂ₁) = ⟨-1, 0⟩ := by ext <;> simp
example : (2 : ℂ₁) = ⟨2, 0⟩ := by rfl
example : (-2 : ℂ₁) = ⟨-2, 0⟩ := by ext <;> (simp; rfl)
example (z w : ℂ₁) : ‖z/w‖ = ‖z‖/‖w‖ := norm_div z w

example : (1 + i) / (1 - i) = i := by
  have ext_nz : 1+i ≠ 0 := by
    intro contra
    apply_fun (·.re) at contra
    simp [i] at contra
  calc
    _ = (1+i)*(1+i) / ((1-i)*(1+i)) := by
      rw [mul_div_mul_right (1+i) (1-i) ext_nz]
    _ = (2*i) / 2 := by
      rw [mul_def, add_re, i]
      simp [add_mul, mul_add, sub_mul, add_def, mul_def]
      norm_num
      congr
      . simp; rfl
      . simp [sub_def]; norm_num; rfl
    _ = 2 * (i/2) := by ring
    _ = i := by
      rw [mul_div_cancel₀ _]
      intro contra
      apply_fun (·.re) at contra
      have : (2 : ℂ₁).re = 2 := rfl
      simp [this] at contra

example : ((1+i) / (Real.sqrt 2))^4 = -1 := by
  rw [div_pow, General.Algebra.binomial_theorem]
  repeat rw [Finset.sum_range_succ]
  rw [Finset.sum_range_zero]
  norm_num
  have : (Real.sqrt 2 : ℂ₁)^4 = 4 := by
    rw [← r_inj_c_pow, pow_add _ 2 2, Real.sq_sqrt (by norm_num)]
    norm_num
    rfl
  rw [this]
  calc
    (1 + 4 * i -(Nat.choose 4 2) + 4 * i^3 + i^4)/4 = (-4)/4 := by
      congr
      rw [pow_add _ 2 1, i_sq, pow_one, ← mul_assoc, mul_comm 4 (-1), mul_assoc, neg_mul, one_mul]
      rw [pow_add _ 2 2, i_sq, neg_mul, mul_comm 1, neg_mul]
      ext <;> simp [i]
      . ring_nf
        have : Nat.choose 4 2 = 6 := by rfl
        rw [this]
        norm_num
        rfl
      . rfl
    (-4)/4 = _ := by
      rw [neg_div, div_self]
      intro contra
      apply r_c_hom_injective at contra
      norm_num at contra

example (n : ℕ) : (1 + i)^(2*n) + (1 -i)^(2*n) = if Even n then (-1)^(n/2)*2^(n+1) else 0 := by
  have plus_sq : (1 + i)^(2*n) = 2^n*i^n := by
    conv =>
      lhs
      simp [pow_mul, pow_two, i, mul_def]
    have : ⟨0, 1 + 1⟩ = 2*i := by
      simp [i, mul_def]
      constructor <;> (norm_num; rfl)
    rw [this, mul_pow]
  have minus_sq : (1 - i)^(2*n) = (-2)^n*i^n := by
      conv =>
        lhs
        simp [pow_mul, pow_two, i, mul_def]
        norm_num
      have : ⟨0, -2⟩ = (-2)*i := by
        simp [i, mul_def]
        ext
        . simp [neg_re]
          rfl
        . simp [neg_im]
          rfl
      rw [this, mul_pow]
  rw [plus_sq, minus_sq]
  have : n % 4 < 4 := Nat.mod_lt _ (by norm_num)
  interval_cases neq : n % 4 <;>
    (
      have nmod : n ≡ n % 4 [MOD 4] := (Nat.mod_modEq n 4).symm
      rw [neq] at nmod
      obtain ⟨k, keq⟩ := Nat.ModEq.dvd nmod.symm
      simp at keq
    )

  -- n == 0 (mod 4)
  . have : k ≥ 0 := by linarith
    have : k = k.natAbs := by rw [← Int.abs_eq_natAbs, abs_eq_self.mpr this]
    rw [this] at keq
    norm_cast at keq
    have n_even : Even n := by
      use 2*k.natAbs
      rw [← mul_add, ← two_mul, ← mul_assoc]
      exact keq
    have n_2_even : Even (n/2) := by
      use k.natAbs
      rw [← two_mul]
      apply Nat.div_eq_of_eq_mul_right (by norm_num)
      rw [← mul_assoc]
      simpa
    simp [i_pow_mod_0 nmod, n_even, pow_add, mul_two]
    rw [Even.neg_one_pow n_2_even, one_mul]

  -- n == 1 (mod 4)
  . apply_fun (. + 1) at keq
    simp at keq
    have : k ≥ 0 := by linarith
    have : k = k.natAbs := by rw [← Int.abs_eq_natAbs, abs_eq_self.mpr this]
    rw [this] at keq
    norm_cast at keq
    have n_odd : Odd n := by
      use 2*k.natAbs
      rw [← mul_assoc, two_mul]
      norm_num
      exact keq
    simp [i_pow_mod_1 nmod, Nat.not_even_iff_odd.mpr n_odd]
    have : (-2 : ℂ₁) = (-1)*2 := by norm_num
    rw [this, mul_pow]
    nth_rewrite 2 [keq]
    have : 4 = 2*2 := by rfl
    rw [pow_add, pow_mul, this, pow_mul, neg_pow_two, one_pow, one_pow, one_pow, pow_one]
    simp

  -- n == 2 (mod 4)
  . apply_fun (. + 2) at keq
    simp at keq
    have : k ≥ 0 := by linarith
    have : k = k.natAbs := by rw [← Int.abs_eq_natAbs, abs_eq_self.mpr this]
    rw [this] at keq
    norm_cast at keq
    have : 4 = 2*2 := by rfl
    rw [this] at keq
    nth_rewrite 3 [← mul_one 2] at keq
    rw [mul_assoc, ← mul_add] at keq
    have n_even : Even n := by
      use 2*k.natAbs + 1
      linarith
    have n_2_odd : Odd (n/2) := by
      use k.natAbs
      apply Nat.div_eq_of_eq_mul_right (by norm_num)
      exact keq
    simp [i_pow_mod_2 nmod, n_even, pow_add, mul_two]
    rw [Odd.neg_one_pow n_2_odd, neg_one_mul, neg_add]

  -- n == 3 (mod 4)
  . apply_fun (. + 3) at keq
    simp at keq
    have : k ≥ 0 := by linarith
    have : k = k.natAbs := by rw [← Int.abs_eq_natAbs, abs_eq_self.mpr this]
    rw [this] at keq
    norm_cast at keq
    have e₁ : 4 = 2*2 := by rfl
    have e₂ : 3 = 2+1 := by rfl
    rw [e₁, e₂] at keq
    nth_rewrite 3 [← mul_one 2] at keq
    rw [← add_assoc, mul_assoc, ← mul_add] at keq
    have n_odd : Odd n := by
      use 2*k.natAbs + 1
    simp [i_pow_mod_3 nmod, Nat.not_even_iff_odd.mpr n_odd, pow_add, mul_two]
    have : (-2 : ℂ₁) = (-1)*2 := by simp
    rw [this, mul_pow, Odd.neg_one_pow n_odd, mul_assoc, neg_one_mul, ← sub_eq_add_neg, sub_neg_eq_add, add_comm, ← sub_eq_add_neg, sub_self]

open General.Differentiation Real in
example : HasPartialDerivAt 0 (fun (xs : Fin 2 → ℝ) ↦ ![sin (xs 0), sin (xs 0) + sin (xs 1), cos (xs 1)])
  ![cos x, cos x, 0] ![x, y] := by
  unfold HasPartialDerivAt
  rw [hasDerivAt_pi]
  intro i; fin_cases i
  . simp [hasDerivAt_sin]
  . simp
    exact HasDerivAt.add_const (hasDerivAt_sin _) (sin y)
  . simp
    apply hasDerivAt_const

open Complex₁.intervalIntegralC in
example : c∫ x in (0 : ℝ)..1, ⟨0, x⟩ = ⟨0, 1/2⟩ := by
  dsimp [intervalIntegralC]
  ext <;> simp
  . exact intervalIntegral.integral_zero
  . convert integral_id using 1
    ring
