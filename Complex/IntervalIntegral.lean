import Complex.Topology
import Complex.Differentiation

open MeasureTheory intervalIntegral Complex₁

namespace Complex₁

def IntervalIntegrableC (f : ℝ → ℂ₁) (a b : ℝ) :=
  IntervalIntegrable (re ∘ f) volume a b ∧ IntervalIntegrable (im ∘ f) volume a b

namespace IntervalIntegrableC

@[simp]
theorem intervalIntegrableC_const : IntervalIntegrableC (fun _ ↦ c) a b := by
  simp [IntervalIntegrableC]; constructor <;> apply _root_.intervalIntegrable_const

theorem symm (h : IntervalIntegrableC f a b) :
    IntervalIntegrableC f b a :=
  ⟨h.left.symm, h.right.symm⟩

@[simp]
theorem refl : IntervalIntegrableC f a a := by
  simp [IntervalIntegrableC]

theorem trans (hab : IntervalIntegrableC f a b)
    (hbc : IntervalIntegrableC f b c) : IntervalIntegrableC f a c :=
  ⟨hab.left.trans hbc.left, hab.right.trans hbc.right⟩

theorem neg (h : IntervalIntegrableC f a b) : IntervalIntegrableC (-f) a b :=
  ⟨h.left.neg, h.right.neg⟩

theorem mono_set (hf : IntervalIntegrableC f a b)
    (h : Set.uIcc c d ⊆ Set.uIcc a b) : IntervalIntegrableC f c d :=
  ⟨hf.left.mono_set h, hf.right.mono_set h⟩

@[simp]
theorem add (hf : IntervalIntegrableC f a b) (hg : IntervalIntegrableC g a b) :
    IntervalIntegrableC (f + g) a b :=
  ⟨hf.left.add hg.left, hf.right.add hg.right⟩

@[simp]
theorem sub (hf : IntervalIntegrableC f a b) (hg : IntervalIntegrableC g a b) :
    IntervalIntegrableC (f - g) a b :=
  ⟨hf.left.sub hg.left, hf.right.sub hg.right⟩

theorem sum [DecidableEq ι] {f : ι → ℝ → ℂ₁} (s : Finset ι)
    (h : ∀ i ∈ s, IntervalIntegrableC (f i) a b) :
    IntervalIntegrableC (∑ i ∈ s, f i) a b := by
  induction' s using Finset.induction_on with i s' ins ih
  . exact intervalIntegrableC_const
  . rw [Finset.sum_insert ins]
    apply IntervalIntegrableC.add
    . apply h; simp
    . apply ih
      intro j jIn
      apply h j
      simp [jIn]

theorem const_mul (hf : IntervalIntegrableC f a b) (c : ℂ₁) :
    IntervalIntegrableC (fun x ↦ c*(f x)) a b :=
  ⟨(hf.left.const_mul c.re).sub (hf.right.const_mul c.im),
   (hf.right.const_mul c.re).add (hf.left.const_mul c.im)⟩

theorem mul_const (hf : IntervalIntegrableC f a b) (c : ℂ₁) :
    IntervalIntegrableC (fun x ↦ (f x)*c) a b := by
  convert hf.const_mul c using 1
  funext; ring

theorem div_const (hf : IntervalIntegrableC f a b) (c : ℂ₁) :
    IntervalIntegrableC (fun x ↦ f x / c) a b := by
  convert hf.const_mul (1/c) using 1
  funext; field_simp

open MeasureTheory in
theorem congr (eq : f =ᶠ[ae (volume.restrict (Set.uIoc a b))] g)
    (hf : IntervalIntegrableC f a b) : IntervalIntegrableC g a b :=
  ⟨hf.left.congr (eq.fun_comp _), hf.right.congr (eq.fun_comp _)⟩

end Complex₁.IntervalIntegrableC

theorem ContinuousOn.intervalIntegrableC (hf : ContinuousOn f (Set.uIcc a b)) :
    IntervalIntegrableC f a b :=
  ⟨(re_continuous.comp_continuousOn hf).intervalIntegrable,
   (im_continuous.comp_continuousOn hf).intervalIntegrable⟩

theorem ContinuousOn.intervalIntegrableC_of_Icc
    (h : a ≤ b) (hf : ContinuousOn f (Set.Icc a b)) :
    IntervalIntegrableC f a b :=
  ⟨(re_continuous.comp_continuousOn hf).intervalIntegrable_of_Icc h,
   (im_continuous.comp_continuousOn hf).intervalIntegrable_of_Icc h⟩

theorem Continuous.intervalIntegrableC (hf : Continuous f) (a b : ℝ) :
    IntervalIntegrableC f a b :=
  ⟨(re_continuous.comp hf).intervalIntegrable a b,
   (im_continuous.comp hf).intervalIntegrable a b⟩

namespace Complex₁

noncomputable def intervalIntegralC (f : ℝ → ℂ₁) (a b : ℝ) : ℂ₁ :=
  ⟨intervalIntegral (re ∘ f) a b volume, intervalIntegral (im ∘ f) a b volume⟩

notation3"c∫ "(...)" in "a".."b", "r:60:(scoped f => intervalIntegralC f a b) => r

namespace intervalIntegralC

@[simp]
theorem integral_zero : c∫ _ in a..b, 0 = 0 := by
  unfold intervalIntegralC
  ext <;> (simp; exact intervalIntegral.integral_zero)

@[simp]
theorem integral_same {f : ℝ → ℂ₁} : c∫ x in a..a, f x = 0 := by
  simp [intervalIntegralC]
  ext <;> simp

theorem integral_symm {f : ℝ → ℂ₁} (a b : ℝ):
    c∫ x in b..a, f x = -(c∫ x in a..b, f x) := by
  simp [intervalIntegralC]
  ext <;> (simp; apply intervalIntegral.integral_symm)

@[simp]
theorem integral_add
    (hf : IntervalIntegrableC f a b) (hg : IntervalIntegrableC g a b) :
    c∫ x in a..b, f x + g x = (c∫ x in a..b, f x) + (c∫ x in a..b, g x) := by
  simp [intervalIntegralC]
  ext
  . exact intervalIntegral.integral_add hf.left hg.left
  . exact intervalIntegral.integral_add hf.right hg.right

theorem integral_finset_sum [DecidableEq ι] {f : ι → ℝ → ℂ₁} {s : Finset ι}
    (h : ∀ i ∈ s, IntervalIntegrableC (f i) a b) :
    c∫ x in a..b, (∑ i ∈ s, f i x) = ∑ i ∈ s, c∫ x in a..b, f i x := by
  induction' s using Finset.induction_on with i s' ins ih
  . simp
  . simp_rw [Finset.sum_insert ins]
    rw [integral_add, ih]
    . intro j jIn
      apply h j
      simp [jIn]
    . apply h; simp
    . have : (fun x ↦ ∑ j ∈ s', f j x) = ∑ j ∈ s', f j := by funext; simp
      rw [this]
      apply IntervalIntegrableC.sum
      intro j jIn
      apply h j
      simp [jIn]

@[simp]
theorem integral_neg {f : ℝ → ℂ₁} : c∫ x in a..b, -f x = -c∫ x in a..b, f x := by
  simp [intervalIntegralC]
  ext <;> convert intervalIntegral.integral_neg using 1

@[simp]
theorem integral_sub (hf : IntervalIntegrableC f a b) (hg : IntervalIntegrableC g a b) :
    c∫ x in a..b, f x - g x = (c∫ x in a..b, f x) - (c∫ x in a..b, g x) := by
  simp [intervalIntegralC]
  ext
  . exact intervalIntegral.integral_sub hf.left hg.left
  . exact intervalIntegral.integral_sub hf.right hg.right

@[simp]
theorem integral_const_mul (c : ℂ₁) (hf : IntervalIntegrableC f a b) :
    c∫ x in a..b, c*(f x) = c*c∫ x in a..b, f x := by
  simp [intervalIntegralC]
  ext <;> simp
  . simp [mul_def]
    have := intervalIntegral.integral_sub (hf.left.const_mul c.re) (hf.right.const_mul c.im)
    rw [intervalIntegral.integral_const_mul c.re, intervalIntegral.integral_const_mul c.im] at this
    rw [← this]
    rfl
  . simp [mul_def]
    have := intervalIntegral.integral_add (hf.right.const_mul c.re) (hf.left.const_mul c.im)
    rw [intervalIntegral.integral_const_mul c.re, intervalIntegral.integral_const_mul c.im] at this
    rw [← this]
    rfl

@[simp]
theorem integral_mul_const (c : ℂ₁) (hf : IntervalIntegrableC f a b) :
    c∫ x in a..b, (f x)*c = (c∫ x in a..b, f x)*c := by
  convert integral_const_mul c hf using 1 <;> simp_rw [mul_comm]

@[simp]
theorem integral_div (c : ℂ₁) (hf : IntervalIntegrableC f a b) :
    c∫ x in a..b, f x/c = (c∫ x in a..b, f x)/c := by
  convert integral_mul_const (1/c) hf using 1 <;> field_simp

@[simp]
theorem integral_const (c : ℂ₁) : c∫ _ in a..b, c = (b-a)•c := by
  simp [intervalIntegralC]
  ext
  . exact intervalIntegral.integral_const c.re
  . exact intervalIntegral.integral_const c.im

theorem integral_add_adjacent_intervals
    (hab : IntervalIntegrableC f a b) (hbc : IntervalIntegrableC f b c) :
    (c∫ x in a..b, f x) + (c∫ x in b..c, f x) = c∫ x in a..c, f x := by
  simp [intervalIntegralC]
  ext
  . exact intervalIntegral.integral_add_adjacent_intervals hab.left hbc.left
  . exact intervalIntegral.integral_add_adjacent_intervals hab.right hbc.right

theorem integral_re : ∫ x in a..b, (re ∘ f) x = (c∫ x in a..b, f x).re := by
  simp [intervalIntegralC]
  rfl

theorem integral_im : ∫ x in a..b, (im ∘ f) x = (c∫ x in a..b, f x).im := by
  simp [intervalIntegralC]
  rfl

theorem norm_integral_leq_integral_norm {f : ℝ → ℂ₁} (le : a ≤ b) (cont : ContinuousOn f (Set.uIcc a b)) :
    ‖c∫ x in a..b, f x‖ ≤ ∫ x in a..b, ‖f x‖ := by
  have int := cont.intervalIntegrableC
  by_cases nz : ‖c∫ x in a..b, f x‖ = 0
  . rw [nz]
    exact integral_nonneg_of_forall le (fun z ↦ norm_nonneg (f z))
  let r := (c∫ x in a..b, f x)/‖c∫ x in a..b, f x‖
  calc
    _ = ‖r⁻¹*(c∫ x in a..b, f x)‖ := by
      rw [NormedField.norm_mul', norm_inv, norm_div]
      simp
    _ = ‖c∫ x in a..b, r⁻¹*f x‖ := by
      rw [integral_const_mul _ int]
    _ = |∫ x in a..b, (r⁻¹*f x).re| := by
      rw [norm_eq_re_abs_of_im_zero, ← integral_re]; rfl
      rw [integral_const_mul _ int]
      unfold r
      field_simp
      rw [mul_div_cancel_right₀ _ (ne_zero_of_norm_ne_zero nz)]
      exact r_cast_im _
    _ ≤ ∫ x in a..b, |(r⁻¹*f x).re| := abs_integral_le_integral_abs le
    _ ≤ ∫ x in a..b, ‖r⁻¹*f x‖ := by
      apply intervalIntegral.integral_mono le _ _ (fun z ↦ abs_re_le_norm (r⁻¹*f z))
      . exact (int.const_mul r⁻¹).left.abs
      . apply ContinuousOn.intervalIntegrable
        show ContinuousOn (fun x ↦ ‖r⁻¹*f x‖) (Set.uIcc a b)
        apply continuous_norm.comp_continuousOn
        exact continuous_const.continuousOn.mul cont
    _ = ∫ x in a..b, ‖f x‖ := by
      simp_rw [NormedField.norm_mul']
      unfold r
      field_simp

-- intervals for f, g are the same if f, g are "same enough"

open Interval in
theorem integral_congr (h : Set.EqOn f g [[a, b]]) : c∫ x in a..b, f x = c∫ x in a..b, g x := by
  simp [intervalIntegralC]
  rw [intervalIntegral.integral_congr (h.comp_left (g := re)),
      intervalIntegral.integral_congr (h.comp_left (g := im))]
  simp [intervalIntegral]

theorem integral_congr_ae {f g : ℝ → ℂ₁} (h : ∀ᵐ x ∂volume, x ∈ Set.uIoc a b → f x = g x) :
    c∫ x in a..b, f x = c∫ x in a..b, g x := by
  simp [intervalIntegralC]
  have re_ae := h.mono (fun x p ↦ (fun xIn ↦ congrArg re (p xIn)))
  have im_ae := h.mono (fun x p ↦ (fun xIn ↦ congrArg im (p xIn)))
  constructor
  . apply (intervalIntegral.integral_congr_ae re_ae)
  . apply (intervalIntegral.integral_congr_ae im_ae)

theorem integral_congr_Ioo {f g : ℝ → ℂ₁} (h : Set.EqOn f g (Set.uIoo a b)) :
    c∫ x in a..b, f x = c∫ x in a..b, g x := by
  apply integral_congr_ae
  rw [MeasureTheory.ae_iff]
  have complSubset : { x | ¬(x ∈ Set.uIoc a b → f x = g x)} ⊆ {a, b} := by
    simp
    intro x ⟨xIn, neq⟩
    contrapose! neq
    simp at neq
    rw [Set.mem_uIoc] at xIn
    rcases xIn with ⟨gt, le⟩ | ⟨gt, le⟩
    . exact h (Set.mem_uIoo_of_lt gt (lt_of_le_of_ne le neq.right))
    . exact h (Set.mem_uIoo_of_gt gt (lt_of_le_of_ne le neq.left))
  apply Set.Finite.measure_zero
  apply Set.Finite.subset _ complSubset
  simp

-- Fundamental Theorem of Calculus 1

open Differentiation Interval Topology in
theorem integral_hasDeriv_of_continuousOn {f : ℝ → ℂ₁} (hf : ContinuousOn f [[a, b]])
    (hy : y ∈ Set.uIoo a b) : HasDerivAt (fun x ↦ c∫ t in a..x, f t) (f y) y := by
  simp [intervalIntegralC]
  have uIoo_a_b_sub : Set.uIoo a b ⊆ [[a, b]] := Set.uIoo_subset_uIcc a b
  have yIn : y ∈ [[a, b]] := uIoo_a_b_sub hy
  have uIcc_a_y_sub : [[a, y]] ⊆ [[a, b]] := Set.uIcc_subset_uIcc_left yIn
  apply hasDerivAt_r_c_of_components_hasDerivAt
  . have cont_uIcc_a_y := (re_continuous.comp_continuousOn hf).mono uIcc_a_y_sub
    have cont_uIoo_a_b := (re_continuous.comp_continuousOn hf).mono uIoo_a_b_sub
    have cont_y := cont_uIoo_a_b.continuousAt (IsOpen.mem_nhds isOpen_Ioo hy)
    exact integral_hasDerivAt_right
      cont_uIcc_a_y.intervalIntegrable
      (cont_uIoo_a_b.stronglyMeasurableAtFilter isOpen_Ioo y hy)
      cont_y
  . have cont_uIcc_a_y := (im_continuous.comp_continuousOn hf).mono uIcc_a_y_sub
    have cont_uIoo_a_b := (im_continuous.comp_continuousOn hf).mono uIoo_a_b_sub
    have cont_y := cont_uIoo_a_b.continuousAt (IsOpen.mem_nhds isOpen_Ioo hy)
    exact integral_hasDerivAt_right
      cont_uIcc_a_y.intervalIntegrable
      (cont_uIoo_a_b.stronglyMeasurableAtFilter isOpen_Ioo y hy)
      cont_y

open Differentiation Interval in
theorem integral_hasDeriv_of_continuous {f : ℝ → ℂ₁} (hf : Continuous f) :
    HasDerivAt (fun x ↦ c∫ t in a..x, f t) (f y) y := by
  simp [intervalIntegralC]
  apply hasDerivAt_r_c_of_components_hasDerivAt
  . have cont := re_continuous.comp hf
    exact integral_hasDerivAt_right
      (cont.intervalIntegrable _ _)
      (cont.stronglyMeasurableAtFilter _ _) cont.continuousAt
  . have cont := im_continuous.comp hf
    exact integral_hasDerivAt_right
      (cont.intervalIntegrable _ _)
      (cont.stronglyMeasurableAtFilter _ _) cont.continuousAt

open Interval in
theorem integral_deriv_of_continuousOn {f : ℝ → ℂ₁} (hf : ContinuousOn f [[a, b]])
    (hy : y ∈ Set.uIoo a b) : deriv (fun x ↦ c∫ t in a..x, f t) y = f y:= by
  exact (integral_hasDeriv_of_continuousOn hf hy).deriv

theorem integral_deriv_of_continuous {f : ℝ → ℂ₁} (hf : Continuous f) :
    deriv (fun x ↦ c∫ t in a..x, f t) = f := by
  funext x
  exact (integral_hasDeriv_of_continuous hf).deriv

open Interval in
theorem exists_antideriv_of_continuousOn {f : ℝ → ℂ₁} (hf : ContinuousOn f [[a, b]]) :
    ∃F, ∀ x ∈ Set.uIoo a b, HasDerivAt F (f x) x :=
  ⟨fun x ↦ c∫ t in (a : ℝ)..x, f t, fun _ xIn ↦ integral_hasDeriv_of_continuousOn hf xIn⟩

theorem exists_antideriv_of_continuous {f : ℝ → ℂ₁} (hf : Continuous f) :
    ∃F, ∀x, HasDerivAt F (f x) x :=
  ⟨fun x ↦ c∫ t in (0 : ℝ)..x, f t, fun _ ↦ integral_hasDeriv_of_continuous hf⟩

-- Fundamental Theorem of Calculus 2

open Interval Differentiation in
theorem integral_eq_sub_of_hasDerivAt_interior (h : ContinuousOn F [[a, b]])
    (h' : ∀ x ∈ Set.uIoo a b, HasDerivAt F (f x) x) (h'' : IntervalIntegrableC f a b) :
    c∫ x in a..b, f x = F b - F a := by
  wlog ab : a ≤ b
  . rw [Set.uIcc_comm] at h
    rw [Set.uIoo_comm] at h'
    apply IntervalIntegrableC.symm at h''
    rw [integral_symm, this h h' h'' (le_of_not_ge ab)]
    ring

  simp [intervalIntegralC]
  ext <;> simp
  . apply intervalIntegral.integral_eq_sub_of_hasDerivAt_of_le ab _ _ h''.left
    . convert re_continuous.comp_continuousOn h using 1
      rw [Set.uIcc_of_le ab]
    . intro x xIn
      rw [Set.uIoo_of_le ab] at h'
      exact (hasDerivAt_components_of_hasDerivAt_r_c (h' x xIn)).left
  . apply intervalIntegral.integral_eq_sub_of_hasDerivAt_of_le ab _ _ h''.right
    . convert im_continuous.comp_continuousOn h using 1
      rw [Set.uIcc_of_le ab]
    . intro x xIn
      rw [Set.uIoo_of_le ab] at h'
      exact (hasDerivAt_components_of_hasDerivAt_r_c (h' x xIn)).right

open Interval Differentiation in
theorem integral_eq_sub_of_hasDerivAt {f F : ℝ → ℂ₁} (h : ∀x ∈ [[a, b]], HasDerivAt F (f x) x)
    (h' : IntervalIntegrableC f a b) : c∫ x in a..b, f x = F b - F a :=
  integral_eq_sub_of_hasDerivAt_interior
    (fun x xIn ↦ (h x xIn).continuousAt.continuousWithinAt)
    (fun x xIn ↦ h x (Set.uIoo_subset_uIcc _ _ xIn))
    h'

open Interval Differentiation Set in
theorem integral_eq_sub_of_hasDerivWithinAt {f F : ℝ → ℂ₁} (h : ContinuousOn F [[a, b]])
    (h' : ∀ x ∈ [[a, b]], HasDerivWithinAt F (f x) [[a, b]] x) (h'' : IntervalIntegrableC f a b) :
    c∫ x in a..b, f x = F b - F a := by
  apply integral_eq_sub_of_hasDerivAt_interior h _ h''
  intro x xIn
  apply (h' x _).hasDerivAt
  . rw [← mem_interior_iff_mem_nhds, mem_interior]
    exact ⟨uIoo a b, uIoo_subset_uIcc _ _, isOpen_Ioo, xIn⟩
  . apply uIoo_subset_uIcc
    exact xIn

-- substitution rule

open Differentiation Interval in
theorem integral_by_substitution {f : ℝ → ℂ₁} {φ φ' : ℝ → ℝ}
    (cont : ContinuousOn f [[c, d]]) (mapsto : Set.MapsTo φ [[a, b]] [[c, d]])
    (exDeriv : ∀x ∈ [[a, b]], HasDerivAt φ (φ' x) x) (derivCont : ContinuousOn φ' [[a, b]]) :
    c∫ x in (φ a)..(φ b), f x = c∫ x in a..b, f (φ x) * φ' x := by

  let f_uIcc : [[c, d]] → ℂ₁ := ([[c, d]]).restrict f
  have f_uIcc_cont : Continuous f_uIcc := by
    rw [← continuousOn_iff_continuous_restrict]
    exact cont
  let f_ext : ℝ → ℂ₁ := Set.IccExtend (by aesop) f_uIcc
  have f_ext_cont : Continuous f_ext := f_uIcc_cont.Icc_extend'

  rw [integral_congr (f := f) (g := f_ext)]
  rw [integral_congr (f := fun x ↦ f (φ x) * φ' x) (g := fun x ↦ f_ext (φ x) * φ' x)]
  rotate_left
  . intro x xIn
    simp; left; symm
    apply Set.IccExtend_of_mem
    exact mapsto xIn
  . intro x xIn
    symm
    apply Set.IccExtend_of_mem
    have : [[φ a, φ b]] ⊆ [[c, d]] := by
      rw [Set.uIcc_subset_uIcc_iff_mem]
      refine' ⟨mapsto _, mapsto _⟩ <;> simp
    exact this xIn

  obtain ⟨F, Fderiv⟩ := exists_antideriv_of_continuous f_ext_cont
  have Fcont : Continuous F := by
    rw [continuous_iff_continuousAt]
    intro x
    exact (Fderiv x).continuousAt
  have compDeriv : ∀ x ∈ [[a, b]], HasDerivAt (F ∘ φ) (f_ext (φ x) * φ' x) x :=
    fun x xIn ↦ (Fderiv (φ x)).rccomp (exDeriv x xIn)
  rw [integral_eq_sub_of_hasDerivAt compDeriv _]
  rw [integral_eq_sub_of_hasDerivAt (fun x _ ↦ Fderiv x) (f_ext_cont.intervalIntegrableC _ _)]
  rfl
  apply ContinuousOn.intervalIntegrableC
  apply ContinuousOn.mul
  . exact f_ext_cont.comp_continuousOn (fun x xIn ↦ (exDeriv x xIn).continuousAt.continuousWithinAt)
  . exact (r_inj_c_continuous.continuousOn.comp derivCont (Set.mapsTo_image φ' [[a, b]]))

open Interval in
theorem integral_by_substitution' {f : ℝ → ℂ₁} {φ φ' : ℝ → ℝ}
    (cont : Continuous f) (exDeriv : ∀x ∈ [[a, b]], HasDerivAt φ (φ' x) x)
    (derivCont : ContinuousOn φ' [[a, b]]) :
    c∫ x in (φ a)..(φ b), f x = c∫ x in a..b, f (φ x) * φ' x := by
  wlog ab : a ≤ b
  . rw [Set.uIcc_comm] at *
    rw [integral_symm, integral_symm b, this cont exDeriv derivCont (le_of_not_ge ab)]
  have φcont : ContinuousOn φ [[a, b]] := fun x xIn ↦ (exDeriv x xIn).continuousAt.continuousWithinAt
  obtain ⟨min, isMin⟩ := isCompact_uIcc.exists_isMinOn Set.nonempty_uIcc φcont
  obtain ⟨max, isMax⟩ := isCompact_uIcc.exists_isMaxOn Set.nonempty_uIcc φcont
  rw [isMinOn_iff] at isMin
  rw [isMaxOn_iff] at isMax
  apply integral_by_substitution cont.continuousOn _ exDeriv derivCont (c := φ min) (d := φ max)
  intro x xIn
  rw [Set.uIcc_of_le]; rotate_left
  . linarith [isMin.right max isMax.left]
  simp
  constructor <;> linarith [isMin.right x xIn, isMax.right x xIn]
