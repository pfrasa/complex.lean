import Mathlib.Tactic
import Mathlib.Algebra.Field.Basic
import Mathlib.Data.Finset.Basic

open BigOperators
open Finset

namespace General.Algebra

theorem pascal_identity (n k : ℕ) : (Nat.choose n (k+1)) + (Nat.choose n k) = Nat.choose (n+1) (k+1) := by
  induction' n with n' ih
  . simp [Nat.choose_zero_succ, zero_add]
    rfl
  . rw [Nat.choose, add_comm (Nat.choose n' k), ih]
    nth_rewrite 2 [Nat.choose]
    ring

theorem binomial_theorem [Field F] (a b : F) (n : ℕ) : (a + b)^n = ∑ k in range (n+1), (Nat.choose n k) * a^(n-k)*b^k := by
  induction' n with n' ih
  . simp
  . rw [pow_succ, ih, sum_mul]
    calc
      _ = ∑ k in range (n'+1), (((Nat.choose n' k) * a^(n'+1-k) * b^k) + ((Nat.choose n' k) * a^(n'-k) * b^(k+1))) := by
        apply sum_congr rfl
        intro k kir
        calc
          _ = (Nat.choose n' k) * (a * a^(n'-k) *b^k + b*a^(n'-k)*b^k) := by ring
          _ = _ := by
            rw [mul_comm a, ← pow_succ, mul_comm b, mul_assoc _ b, mul_comm b, ← pow_succ]
            rw [← Nat.sub_add_comm (Nat.le_of_lt_succ (mem_range.mp kir)), mul_add, mul_assoc, mul_assoc]
      _ = ∑ k in range (n'+1), ((Nat.choose n' k) * a^(n'+1-k) * b^k) + ∑ k in range (n'+1), ((Nat.choose n' k) * a^(n'-k) * b^(k+1)) := by
        rw [sum_add_distrib]
      _ = (∑ k in range (n'+1), ((Nat.choose n' k) * a^(n'+1-k) * b^k)) + (∑ k in range n', ((Nat.choose n' k) * a^(n'-k) * b^(k+1)) + b^(n'+1)) := by
        congr
        convert sum_range_add _ n' 1
        simp
      _ = (a^(n'+1) + ∑ k in range n', ((Nat.choose n' (k+1)) * a^(n'-k) * b^(k+1))) + (∑ k in range n', ((Nat.choose n' k) * a^(n'-k) * b^(k+1)) + b^(n'+1)) := by
        congr 1
        rw [add_comm n' 1, sum_range_add]
        congr 1
        . simp
        . apply sum_congr rfl
          intro k _
          congr 2 <;> ring_nf
          congr 1
          rw [← Nat.sub_sub, add_comm, Nat.add_sub_cancel]
      _ = a^(n'+1) + ∑ k in range n', ((Nat.choose n' (k+1) + Nat.choose n' k) * a^(n'-k) * b^(k+1)) + b^(n'+1) := by
        rw [add_assoc]
        nth_rewrite 2 [← add_assoc, add_assoc]
        congr
        rw [← sum_add_distrib]
        apply sum_congr rfl
        intro k _
        ring
      _ = a^(n'+1) + ∑ k in range n', ((Nat.choose (n'+1) (k+1)) * a^(n'-k) * b^(k+1)) + b^(n'+1) := by
        congr 2
        apply sum_congr rfl
        intro k _
        congr
        rw [← pascal_identity]
        simp
      _ = a^(n'+1) + ∑ k in range (n'+1), ((Nat.choose (n'+1) (k+1)) * a^(n'-k) * b^(k+1)) := by
        rw [add_assoc]
        congr
        rw [sum_range_succ]
        simp
      _ = _ := by
        rw [add_comm n'.succ 1, sum_range_add _ 1 n'.succ]
        congr 1
        . simp
        . apply sum_congr rfl
          intro k _
          congr 2
          . congr 2; linarith
          . rw [Nat.sub_add_eq, Nat.succ_sub_one]
          . linarith

lemma geometric_sum_qf [Field F] (x : F) (n : ℕ) : (1-x)*(∑ k in range (n+1), x^k) = 1-x^(n+1) := by
  induction' n with n ih
  . simp
  rw [sum_range_add, mul_add, ih, sub_mul]
  field_simp
  simp [pow_add, mul_comm]

theorem geometric_sum [Field F] {x : F} (xne1 : x ≠ 1) (n : ℕ) : ∑ k in range (n+1), x^k = (1-x^(n+1)) / (1-x) := by
  apply eq_div_of_mul_eq
  . intro contra
    apply_fun (. + x) at contra
    simp at contra
    symm at contra
    contradiction
  rw [mul_comm]
  apply geometric_sum_qf
