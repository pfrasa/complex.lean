import Mathlib.Analysis.Calculus.Deriv.Prod
import Mathlib.Analysis.Calculus.MeanValue
import Mathlib.Analysis.InnerProductSpace.Calculus
import Mathlib.Analysis.Normed.Order.Lattice
import Mathlib.Topology.EMetricSpace.Paracompact

namespace General.Differentiation

open Filter Set Real

section
variable [NontriviallyNormedField F]

open scoped Classical in
theorem hasDerivAt_iff_exists_continuous {f : F → F} :
    HasDerivAt f f' x₀ ↔ ∃ f₁, (∀ x, f x = f x₀ + (x-x₀) * f₁ x) ∧ f₁ x₀ = f' ∧ ContinuousAt f₁ x₀ := by
  simp_rw [hasDerivAt_iff_tendsto, ← norm_inv, ← norm_mul, ← tendsto_zero_iff_norm_tendsto_zero, smul_eq_mul]
  let f₁ (x : F) := if x = x₀ then f' else (f x - f x₀) / (x - x₀)
  have f₁eq : f₁ = fun x ↦ (x-x₀)⁻¹*(f x - f x₀ - (x-x₀)*f') + f' := by
    funext x
    by_cases xx₀ : x = x₀
    . rw [xx₀, sub_self, CommGroupWithZero.inv_zero, zero_mul, zero_add]
      unfold f₁
      simp
    . rw [mul_sub, inv_mul_cancel_left₀ (sub_ne_zero_of_ne xx₀)]
      unfold f₁
      simp [if_neg xx₀]
      ring
  have f_lin (x : F) : f x = f x₀ + (x-x₀) * f₁ x := by
    by_cases xx₀ : x = x₀
    . rw [xx₀, sub_self, zero_mul, add_zero]
    . unfold f₁
      simp_rw [if_neg xx₀]
      rw [mul_div_cancel₀ _ (sub_ne_zero_of_ne xx₀)]
      ring

  constructor
  . intro tt
    refine' ⟨f₁, fun x ↦ f_lin x, if_pos rfl, _⟩
    apply continuousAt_of_tendsto_nhds (y := f')
    rw [f₁eq]
    nth_rw 3 [← zero_add f']
    apply Tendsto.add tt
    exact tendsto_const_nhds
  . rintro ⟨f₁', f_lin', f₁'x₀, f₁'_cont⟩
    have f₁f₁' : f₁ = f₁' := by
      funext x
      by_cases xx₀ : x = x₀
      . unfold f₁
        symm
        simpa [xx₀]
      . specialize f_lin x
        specialize f_lin' x
        rwa [f_lin, add_right_inj, mul_right_inj' (sub_ne_zero_of_ne xx₀)] at f_lin'
    have f₁eq' : (fun x ↦ (x-x₀)⁻¹*(f x - f x₀ - (x-x₀)*f')) = f₁ - (fun _ ↦ f') := by
      rw [f₁eq]
      funext x
      simp
    rw [f₁eq', f₁f₁', ← sub_self f', ← f₁'x₀]
    exact Tendsto.sub f₁'_cont.tendsto tendsto_const_nhds

open scoped RealInnerProductSpace in
theorem mean_value_ineq_vector_valued  (f : ℝ → EuclideanSpace ℝ (Fin n)) (a b : ℝ) (lt : a < b)
    (cont : ContinuousOn f (Icc a b)) (diff : DifferentiableOn ℝ f (Ioo a b)) :
    ∃ c, c ∈ Ioo a b ∧ ‖f b - f a‖ ≤ (b-a)*‖deriv f c‖ := by
  have f_mapsto : MapsTo f (Icc a b) (f '' (Icc a b)) := by exact mapsTo_image _ _
  let φ := fun t ↦ ⟪f b - f a, f t⟫
  have φ_eq : φ = (fun t ↦ ⟪f b - f a, t⟫) ∘ f := by
    funext t
    unfold φ; simp
  have φ_cont : ContinuousOn φ (Icc a b) := by
    rw [φ_eq]
    apply ContinuousOn.comp _ cont f_mapsto
    simp
    apply continuousOn_finset_sum; intros
    apply ContinuousOn.mul continuousOn_const
    apply continuousOn_pi.mp continuousOn_id
  have φ_deriv : ∀ t ∈ Ioo a b, HasDerivAt φ ⟪f b - f a, deriv f t⟫ t := by
    intro t tIn
    have : ⟪f b - f a, deriv f t⟫ = ⟪f b - f a, deriv f t⟫ + ⟪0, f t⟫ := by simp
    rw [this]
    apply HasDerivAt.inner
    . apply hasDerivAt_const
    exact diff.hasDerivAt (IsOpen.mem_nhds isOpen_Ioo tIn)
  have φ_diff : DifferentiableOn ℝ φ (Ioo a b) := by
    intro t tIn
    apply (φ_deriv t tIn).differentiableAt.differentiableWithinAt
  obtain ⟨c, cIn, ceq⟩ := exists_deriv_eq_slope φ lt φ_cont φ_diff
  refine' ⟨c, cIn, _⟩

  suffices ‖f b - f a‖^2 ≤ ‖f b - f a‖*(b-a)*‖deriv f c‖ by
    by_cases eq : f a = f b
    . simp [eq]
      apply mul_nonneg
      . linarith
      . exact norm_nonneg _
    rw [pow_two, mul_assoc] at this
    apply (mul_le_mul_left _).mp this
    apply lt_of_le_of_ne (norm_nonneg _) (norm_ne_zero_iff.mpr (sub_ne_zero_of_ne (by tauto))).symm

  calc
    ‖f b - f a‖^2 = φ b - φ a := by
      rw [← real_inner_self_eq_norm_sq, ← inner_sub_right]
    _ = (b-a)*(deriv φ c) := by
      rw [ceq, mul_div_cancel₀]
      linarith
    _ = (b-a)*⟪(f b - f a), (deriv f c)⟫ := by
      rw [(φ_deriv _ cIn).deriv]
    _ ≤ _ := by
      rw [mul_comm _ (b-a), mul_assoc]
      apply (mul_le_mul_iff_of_pos_left (by linarith)).mpr
      apply le_trans (le_norm_self _)
      apply norm_inner_le_norm

end

section
private abbrev RV (n : ℕ) := Fin n → ℝ

variable [NormedAddCommGroup F] [NormedSpace ℝ F]

def HasPartialDerivAt (i : Fin n) (f : RV n → F) (dfi : F) (a : RV n) :=
  HasDerivAt (f ∘ (Function.update a i)) dfi (a i)

theorem hasPartialDerivAt_pi {i : Fin n} {f : RV n → RV m} :
    HasPartialDerivAt i f dfi a ↔ ∀ j, HasPartialDerivAt i ((fun v ↦ v j) ∘ f) (dfi j) a := hasDerivAt_pi

theorem HasPartialDerivAt.add {f g : RV n → F}
    (hf : HasPartialDerivAt i f dfi a) (hg : HasPartialDerivAt i g dgi a) :
    HasPartialDerivAt i (f + g) (dfi + dgi) a := HasDerivAt.add hf hg

theorem HasPartialDerivAt.add_const {f : RV n → F} (c : F) (hf : HasPartialDerivAt i f dfi a) :
    HasPartialDerivAt i (fun x ↦ f x + c) dfi a := HasDerivAt.add_const hf c

theorem HasPartialDerivAt.const_add {f : RV n → F} (c : F) (hf : HasPartialDerivAt i f dfi a) :
    HasPartialDerivAt i (fun x ↦ c + f x) dfi a := HasDerivAt.const_add c hf

theorem HasPartialDerivAt.neg {f : RV n → F} (hf : HasPartialDerivAt i f dfi a) :
    HasPartialDerivAt i (Neg.neg ∘ f) (-dfi) a := HasDerivAt.neg hf

theorem HasPartialDerivAt.sub {f g : RV n → F}
    (hf : HasPartialDerivAt i f dfi a) (hg : HasPartialDerivAt i g dgi a) :
    HasPartialDerivAt i (f - g) (dfi - dgi) a := HasDerivAt.sub hf hg

theorem HasPartialDerivAt.const_sub {f : RV n → F} (c : F) (hf : HasPartialDerivAt i f dfi a) :
    HasPartialDerivAt i (fun x ↦ c - f x) (-dfi) a := HasDerivAt.const_sub c hf

theorem HasPartialDerivAt.sub_const {f : RV n → F} (c : F) (hf : HasPartialDerivAt i f dfi a) :
    HasPartialDerivAt i (fun x ↦ f x - c) dfi a := HasDerivAt.sub_const hf c

noncomputable def partialDeriv (f : RV n → RV m) (i : Fin n) (a : RV n) : RV m :=
  deriv (f ∘ (Function.update a i)) (a i)
