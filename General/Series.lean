import Mathlib.Data.Real.StarOrdered
import General.Algebra

namespace General.Series

open Topology Filter BigOperators Finset General.Algebra

variable [NormedField F] {s : ℕ → F}

-- Basic definitions

@[reducible]
def SeriesConvergesTo (s : ℕ → F) (x : F) : Prop := Tendsto (fun n ↦ ∑ k in range (n+1), s k) atTop (𝓝 x)

@[reducible]
def SeriesConvergent (s : ℕ → F) : Prop := ∃ x, SeriesConvergesTo s x

@[reducible]
def SeriesDivergent (s : ℕ → F) : Prop := ¬ SeriesConvergent s

@[reducible]
def AbsolutelyConvergent (s : ℕ → F) : Prop := SeriesConvergent (norm ∘ s)

open scoped Classical in
noncomputable def isum (s : ℕ → F) : F :=
  if conv : ∃ x, SeriesConvergesTo s x then conv.choose else 0

@[reducible]
noncomputable def isum_from (s : ℕ → F) (n : ℕ) : F := isum (fun k ↦ s (n + k))

notation "∑ᵢ " s => isum s
notation "∑ᵢfrom " n ", " s => isum_from s n

theorem SeriesConvergesTo.get_isum (convTo : SeriesConvergesTo s x) : (∑ᵢ s) = x := by
  have conv : SeriesConvergent s := ⟨x, convTo⟩
  rw [isum, dif_pos conv]
  exact tendsto_nhds_unique conv.choose_spec convTo

theorem SeriesDivergent.get_isum (div : SeriesDivergent s) : (∑ᵢ s) = 0 := by rw [isum, dif_neg div]

-- Basic operations on series and convergence

theorem SeriesConvergesTo.from (conv : SeriesConvergesTo s x) :
    SeriesConvergesTo (fun k ↦ s (n+k)) (x - ∑ k in range n, s k) := by
  simp_rw [SeriesConvergesTo, Metric.tendsto_nhds, eventually_atTop] at *
  intro ε ε0
  obtain ⟨N, Nineq⟩ := conv ε ε0
  use N
  intro m mgeN
  simp [add_comm, ← sum_range_add, ← add_assoc]
  exact Nineq (n+m) (by omega)

theorem SeriesConvergesTo.of_from (conv : SeriesConvergesTo (fun k ↦ s (n+k)) x) :
    SeriesConvergesTo s (x + ∑ k in range n, s k) := by
  simp_rw [SeriesConvergesTo, Metric.tendsto_nhds, eventually_atTop] at *
  intro ε ε0
  obtain ⟨N, Nineq⟩ := conv ε ε0
  use N+n
  intro m mge
  rw [NormedField.dist_eq, add_comm x, ← sub_sub, ← NormedField.dist_eq]
  convert Nineq (m-n) (by omega) using 2
  apply sub_eq_of_eq_add
  rw [add_comm _ (∑ k in range n, s k), ← sum_range_add]
  congr 2
  omega

theorem SeriesConvergesTo.add (conv₁ : SeriesConvergesTo s x) (conv₂ : SeriesConvergesTo t y) :
    SeriesConvergesTo (fun k ↦ s k + t k) (x + y) := by
  simp_rw [SeriesConvergesTo, sum_add_distrib]
  apply Tendsto.add conv₁ conv₂

theorem SeriesConvergesTo.iff_neg : SeriesConvergesTo s x ↔ SeriesConvergesTo (fun z ↦ -(s z)) (-x) := by
  simp_rw [SeriesConvergesTo, sum_neg_distrib]
  constructor
  . exact Tendsto.neg
  . have eq₁ : (fun n ↦ ∑ k in range (n+1), s k) = (fun n ↦ -(-∑ k in range (n+1), s k)) := by simp
    have eq₂ : 𝓝 x = 𝓝 (-(-x)) := by simp
    rw [eq₁, eq₂]
    exact Tendsto.neg

theorem SeriesConvergesTo.const_mul : SeriesConvergesTo s x → SeriesConvergesTo (fun k ↦ a * (s k)) (a*x) := by
  simp_rw [SeriesConvergesTo, mul_comm a, ← sum_mul]
  exact Tendsto.mul_const a

theorem SeriesConvergent.from (conv : SeriesConvergent s) : SeriesConvergent (fun k ↦ s (n+k)) :=
  ⟨_, conv.choose_spec.from⟩

theorem SeriesConvergent.of_from (conv : SeriesConvergent (fun k ↦ s (n+k))) : SeriesConvergent s :=
  ⟨_, conv.choose_spec.of_from⟩

theorem SeriesConvergent.add (conv₁ : SeriesConvergent s) (conv₂ : SeriesConvergent t) : SeriesConvergent (fun k ↦ s k + t k) :=
  ⟨_, conv₁.choose_spec.add conv₂.choose_spec⟩

theorem SeriesConvergent.iff_neg : SeriesConvergent s ↔ SeriesConvergent (fun k ↦ -(s k)) := by
  constructor
  . rintro ⟨_, convto_x⟩
    exact ⟨_, SeriesConvergesTo.iff_neg.mp convto_x⟩
  . rintro ⟨neg_x, convto_neg_x⟩
    use -neg_x
    convert SeriesConvergesTo.iff_neg.mp convto_neg_x
    simp

theorem SeriesConvergent.const_mul (conv : SeriesConvergent s) : SeriesConvergent (fun k ↦ a * (s k)) := by
  obtain ⟨x, convto_x⟩ := conv
  exact ⟨_, convto_x.const_mul⟩

theorem AbsolutelyConvergent.from (abs_conv : AbsolutelyConvergent s) : AbsolutelyConvergent (fun k ↦ s (n+k)) :=
  SeriesConvergent.from abs_conv

theorem AbsolutelyConvergent.of_from (abs_conv : AbsolutelyConvergent (fun k ↦ s (n+k))) : AbsolutelyConvergent s := by
  rw [AbsolutelyConvergent] at *
  exact SeriesConvergent.of_from abs_conv

-- AbsolutelyConvergent.add can only be proven after we have proven the direct comparison theorem

theorem AbsolutelyConvergent.iff_neg : AbsolutelyConvergent s ↔ AbsolutelyConvergent (fun k ↦ -(s k)) := by
  have fun_eq : norm ∘ (fun k ↦ -(s k)) = norm ∘ s := by ext; simp
  constructor <;> (simp_rw [AbsolutelyConvergent, SeriesConvergent, fun_eq]; tauto)

theorem AbsolutelyConvergent.const_mul (abs_conv : AbsolutelyConvergent s) : AbsolutelyConvergent (fun k ↦ a * (s k)) := by
  obtain ⟨x, convto_x⟩ := abs_conv
  use ‖a‖*x
  convert convto_x.const_mul
  simp

theorem isum_from_of_series_convergent (conv : SeriesConvergent s) :
    (∑ᵢfrom n, s) = (∑ᵢ s) - ∑ k in range n, s k := by
  obtain ⟨x, convto_x⟩ := conv
  rw [convto_x.get_isum, isum_from, convto_x.from.get_isum]

theorem isum_add_of_series_convergent_of_series_convergent {s t : ℕ → F} (conv₁ : SeriesConvergent s) (conv₂ : SeriesConvergent t) :
    (∑ᵢ fun k ↦ s k + t k) = (∑ᵢ s) + ∑ᵢ t := by
  obtain ⟨x, convto_x⟩ := conv₁
  obtain ⟨y, convto_y⟩ := conv₂
  rw [convto_x.get_isum, convto_y.get_isum, SeriesConvergesTo.get_isum (convto_x.add convto_y)]

theorem isum_neg_of_series_convergent (conv : SeriesConvergent s) : (∑ᵢ fun k ↦ -(s k)) = -(∑ᵢ s) := by
  obtain ⟨x, convto_x⟩ := conv
  rw [convto_x.get_isum, SeriesConvergesTo.get_isum (SeriesConvergesTo.iff_neg.mp convto_x)]

theorem isum_neg_of_neg_series_convergent (conv : SeriesConvergent (fun k ↦ -(s k))) : (∑ᵢ fun k ↦ - (s k)) = -(∑ᵢ s) := by
  obtain ⟨neg_x, convto_neg_x⟩ := conv
  have : neg_x = -(-neg_x) := by simp
  rw [this] at convto_neg_x
  rw [convto_neg_x.get_isum, SeriesConvergesTo.get_isum (SeriesConvergesTo.iff_neg.mpr convto_neg_x)]

theorem isum_const_mul_of_series_convergent (conv : SeriesConvergent s) : (∑ᵢ fun k ↦ a * (s k)) = a * ∑ᵢ s := by
  obtain ⟨x, convto_x⟩ := conv
  rw [convto_x.get_isum, SeriesConvergesTo.get_isum (convto_x.const_mul)]

-- Series converges -> terms tend to zero

theorem termseq_tendsto_zero_of_series_convergent (conv : SeriesConvergent s) : Tendsto s atTop (𝓝 0) := by
  apply tendsto_zero_iff_norm_tendsto_zero.mpr
  apply Metric.tendsto_nhds.mpr
  intro ε ε0
  apply eventually_atTop.mpr
  obtain ⟨_, convTo⟩ := conv
  have seriesCauchy := convTo.cauchySeq
  rw [Metric.cauchySeq_iff] at seriesCauchy
  rcases seriesCauchy ε ε0 with ⟨N, h⟩
  use N+1
  intro n ngeN1
  have filter_eq : filter (fun k ↦ n - 1 + 1 ≤ k) (range (n + 1)) = {n} := by
    ext k
    constructor <;> (simp; omega)
  have := h n (by linarith) (n-1) (Nat.le_sub_one_of_lt ngeN1)
  rw [NormedField.dist_eq, sum_range_sub_sum_range (by omega), filter_eq, sum_singleton] at this
  rwa [dist_zero_right, norm_norm]

theorem series_divergent_of_termseq_not_tendsto_zero (nt : ¬ Tendsto s atTop (𝓝 0)) : SeriesDivergent s :=
  termseq_tendsto_zero_of_series_convergent.mt nt

-- Geometric series

theorem geometric_series_converges_to {z : F} (zlt1 : ‖z‖ < 1) : SeriesConvergesTo (fun i ↦ z^i) (1 / (1-z)) := by
  have fun_eq : (fun n ↦ ∑ k in range (n+1), z^k) = (fun n ↦ (1-z^(n+1))/(1-z)) := by
    ext n
    apply geometric_sum
    intro z1
    apply_fun norm at z1
    rw [norm_one] at z1
    linarith
  rw [SeriesConvergesTo, fun_eq]
  apply Tendsto.div
  . nth_rw 3 [← sub_zero 1]
    apply Tendsto.sub tendsto_const_nhds
    simp_rw [pow_succ]
    rw [← zero_mul z]
    exact Tendsto.mul (tendsto_pow_atTop_nhds_zero_of_norm_lt_one zlt1) tendsto_const_nhds
  . exact tendsto_const_nhds
  . intro z1
    rw [sub_eq_zero] at z1
    apply_fun norm at z1
    rw [norm_one] at z1
    linarith

theorem geometric_series_val {z : F} (zlt1 : ‖z‖ < 1) : (∑ᵢ fun i ↦ z^i) = 1 / (1-z) := by
  apply SeriesConvergesTo.get_isum
  apply geometric_series_converges_to zlt1

-- Direct comparison test

lemma finsum_le_by_direct_comparison {t : ℕ → ℝ} (n₀ : ℕ) (dominates : ∀ n ≥ n₀, ‖s n‖ ≤ t n)
    (set : Finset ℕ) (gen₀ : ∀ n ∈ set, n ≥ n₀) :
    ‖∑ k in set, s k‖ ≤ ∑ k in set, t k := by
  calc
    _ ≤ ∑ k in set, ‖s k‖ := norm_sum_le _ _
    _ ≤ ∑ k in set, t k := by
      apply sum_le_sum
      intro k kSet
      exact dominates k (gen₀ k kSet)

lemma convergent_by_direct_comparison [CompleteSpace F] {t : ℕ → ℝ} (n₀ : ℕ)
    (dom_conv: SeriesConvergent t) (dominates : ∀ n ≥ n₀, ‖s n‖ ≤ t n) :
    SeriesConvergent s := by
  rcases dom_conv with ⟨dom_isum, dom_convto⟩
  apply cauchySeq_tendsto_of_complete
  rw [Metric.cauchySeq_iff]
  intro ε ε0
  have dom_cauchy := dom_convto.cauchySeq
  rw [Metric.cauchySeq_iff] at dom_cauchy
  obtain ⟨n₁, dom_ineq⟩ := dom_cauchy ε ε0
  let N := max n₀ n₁
  use N
  intro m mgeN n ngeN
  wlog mgen : (m ≥ n) generalizing m n
  . rw [dist_comm]
    exact this n ngeN m mgeN (le_of_not_ge mgen)
  have dom_ineq' := dom_ineq m (by omega) n (by omega)
  rw [NormedField.dist_eq, sum_range_sub_sum_range (by omega)] at *
  set nmI := filter (fun k ↦ n + 1 ≤ k) (range (m+1))
  calc
    ‖∑ k in nmI, s k‖ ≤ ∑ k in nmI, t k := by
      apply finsum_le_by_direct_comparison n₀ dominates
      intro k kI
      have := (mem_filter.mp kI).2
      omega
    _ ≤ ‖∑ k in nmI, t k‖:= by
      simp
      rw [abs_sum_of_nonneg]
      intro k kI
      have := (mem_filter.mp kI).2
      exact le_trans (norm_nonneg _) (dominates k (by omega))
    _ < ε := dom_ineq'

theorem absolutely_convergent_by_direct_comparison {t : ℕ → ℝ} (n₀ : ℕ)
    (dom_conv: SeriesConvergent t) (dominates : ∀ n ≥ n₀, ‖s n‖ ≤ t n) :
    AbsolutelyConvergent s :=
  convergent_by_direct_comparison n₀ dom_conv (by simpa)

-- Ratio test

lemma dominates_by_ratio_test (n₀ : ℕ) {q : ℝ} (qgt0 : q > 0)
    (ne_zero : ∀ n ≥ n₀, s n ≠ 0) (ratio_le : ∀ n ≥ n₀, ‖(s (n+1))/(s n)‖ ≤ q) :
    ∀ n ≥ n₀, ‖s n‖ ≤ ‖s n₀‖*q^(n-n₀) := by
  intro n nge
  set m := n - n₀
  have : n = n₀ + m := by omega
  rw [this]
  induction' m with m' ih
  . simp
  . calc
      ‖s (n₀ + m'.succ)‖ ≤ ‖s (n₀ + m')‖*q := by
        apply (div_le_iff₀' _).mp
        . rw [← norm_div]
          refine' ratio_le _ _
          simp
        . have ge0 : ‖s (n₀ + m')‖ ≥ 0 := norm_nonneg _
          have ne0 : s (n₀ + m') ≠ 0 := ne_zero _ (by omega)
          exact lt_of_le_of_ne ge0 (norm_ne_zero_iff.mpr ne0).symm
      _ ≤ ‖s n₀‖*q^m'.succ := by
        rw [pow_succ, ← mul_assoc]
        exact (mul_le_mul_iff_of_pos_right qgt0).mpr ih

theorem absolutely_convergent_by_ratio_test (n₀ : ℕ) {q : ℝ} (qgt0 : q > 0) (qlt1 : q < 1)
    (ne_zero : ∀ n ≥ n₀, s n ≠ 0) (ratio_le : ∀ n ≥ n₀, ‖(s (n+1))/(s n)‖ ≤ q) :
    AbsolutelyConvergent s := by
  suffices AbsolutelyConvergent (fun k ↦ s (n₀+k)) from this.of_from
  suffices SeriesConvergent (fun k ↦ ‖s n₀‖*q^k) by
    refine' absolutely_convergent_by_direct_comparison n₀ this _
    intro n nge
    convert (dominates_by_ratio_test n₀ qgt0 ne_zero ratio_le) (n₀+n) (by omega)
    simp
  apply SeriesConvergent.const_mul
  refine' ⟨_, geometric_series_converges_to _⟩
  apply abs_lt.mpr
  constructor <;> linarith

-- Absolute convergence -> convergence


theorem AbsolutelyConvergent.convergent [CompleteSpace F] (abs : AbsolutelyConvergent s) : SeriesConvergent s :=
  convergent_by_direct_comparison 0 abs (by simp)

theorem AbsolutelyConvergent.norm_le [CompleteSpace F] (abs : AbsolutelyConvergent s) : ‖∑ᵢ (s)‖ ≤ ∑ᵢ (norm ∘ s) := by
  have abs' := abs
  rcases abs' with ⟨absv, absconv⟩
  rw [isum, dif_pos abs.convergent, absconv.get_isum]
  apply le_of_tendsto_of_tendsto' (Tendsto.norm abs.convergent.choose_spec) absconv
  intro n
  apply norm_sum_le

-- Inequalities for direct comparison and ratio tests

theorem norm_le_by_direct_comparison {t : ℕ → ℝ} (n₀ : ℕ)
    (dom_conv: SeriesConvergent t) (dominates : ∀ n ≥ n₀, ‖s n‖ ≤ t n) :
    ∀ n ≥ n₀, (∑ᵢfrom n, fun k ↦ ‖s k‖) ≤ ∑ᵢfrom n, (t) := by
  intro n nge
  have s_from_abs_conv :=
    (absolutely_convergent_by_direct_comparison n₀ dom_conv dominates).from (n := n)
  obtain ⟨x, convto_x⟩ := s_from_abs_conv
  obtain ⟨y, convto_y⟩ := dom_conv.from (n := n)
  unfold Function.comp at convto_x
  rw [isum_from, convto_x.get_isum, isum_from, convto_y.get_isum]
  apply le_of_tendsto_of_tendsto' convto_x convto_y
  intro l
  apply sum_le_sum
  intro m _
  simp
  exact dominates (n+m) (by omega)

theorem norm_le_by_direct_comparison' [CompleteSpace F] {t : ℕ → ℝ} (n₀ : ℕ)
    (dom_conv: SeriesConvergent t) (dominates : ∀ n ≥ n₀, ‖s n‖ ≤ t n) :
    ∀ n ≥ n₀, ‖∑ᵢfrom n, (s)‖ ≤ ∑ᵢfrom n, (t) := by
  intro n nge
  have s_from_abs_conv :=
    (absolutely_convergent_by_direct_comparison n₀ dom_conv dominates).from (n := n)
  apply le_trans s_from_abs_conv.norm_le
  apply norm_le_by_direct_comparison n₀ dom_conv dominates
  exact nge

theorem norm_le_by_ratio_test (n₀ : ℕ) {q : ℝ} (qgt0 : q > 0) (qlt1 : q < 1)
    (ne_zero : ∀ n ≥ n₀, s n ≠ 0) (ratio_le : ∀ n ≥ n₀, ‖(s (n+1))/(s n)‖ ≤ q) :
    ∀ n ≥ n₀, (∑ᵢfrom n, fun k ↦ ‖s k‖) ≤ ‖s n₀‖ / (1 - q) := by
  have dominates : ∀ n ≥ 0, ‖s (n₀+n)‖ ≤ ‖s n₀‖ * q^n := by
    intro n _
    convert (dominates_by_ratio_test n₀ qgt0 ne_zero ratio_le) (n₀+n) (by omega)
    simp
  have dom_conv_to : SeriesConvergesTo (fun k ↦ ‖s n₀‖ * q^k) (‖s n₀‖*(1/(1-q))) := by
    refine' (geometric_series_converges_to _).const_mul
    apply abs_lt.mpr
    constructor <;> linarith

  intro n nge
  apply le_trans
  . convert (norm_le_by_direct_comparison 0 _ dominates) (n-n₀) (by omega) using 1
    . simp [isum_from, ← add_assoc, Nat.add_sub_cancel' nge]
    . exact ⟨_, dom_conv_to⟩
  . have := dom_conv_to.from (n := n-n₀)
    rw [isum_from, this.get_isum, ← mul_div_assoc, mul_one]
    apply sub_le_self
    apply sum_nonneg
    intro k _
    apply mul_nonneg (norm_nonneg _)
    apply pow_nonneg
    linarith

theorem norm_le_by_ratio_test' [CompleteSpace F] (n₀ : ℕ) {q : ℝ} (qgt0 : q > 0) (qlt1 : q < 1)
    (ne_zero : ∀ n ≥ n₀, s n ≠ 0) (ratio_le : ∀ n ≥ n₀, ‖(s (n+1))/(s n)‖ ≤ q) :
    ∀ n ≥ n₀, ‖∑ᵢfrom n, (s)‖ ≤ ‖s n₀‖ / (1 - q) := by
  intro n nge
  have s_from_abs_conv :=
    (absolutely_convergent_by_ratio_test n₀ qgt0 qlt1 ne_zero ratio_le).from (n := n)
  apply le_trans s_from_abs_conv.norm_le
  apply norm_le_by_ratio_test _ qgt0 qlt1 ne_zero ratio_le
  exact nge

-- Sum of absolutely convergent series is absolutely convergent

theorem AbsolutelyConvergent.add (abs_conv₁ : AbsolutelyConvergent s) (abs_conv₂ : AbsolutelyConvergent t) :
    AbsolutelyConvergent (fun k ↦ s k + t k) := by
  apply absolutely_convergent_by_direct_comparison 0
  . apply SeriesConvergent.add abs_conv₁ abs_conv₂
  . intros
    apply norm_add_le

-- Multiplication Theorem

namespace Multiplication

variable (s t : ℕ → F)

def CauchyProd : ℕ → F := fun n ↦ ∑ k in (range (n+1)), (s (n-k)) * (t k)

@[reducible]
private def CauchyProdPartialSum : ℕ → F := fun n ↦ ∑ k in range (n+1), CauchyProd s t k

@[reducible]
private def PartialSumsProd : ℕ → F := fun n ↦ (∑ k in range (n+1), s k) * (∑ k in range (n+1), t k)

private lemma cauchyProd_eq (n : ℕ) :
    CauchyProd s t n =
    ∑ ij in filter (fun ⟨i,j⟩ ↦ i+j = n) ((range (n+1)) ×ˢ (range (n+1))), (s ij.fst) * (t ij.snd) := by
  rw [CauchyProd]
  apply sum_nbij (fun k ↦ ⟨n-k, k⟩)
  . intro k kRange
    rw [mem_range] at kRange
    rw [mem_filter]
    constructor
    . rw [mem_product]
      constructor <;> (rw [mem_range]; omega)
    . omega
  . intro k _ l _ feq
    apply_fun (·.snd) at feq
    simp at feq
    exact feq
  . rintro ⟨k, l⟩ klRange
    simp at klRange
    use l
    simp
    omega
  . intro k _
    simp

private def pair_sum_equiv : Setoid (ℕ × ℕ) := {
  r := fun ⟨a, b⟩ ⟨c, d⟩ ↦ a+b = c+d
  iseqv := {
    refl := by simp
    symm := by
      simp
      intro a b c d
      tauto
    trans := by
      simp
      intro a b c d e f
      intro h1 h2
      rw [h1, h2]
  }
}

private lemma psqev_exact (h : Quot.mk pair_sum_equiv ⟨a, b⟩ = Quot.mk pair_sum_equiv ⟨c, d⟩) : a+b = c+d :=
  (Equivalence.eqvGen_iff pair_sum_equiv.iseqv).mp (Quot.eqvGen_exact h)

open scoped Classical in
private lemma cauchyProd_partSum_eq (n : ℕ) :
    CauchyProdPartialSum s t n =
    ∑ ij in filter (fun ⟨i, j⟩ ↦ i+j ≤ n) ((range (n+1)) ×ˢ (range (n+1))), (s ij.fst) * (t ij.snd) := by
  simp_rw [CauchyProdPartialSum, cauchyProd_eq]
  rw [sum_partition pair_sum_equiv]
  apply sum_nbij (ι := ℕ) (κ := Quotient pair_sum_equiv) (fun k ↦ Quot.mk pair_sum_equiv ⟨k, 0⟩)
  . intro k kRange
    rw [mem_image]
    use ⟨k, 0⟩
    simp
    rw [mem_range] at kRange
    constructor
    . omega
    . rfl
  . intro x _ y _ quot_eq
    convert psqev_exact quot_eq
  . rintro ⟨l, m⟩
    simp
    intro x y _ _ xyle quot_eq
    refine' ⟨l+m, _, _⟩
    have : l+m = x+y := (psqev_exact quot_eq).symm
    . rw [this]
      omega
    . apply Quot.sound
      show (l+m) + 0 = l + m
      rw [add_zero]
  . intro k kRange
    rw [mem_range] at kRange
    apply sum_congr
    . ext ⟨x, y⟩
      simp
      constructor
      . rintro ⟨⟨_, ylt⟩, sum_eq⟩
        constructor
        . omega
        . apply Quot.sound
          rw [← sum_eq]
          simp [pair_sum_equiv]
      . rintro ⟨_, quot_eq⟩
        have sum_eq : x + y = k := by convert psqev_exact quot_eq
        exact ⟨by omega, sum_eq⟩
    . tauto

private lemma partialSums_prod_eq (n : ℕ) : PartialSumsProd s t n = ∑ ij in range (n+1) ×ˢ range (n+1), (s ij.fst) * (t ij.snd) := by
  rw [PartialSumsProd, sum_mul_sum]
  symm
  apply sum_finset_product' (range (n+1) ×ˢ range (n+1)) (range (n+1)) (fun _ ↦ range (n+1)) (f := fun x y ↦ s x * t y)
  intro
  simp only [mem_range, mem_product]


@[reducible]
private def mt_A (n : ℕ) : Finset (ℕ × ℕ) := filter (fun ⟨i, j⟩ ↦ i+j > n) (range (n+1) ×ˢ range (n+1))
@[reducible]
private def mt_B (n₀ n : ℕ) : Finset (ℕ × ℕ) := filter (fun ⟨i, j⟩ ↦ i > n₀ ∨ j > n₀) (range (n+1) ×ˢ range (n+1))

private lemma mt_A_subset_B {n₀ n : ℕ} (gt : n > 2*n₀) : mt_A n ⊆ mt_B n₀ n := by
  intro ⟨x, y⟩
  simp only [mem_filter, mem_product, mem_range]
  rintro ⟨lt, sum_gt⟩
  omega

private lemma PartialSumsProd_minus_CauchyProdPartialSum_eq (n : ℕ) :
    PartialSumsProd s t n - CauchyProdPartialSum s t n = ∑ ij in mt_A n, (s ij.fst) * (t ij.snd) := by
  rw [partialSums_prod_eq, cauchyProd_partSum_eq]
  apply sub_eq_of_eq_add
  symm
  convert sum_inter_add_sum_diff (range (n+1) ×ˢ range (n+1)) (mt_A n) _ using 2
  . refine' sum_congr _ (by tauto)
    symm
    rw [inter_eq_right]
    intro x xA
    simp only [mem_filter, mem_product, mem_range] at *
    tauto
  . refine' sum_congr _ (by tauto)
    ext x
    simp only [mem_filter, mem_product, mem_range, mem_sdiff]
    omega

private lemma norm_partialSumsProd_diff_eq {n₀ n : ℕ} (gte : n ≥ n₀) :
    ‖(PartialSumsProd (norm ∘ s) (norm ∘ t) n) - (PartialSumsProd (norm ∘ s) (norm ∘ t) n₀)‖ =
    ∑ ij in mt_B n₀ n, ‖(s ij.fst) * (t ij.snd)‖ := by
  apply (abs_eq _).mpr
  . left
    simp_rw [partialSums_prod_eq]
    apply sub_eq_of_eq_add
    symm
    convert sum_inter_add_sum_diff (range (n+1) ×ˢ range (n+1)) (mt_B n₀ n) _ using 2
    . refine' sum_congr _ (by simp [norm_mul])
      symm
      rw [inter_eq_right]
      intro x xB
      simp only [mem_filter, mem_product, mem_range] at *
      tauto
    . refine' sum_congr _ (by simp [norm_mul])
      ext ⟨x, y⟩
      simp only [mem_filter, mem_product, mem_range, mem_sdiff]
      omega
  . apply sum_nonneg
    intros
    apply norm_nonneg

theorem cauchyProd_converges_to [CompleteSpace F] {s t : ℕ → F}
    (s_abs_conv : AbsolutelyConvergent s) (t_abs_conv : AbsolutelyConvergent t) :
    SeriesConvergesTo (CauchyProd s t) ((∑ᵢ s) * (∑ᵢ t)) := by
  suffices Tendsto (fun n ↦ ‖(PartialSumsProd s t n) - (CauchyProdPartialSum s t n)‖) atTop (𝓝 0) by
    have ⟨x, s_convto⟩ := s_abs_conv.convergent
    have ⟨y, t_convto⟩ := t_abs_conv.convergent
    rw [s_convto.get_isum, t_convto.get_isum]
    apply tendsto_of_tendsto_of_dist (s_convto.mul t_convto)
    convert this using 1
    simp [dist_eq_norm]
  apply Metric.tendsto_atTop'.mpr
  intro ε ε0
  have ⟨x, s_abs_convto⟩ := s_abs_conv
  have ⟨y, t_abs_convto⟩ := t_abs_conv
  have ⟨n₀, cauchy⟩ := Metric.cauchySeq_iff.mp ((s_abs_convto.mul t_abs_convto).cauchySeq) ε ε0
  use 2*n₀+1
  intro n nge
  have ngt : n > 2*n₀ := by omega
  simp [dist_eq_norm, PartialSumsProd_minus_CauchyProdPartialSum_eq]
  calc
    ‖∑ ij in mt_A n, s ij.fst * t ij.snd‖ ≤ ∑ ij in mt_A n, ‖s ij.fst * t ij.snd‖ := norm_sum_le _ _
    _ ≤ ∑ ij in mt_B n₀ n, ‖s ij.fst * t ij.snd‖ := by
      apply sum_le_sum_of_subset_of_nonneg (mt_A_subset_B ngt)
      intros
      exact norm_nonneg _
    _ < ε := by
      rw [← norm_partialSumsProd_diff_eq _ _ (by omega)]
      exact cauchy n (by omega) n₀ (le_refl _)

theorem cauchyProd_get_isum [CompleteSpace F]
    (s_abs_conv : AbsolutelyConvergent s) (t_abs_conv : AbsolutelyConvergent t) :
    (∑ᵢ CauchyProd s t) = ((∑ᵢ s) * (∑ᵢ t)) := by
  have : SeriesConvergent (CauchyProd s t) := ⟨_, cauchyProd_converges_to s_abs_conv t_abs_conv⟩
  rw [isum, dif_pos this]
  apply tendsto_nhds_unique this.choose_spec (cauchyProd_converges_to s_abs_conv t_abs_conv)

theorem cauchyProd_absolutelyConvergent (s_abs_conv : AbsolutelyConvergent s) (t_abs_conv : AbsolutelyConvergent t) :
    AbsolutelyConvergent (CauchyProd s t) := by
  have dom_conv : SeriesConvergent (CauchyProd (norm ∘ s) (norm ∘ t)) := by
    have norm_norm_comp (f : ℕ → F) : norm ∘ norm ∘ f = norm ∘ f := by ext; simp
    have norm_s_abs_conv : AbsolutelyConvergent (norm ∘ s) := by
      unfold AbsolutelyConvergent
      simpa [norm_norm_comp s]
    have norm_t_abs_conv : AbsolutelyConvergent (norm ∘ t) := by
      unfold AbsolutelyConvergent
      simpa [norm_norm_comp t]
    exact ⟨_, cauchyProd_converges_to norm_s_abs_conv norm_t_abs_conv⟩
  apply absolutely_convergent_by_direct_comparison 0 dom_conv
  intros
  unfold CauchyProd
  convert norm_sum_le _ _
  simp [norm_mul]

end Multiplication

-- Weierstraß M-test
theorem series_tendstoUniformly_of_dominated [CompleteSpace F] (f : ℕ → E → F) (M : ℕ → ℝ)
    (nonneg : ∀ n, M n ≥ 0) (dominated : ∀ n p, ‖f n p‖ ≤ M n) (conv : SeriesConvergent M) :
    ∃ g, TendstoUniformly (fun n ↦ ∑ k in range (n+1), f k) g atTop := by
  have uniformCauchy : UniformCauchySeqOn (fun n ↦ ∑ k in range (n+1), f k) atTop Set.univ := by
    rw [Metric.uniformCauchySeqOn_iff]
    intro ε εpos
    obtain ⟨m, hm⟩ := conv
    obtain ⟨N, hN⟩ := Metric.cauchySeq_iff.mp hm.cauchySeq ε εpos
    use N
    intro m mN n nN x _
    specialize hN m mN n nN
    simp
    wlog nm : n ≤ m generalizing n m
    . rw [dist_comm] at hN ⊢
      specialize this n nN m mN hN
      apply this
      omega
    rw [dist_eq_norm, sum_range_sub_sum_range (by omega)] at hN ⊢
    apply lt_of_le_of_lt _ hN
    rw [Real.norm_eq_abs, abs_of_nonneg]
    . apply le_trans (norm_sum_le _ _)
      apply sum_le_sum
      intro k _
      exact dominated k x
    . apply sum_nonneg
      intro k _
      exact nonneg k

  have pw_conv : ∀ p, ∃ y, SeriesConvergesTo (fun n ↦ f n p) y := by
    intro p
    convert cauchySeq_tendsto_of_complete <| uniformCauchy.cauchySeq (Set.mem_univ p)
    simp

  choose! g g_conv using pw_conv
  use g
  rw [← tendstoUniformlyOn_univ]
  apply uniformCauchy.tendstoUniformlyOn_of_tendsto
  intro x _
  convert g_conv x
  simp

-- Normal convergence -> local uniform convergence + continuity

open BoundedContinuousFunction

@[reducible]
def NormallyConvergent [TopologicalSpace E] (s : ℕ → E →ᵇ F) : Prop := AbsolutelyConvergent (fun n ↦ ‖s n‖)

@[reducible]
def LocallyNormallyConvergent [TopologicalSpace E] (s : ℕ → E → F) : Prop :=
  ∀ x, ∃ U ∈ 𝓝 x, ∃ (s' : ℕ → U →ᵇ F), (∀ n, (s' n).toFun = U.restrict (s n)) ∧ NormallyConvergent s'

theorem series_tendstoLocallyUniformly_of_locallyNormallyConvergent [TopologicalSpace E] [CompleteSpace F]
    {f : ℕ → E → F} {g : E → F}
    (convto : ∀ z, SeriesConvergesTo (fun n ↦ f n z) (g z)) (norm_conv : LocallyNormallyConvergent f) :
    ∀ x, ∃ U ∈ 𝓝 x, TendstoUniformlyOn (fun n ↦ ∑ k in range (n+1), f k) g atTop U := by
  intro x
  obtain ⟨U, Unbhd, ⟨s, seq, snorm⟩⟩ := norm_conv x
  refine' ⟨U, Unbhd, _⟩
  let M := fun n ↦ ‖s n‖
  obtain ⟨g', hg'⟩ := series_tendstoUniformly_of_dominated
    (fun n ↦ (s n).toFun) M (fun _ ↦ norm_nonneg _)
    (fun n ↦ (norm_le (norm_nonneg _)).mp (le_refl _))
    snorm.convergent
  rw [tendstoUniformlyOn_iff_tendstoUniformly_comp_coe]
  convert hg' with i x
  . simp; congr
    conv =>
      rhs
      ext j
      change (s j).toFun x
      rw [seq]
    simp
  . ext z
    specialize convto z
    have h := hg'.tendsto_at z
    have : ∀ n, s n = U.restrict (f n) := by intros; convert seq _
    simp [this, seq] at h
    exact tendsto_nhds_unique convto h

theorem series_continuous_of_terms_continuous_and_locallyNormallyConvergent
    [TopologicalSpace E] [CompleteSpace F] {f : ℕ → E → F} {g : E → F}
    (convto : ∀ z, SeriesConvergesTo (fun n ↦ f n z) (g z)) (norm_conv : LocallyNormallyConvergent f)
    (cont : ∀ n, Continuous (f n)):
    Continuous g := by
  choose! U hU using series_tendstoLocallyUniformly_of_locallyNormallyConvergent convto norm_conv
  apply continuous_of_cover_nhds (s := U)
  . intro x
    use x
    exact (hU x).left
  . intro x
    apply (hU x).right.continuousOn
    apply Eventually.of_forall
    intro n
    induction' n with n' ih
    . simp
      exact (cont 0).continuousOn
    . rw [sum_range_succ]
      apply ContinuousOn.add ih
      exact (cont (n' + 1)).continuousOn

open Classical in
theorem tendsto_compactly_of_tendstoLocallyUniformly [MetricSpace E] {f : ℕ → E → F} {g : E → F}
    (h : ∀x, ∃U ∈ 𝓝 x, TendstoUniformlyOn f g atTop U) :
    ∀K, IsCompact K → TendstoUniformlyOn f g atTop K := by
  intro K Kcompact
  choose! U Uin ttU using h
  choose! V VU Vopen xV using fun (x : E) ↦ mem_nhds_iff.mp (Uin x)
  have Vcover : K ⊆ ⋃ x, V x := by
    intro x _
    simp
    exact ⟨x, xV x⟩
  have Vuniform : ∀ x, TendstoUniformlyOn f g atTop (V x) :=
    fun x ↦ TendstoUniformlyOn.mono (ttU x) (VU x)
  rw [isCompact_iff_finite_subcover] at Kcompact
  obtain ⟨S, Scover⟩ := Kcompact V Vopen Vcover
  simp_rw [Metric.tendstoUniformlyOn_iff, eventually_atTop] at Vuniform
  choose! N h using Vuniform
  rw [Metric.tendstoUniformlyOn_iff]
  intro ε εpos
  rw [eventually_atTop]
  by_cases nempty : S.Nonempty; rotate_left
  . simp at nempty
    simp [nempty] at Scover
    rw [Scover]
    simp
  obtain ⟨Nmax, Nmaxin, Nmaxge⟩ := exists_max_image S (flip N ε) nempty
  use N Nmax ε
  intro n nge x xK
  specialize Scover xK; simp at Scover
  obtain ⟨y, yS, xVy⟩ := Scover
  apply h y ε εpos n
  . exact le_trans (Nmaxge y yS) nge
  . exact xVy

theorem tendstoLocallyUniformly_of_tendsto_compactly [MetricSpace E] [WeaklyLocallyCompactSpace E]
    {f : ℕ → E → F} {g : E → F} (h : ∀K, IsCompact K → TendstoUniformlyOn f g atTop K) :
    ∀x, ∃U ∈ 𝓝 x, TendstoUniformlyOn f g atTop U := by
  intro x
  obtain ⟨K, Kcompact, Knhds⟩ := exists_compact_mem_nhds x
  exact ⟨K, Knhds, h K Kcompact⟩
