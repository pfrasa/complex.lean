import Mathlib.Analysis.SpecialFunctions.Pow.Real
import General.Algebra
import General.Series
import General.Differentiation

namespace General

variable [NontriviallyNormedField F]

structure PowerSeries (F : Type*) where
  coeff : ℕ → F
  center : F

@[reducible]
def PowerSeries.series_at (ps : PowerSeries F) (z : F) : ℕ → F := fun n ↦ ps.coeff n * (z-ps.center)^n

@[simp]
theorem PowerSeries.series_at_zero_eq (ps : PowerSeries F) (at_zero : ps.center = 0) :
    ∀ z, ps.series_at z = fun n ↦ ps.coeff n * z^n := by
  intro z
  ext n
  simp [at_zero]

noncomputable def PowerSeries.isum_at (ps : PowerSeries F) (z : F) := ∑ᵢ ps.series_at z

namespace PowerSeries

open General.Series General.Algebra Finset Filter Topology Real

-- Abel's Lemma

lemma powerSeries_dominated_by_abels_lemma (ps : PowerSeries F) {ρ r M : ℝ} {z : F} (rpos : r > 0) (ρpos : ρ > 0)
    (le_M : ∀ n, ‖ps.coeff n‖*ρ^n ≤ M) (z_le : dist z ps.center ≤ r) :
    ∀ n, ‖ps.series_at z n‖ ≤ M*(r/ρ)^n := by
  intro n
  unfold PowerSeries.series_at
  rw [norm_mul, norm_pow]
  calc
    ‖ps.coeff n‖*‖z - ps.center‖^n ≤ ‖ps.coeff n‖*r^n :=
      mul_le_mul_of_nonneg_left (pow_le_pow_left₀ (norm_nonneg _) (dist_eq_norm z _ ▸ z_le) n) (norm_nonneg _)
    _ = (‖ps.coeff n‖*ρ^n)*(r/ρ)^n := by
      field_simp
      ring
    _ ≤ M*(r/ρ)^n := by
      refine' mul_le_mul_of_nonneg_right (le_M n) _
      apply pow_nonneg
      apply div_nonneg
      repeat linarith

private lemma powerSeries_dominating_by_abels_lemma_converges_to {ρ r M : ℝ} (rpos : r > 0) (ρpos : ρ > 0) (r_lt :r < ρ) :
    SeriesConvergesTo (fun n ↦ M*(r/ρ)^n) (M/(1-(r/ρ))):= by
  rw [← mul_one_div M]
  apply SeriesConvergesTo.const_mul
  apply geometric_series_converges_to
  rw [Real.norm_eq_abs, abs_div, abs_of_pos rpos, abs_of_pos ρpos, div_lt_one ρpos]
  exact r_lt

lemma powerSeries_absolutelyConvergent_by_abels_lemma' (ps : PowerSeries F) {ρ r M : ℝ} {z : F} (rpos : r > 0) (ρpos : ρ > 0) (r_lt : r < ρ)
    (le_M : ∀ n, ‖ps.coeff n‖*ρ^n ≤ M) (z_le : dist z ps.center ≤ r) :
    AbsolutelyConvergent (ps.series_at z) := by
  apply absolutely_convergent_by_direct_comparison 0 ⟨_, powerSeries_dominating_by_abels_lemma_converges_to rpos ρpos r_lt⟩
  intro n _
  exact powerSeries_dominated_by_abels_lemma ps rpos ρpos le_M z_le n

lemma powerSeries_absolutelyConvergent_by_abels_lemma (ps : PowerSeries F) {ρ M : ℝ} {z : F} (ρpos : ρ > 0)
    (le_M : ∀ n, ‖ps.coeff n‖*ρ^n ≤ M) (z_le : dist z ps.center < ρ) :
    AbsolutelyConvergent (ps.series_at z) := by
  obtain ⟨r, ⟨z_le', r_lt⟩⟩ := exists_between z_le
  refine' powerSeries_absolutelyConvergent_by_abels_lemma' ps _ ρpos r_lt le_M (le_of_lt z_le')
  linarith [dist_eq_norm z ps.center, norm_nonneg (z - ps.center)]

lemma powerSeries_le_by_abels_lemma (ps : PowerSeries F) {ρ r M : ℝ} {z : F} (rpos : r > 0) (ρpos : ρ > 0) (r_lt : r < ρ)
    (le_M : ∀ n, ‖ps.coeff n‖*ρ^n ≤ M) (z_le : dist z ps.center ≤ r) :
    ∀ n, (∑ᵢfrom n, norm ∘ (ps.series_at z)) ≤ M*(r/ρ)^n*(1/(1-(r/ρ))) := by
  intro n
  have convto := powerSeries_dominating_by_abels_lemma_converges_to rpos ρpos r_lt (M := M)
  convert norm_le_by_direct_comparison (F := F) 0 ⟨_, convto⟩ _ n (Nat.zero_le _)
  . simp_rw [isum_from, pow_add, ← mul_assoc, mul_comm M, mul_assoc, isum_const_mul_of_series_convergent ⟨_, convto⟩, convto.get_isum, mul_one_div]
  . intro n _
    apply powerSeries_dominated_by_abels_lemma (F := F) ps rpos ρpos le_M z_le

-- Radius of convergence

lemma powerSeries_absolutelyConvergent_of_dist_le_of_converges {ps : PowerSeries F} {z z₁ : F}
    (conv : SeriesConvergent (ps.series_at z₁)) (dist_le : dist z ps.center < dist z₁ ps.center) :
    AbsolutelyConvergent (ps.series_at z) := by
  obtain ⟨M, bounded⟩ : ∃ M, ∀ n, ‖ps.coeff n‖*(dist z₁ ps.center)^n ≤ M := by
    have tt : Tendsto (fun n ↦ ‖ps.coeff n‖*‖z₁ - ps.center‖^n) atTop (𝓝 0) := by
      simp_rw [← norm_pow, ← norm_mul]
      apply tendsto_zero_iff_norm_tendsto_zero.mp
      exact termseq_tendsto_zero_of_series_convergent conv
    apply Tendsto.bddAbove_range at tt
    rw [bddAbove_def] at tt
    obtain ⟨M, b⟩ := tt
    use M
    intro n
    apply b
    simp [Set.mem_range, dist_eq_norm]
  refine' powerSeries_absolutelyConvergent_by_abels_lemma ps _ bounded dist_le
  linarith [dist_eq_norm z ps.center, norm_nonneg (z - ps.center)]

open ENNReal

noncomputable def RadiusOfConvergence (ps : PowerSeries F) : ℝ≥0∞ :=
  sSup <| ENNReal.ofReal <$> { r : ℝ | ∃ z, dist z ps.center = r ∧ SeriesConvergent (ps.series_at z) }

def DiskOfConvergence (ps : PowerSeries F) : Set F :=
  if RadiusOfConvergence ps = ∞ then Set.univ else
  if RadiusOfConvergence ps = 0 then {ps.center} else
  Metric.ball ps.center (RadiusOfConvergence ps).toReal

theorem center_mem_disk {ps : PowerSeries F} : ps.center ∈ DiskOfConvergence ps := by
  rw [DiskOfConvergence]
  by_cases rad_inf : RadiusOfConvergence ps = ∞
  . rw [if_pos rad_inf]
    simp
  . by_cases rad_zero : RadiusOfConvergence ps = 0
    . rw [if_neg rad_inf, if_pos rad_zero]
      simp
    . rw [if_neg rad_inf, if_neg rad_zero]
      simp
      exact toReal_pos rad_zero rad_inf

theorem disk_mem_nhds {ps : PowerSeries F} (rad_pos : RadiusOfConvergence ps ≠ 0) (xdisk : x ∈ DiskOfConvergence ps) :
    DiskOfConvergence ps ∈ 𝓝 x := by
  rw [DiskOfConvergence] at xdisk ⊢
  by_cases rad_inf : RadiusOfConvergence ps = ∞
  . rw [if_pos rad_inf]
    exact univ_mem
  . rw [if_neg rad_inf, if_neg rad_pos] at xdisk ⊢
    rw [← mem_interior_iff_mem_nhds]
    convert xdisk
    rw [interior_eq_iff_isOpen]
    apply Metric.isOpen_ball

theorem mem_disk_of_lt_radius {ps : PowerSeries F} (dist_lt : ENNReal.ofReal (dist z ps.center) < RadiusOfConvergence ps) :
    z ∈ DiskOfConvergence ps := by
  rw [DiskOfConvergence]
  by_cases rad_inf : RadiusOfConvergence ps = ∞
  . rw [if_pos rad_inf]
    simp
  . by_cases rad_zero : RadiusOfConvergence ps = 0
    . simp [rad_zero] at dist_lt
    . rw [if_neg rad_inf, if_neg rad_zero]
      rw [Metric.mem_ball]
      exact (ofReal_lt_iff_lt_toReal dist_nonneg rad_inf).mp dist_lt

theorem lt_radius_of_mem_disk {ps : PowerSeries F} (xdisk : x ∈ DiskOfConvergence ps) (rad_pos : RadiusOfConvergence ps ≠ 0) :
    ENNReal.ofReal (dist x ps.center) < RadiusOfConvergence ps := by
  by_cases rad_inf : RadiusOfConvergence ps = ∞
  . rw [rad_inf]
    exact ofReal_lt_top
  . rw [DiskOfConvergence, if_neg rad_inf, if_neg rad_pos, Metric.mem_ball] at xdisk
    exact (ofReal_lt_iff_lt_toReal dist_nonneg rad_inf).mpr xdisk

@[reducible]
def series_on_disk (ps : PowerSeries F) := flip <| (DiskOfConvergence ps).restrict ps.series_at

theorem exists_dist_gt_of_mem_disk [NormedSpace ℝ F] (ps : PowerSeries F)
    (xdisk : x ∈ DiskOfConvergence ps) (rad_pos : RadiusOfConvergence ps ≠ 0) :
    ∃ y ∈ DiskOfConvergence ps, dist ps.center x < dist ps.center y := by
  by_cases rad_inf : RadiusOfConvergence ps = ∞
  . refine' ⟨ps.center + ((dist ps.center x)+1)•1, _, _⟩
    . rw [DiskOfConvergence, if_pos rad_inf]
      simp
    . nth_rw 2 [dist_eq_norm]
      simp [norm_smul]
      rw [abs_of_nonneg]
      linarith
      linarith [dist_nonneg (x := ps.center) (y := x)]
  . simp_rw [DiskOfConvergence, if_neg rad_inf, if_neg rad_pos, Metric.mem_ball] at xdisk ⊢
    obtain ⟨r, r_gt, r_lt⟩ := exists_between xdisk
    have r_eq_abs : |r| = r := by
      rw [abs_of_nonneg]
      linarith [dist_nonneg (x := x) (y := ps.center)]
    refine' ⟨ps.center + r•1, _, _⟩
    . simpa [dist_eq_norm, norm_smul, r_eq_abs]
    . nth_rw 2 [dist_eq_norm]
      simp [norm_smul]
      rwa [r_eq_abs, dist_comm]

theorem powerSeries_absolutelyConvergent_of_lt_radius {z : F} (dist_lt : ENNReal.ofReal (dist z ps.center) < RadiusOfConvergence ps) :
    AbsolutelyConvergent (ps.series_at z) := by
  obtain ⟨_, ⟨⟨r', ⟨⟨z₁, ⟨rfl, conv⟩⟩, rfl⟩⟩, dist_lt'⟩⟩ := lt_sSup_iff.mp dist_lt
  apply powerSeries_absolutelyConvergent_of_dist_le_of_converges conv
  apply edist_lt_ofReal.mp
  convert dist_lt'
  exact edist_dist z ps.center

theorem powerSeries_comp_converges_to_at_center [NormedField G] (ps : PowerSeries F) (f : F → G) (fz : f 0 = 0):
    SeriesConvergesTo (f ∘ ps.series_at ps.center) (f (ps.coeff 0)) := by
  unfold series_at
  simp
  apply tendsto_atTop_of_eventually_const (i₀ := 0)
  intro n _
  induction' n with n' ih
  . simp
  . have ih := ih (Nat.zero_le _)
    rw [Nat.succ_add, sum_range_succ]
    simpa [fz]

theorem powerSeries_converges_to_at_center (ps : PowerSeries F) : SeriesConvergesTo (ps.series_at ps.center) (ps.coeff 0) :=
  powerSeries_comp_converges_to_at_center ps id (by simp)

theorem powerSeries_norm_converges_to_at_center (ps : PowerSeries F) : SeriesConvergesTo (norm ∘ ps.series_at ps.center) ‖ps.coeff 0‖ :=
  powerSeries_comp_converges_to_at_center ps norm (by simp)

theorem powerSeries_absolutelyConvergent_of_radius_infty (rad_eq : RadiusOfConvergence ps = ∞) (z : F) : AbsolutelyConvergent (ps.series_at z) := by
  apply powerSeries_absolutelyConvergent_of_lt_radius
  rw [rad_eq]
  exact ofReal_lt_top

theorem powerSeries_radius_infty_of_everywhere_convergent [NormedSpace ℝ F] {ps : PowerSeries F}
    (conv : ∀ z, SeriesConvergent (ps.series_at z)) :
    RadiusOfConvergence ps = ∞ := by
  apply eq_top_of_forall_nnreal_le
  intro r
  refine' le_sSup_of_le (b := r) _ (le_refl _)
  use r
  constructor
  . use r.toReal•1 + ps.center
    refine' ⟨_, conv _⟩
    simp [norm_smul]
  . exact ofReal_coe_nnreal

theorem powerSeries_divergent_of_gt_radius {z : F} (dist_gt : ENNReal.ofReal (dist z ps.center) > RadiusOfConvergence ps) :
    SeriesDivergent (ps.series_at z) := by
  intro conv
  set d := ENNReal.ofReal (dist z ps.center)
  suffices d ≤ RadiusOfConvergence ps from (not_le_of_gt dist_gt) this
  refine' le_sSup_of_le (b := d) _ (le_refl _)
  use d.toReal
  simp
  refine' ⟨⟨z, ⟨_, conv⟩⟩, ofReal_ne_top⟩
  rw [toReal_ofReal (dist_nonneg)]

theorem powerSeries_absolutelyConvergent_of_mem_disk {z : F} (mem_disk : z ∈ DiskOfConvergence ps) : AbsolutelyConvergent (ps.series_at z) := by
  by_cases rad_inf : RadiusOfConvergence ps = ∞
  . exact powerSeries_absolutelyConvergent_of_radius_infty rad_inf z
  . rw [DiskOfConvergence, if_neg rad_inf] at mem_disk
    by_cases rad_zero : RadiusOfConvergence ps = 0
    . rw [if_pos rad_zero] at mem_disk
      rw [mem_disk]
      use ‖ps.coeff 0‖
      exact powerSeries_norm_converges_to_at_center ps
    . rw [if_neg rad_zero, Metric.mem_ball] at mem_disk
      apply powerSeries_absolutelyConvergent_of_lt_radius
      convert mem_disk
      exact ofReal_lt_iff_lt_toReal dist_nonneg rad_inf

-- Cauchy-Hadamard formula

private lemma eventually_bounded_of_limsup_bounded {f : ℕ → ℝ} (finite_bound : r ≠ ∞) (limsup_lt : limsup (ENNReal.ofReal ∘ f) atTop < r) :
    ∃ n₀, ∀ n ≥ n₀, f n ≤ r.toReal := by
  simp_rw [limsup_eq, eventually_atTop, sInf_lt_iff] at limsup_lt
  obtain ⟨max_end, ⟨⟨n₀, bounded⟩, max_end_lt⟩⟩ := limsup_lt
  use n₀
  intro n nge
  apply le_of_lt
  apply lt_of_le_of_lt
  . apply (ofReal_le_iff_le_toReal _).mp (bounded n nge)
    exact ne_top_of_lt max_end_lt
  . exact toReal_strict_mono finite_bound max_end_lt

private lemma frequently_lt_of_lt_limsup' {f : ℕ → ℝ≥0∞} (lt_limsup : r < limsup f atTop) (fnn : ∀ n, f n ≥ 0):
    ∀ n₀, ∃ n ≥ n₀, r < f n := by
  apply frequently_atTop.mp
  apply frequently_lt_of_lt_limsup _ lt_limsup
  unfold IsCoboundedUnder
  unfold IsCobounded
  simp
  use 0
  intro a b h
  have := h b (le_refl _)
  exact le_trans (fnn _) this

private lemma bounded_of_eventually_bounded {f : ℕ → ℝ} (ev_bounded : ∀ n ≥ n₀, f n ≤ K) : ∃ M, ∀ n, f n ≤ M := by
  let N := n₀+1
  let max_init := Finset.max' (image f <| range N) (by
    apply Nonempty.image
    apply nonempty_range_iff.mpr
    omega
  )
  use max max_init K
  intro n
  by_cases gt_N : n ≥ N
  . exact le_trans (ev_bounded n (by omega)) (le_max_right _ _)
  . refine' le_trans _ (le_max_left _ _)
    apply le_max'
    simp
    use n
    simp
    omega

private lemma nroot_rpow {x : ℝ} {n : ℕ} (nnz : n ≠ 0) (xnneg : x ≥ 0) : x = (x^(1/(n : ℝ)))^(n : ℝ) := by
  rw [← rpow_mul xnneg, one_div_mul_cancel _, Real.rpow_one]
  norm_cast

private lemma rpow_nroot {x : ℝ} {n : ℕ} (nnz : n ≠ 0) (xnneg : x ≥ 0) : x = (x^(n : ℝ))^(1/(n : ℝ)) := by
  rw [← rpow_mul xnneg, mul_comm, one_div_mul_cancel _, Real.rpow_one]
  norm_cast

private lemma le_pow_of_nroot_le {x y : ℝ} {n : ℕ} (nnz : n ≠ 0) (xnneg : x ≥ 0) (h : rpow x (1/n) ≤ y) : x ≤ y^n := by
  rw [nroot_rpow nnz xnneg]
  convert rpow_le_rpow (rpow_nonneg xnneg _) h _
  norm_cast
  norm_cast
  exact Nat.zero_le n

private lemma nroot_le_of_le_pow {x y : ℝ} {n : ℕ} (nnz : n ≠ 0) (xnneg : x ≥ 0) (ynneg : y ≥ 0) (h : x ≤ y^n) : rpow x (1/n) ≤ y := by
  convert rpow_le_rpow xnneg h _
  . convert rpow_nroot nnz ynneg
    norm_cast
  . simp

private lemma hadamard_bound_of_nroot_le {f : ℕ → F} (ρnneg : ρ ≥ 0) (nroot_le : ∀ n ≥ n₀, rpow ‖f n‖ (1/n) ≤ 1/ρ) :
    ∀ n ≥ n₀, ‖f n‖ * ρ^n ≤ max 1 ‖f 0‖ := by
   intro n ngt
   cases' n with n'
   . simp
   . apply le_trans _ (le_max_left _ _)
     apply mul_le_of_le_div₀ (by norm_num) (pow_nonneg ρnneg _)
     nth_rw 2 [← one_pow n'.succ]
     rw [← div_pow]
     exact le_pow_of_nroot_le (by omega) (norm_nonneg _) (nroot_le n'.succ ngt)

private lemma ENNReal.le_one_div {x y : ℝ≥0∞} (h : y < 1/x) (h' : y ≠ 0) (h'' : y ≠ ∞) : x < 1/y :=
  (ENNReal.lt_div_iff_mul_lt (Or.inl h') (Or.inl h'')).mpr (mul_lt_of_lt_div' h)

lemma radiusOfConvergence_ge_limsup [NormedSpace ℝ F] [CompleteSpace F] {ps : PowerSeries F} :
    RadiusOfConvergence ps ≥ 1 / limsup (fun n ↦ ENNReal.ofReal (rpow ‖ps.coeff n‖ (1/n))) atTop := by
  set R := 1 / limsup (fun n ↦ ENNReal.ofReal (rpow ‖ps.coeff n‖ (1/n))) atTop

  by_cases R_gtz : R > 0
  . apply le_of_forall_ge_of_dense
    intro r r_lt
    obtain ⟨s, ⟨s_gt, s_lt⟩⟩ := exists_between r_lt
    let r' := r.toReal
    let s' := s.toReal
    have s_gt' : r' < s' := toReal_strict_mono (ne_top_of_lt s_lt) s_gt
    have s_gtz' : s' > 0 := toReal_pos (ne_zero_of_lt s_gt) (ne_top_of_lt s_lt)

    refine' le_sSup_of_le _ (le_refl _)
    use r'
    constructor
    . use r'•1 + ps.center
      have dist_eq : dist (r'•1 + ps.center) ps.center = r' := by
        simp [norm_smul]
        exact toReal_nonneg
      refine' ⟨dist_eq, _⟩
      apply AbsolutelyConvergent.convergent
      have limsup_bounded := ENNReal.le_one_div s_lt (ne_bot_of_gt s_gt) (ne_top_of_lt s_lt)
      have finite_bound : 1/s ≠ ∞ := by
        intro eq_top
        rw [div_eq_top] at eq_top
        rcases eq_top with ⟨_, s_bot⟩ | ⟨one_top, _⟩
        . exact ne_bot_of_gt s_gt s_bot
        . exact one_ne_top one_top
      obtain ⟨M, le_M⟩ := eventually_bounded_of_limsup_bounded finite_bound limsup_bounded
      have : (1/s).toReal = 1 / s' := by
        suffices (1/s).toReal = ENNReal.toReal 1 / s' by
          rw [this]
          simp
        exact toReal_div 1 s
      rw [this] at le_M
      obtain ⟨M', le_M'⟩ := bounded_of_eventually_bounded <| hadamard_bound_of_nroot_le (le_of_lt s_gtz') le_M (F := F)
      apply powerSeries_absolutelyConvergent_by_abels_lemma _ s_gtz' le_M'
      rw [dist_eq]
      exact s_gt'
    . apply ofReal_toReal
      exact ne_top_of_lt r_lt
  . have R_z : R = 0 := by
      apply le_zero_iff.mp
      push_neg at R_gtz
      exact R_gtz
    rw [R_z]
    apply zero_le

private lemma divergent_below {x y z : ℝ≥0∞} (y_gt : y > z) (x_ne_top : x ≠ ⊤) (y_lt_x : y < x) :
    Tendsto (fun n ↦ (x.toReal/y.toReal)^n) atTop atTop := by
  apply tendsto_pow_atTop_atTop_of_one_lt
  have ygtz : y.toReal > 0 := toReal_pos (ne_bot_of_gt y_gt) (ne_top_of_lt y_lt_x)
  apply (one_lt_div ygtz).mpr

  exact (toReal_lt_toReal (ne_top_of_lt y_lt_x) x_ne_top).mpr y_lt_x

lemma radiusOfConvergence_le_limsup {ps : PowerSeries F} :
    RadiusOfConvergence ps ≤ 1 / limsup (fun n ↦ ENNReal.ofReal (rpow ‖ps.coeff n‖ (1/n))) atTop := by
  set f := fun n ↦ ENNReal.ofReal (rpow ‖ps.coeff n‖ (1/n))
  set L := limsup f atTop
  set R := 1 / L

  apply le_of_forall_le_of_dense
  intro r r_gt
  apply sSup_le
  simp
  intro s s' z dist_eq w convto ss'
  have snetop : s ≠ ⊤ := by
    rw [← ss']
    exact ofReal_ne_top
  have s's : s' = s.toReal := by
    rw [← ss', toReal_ofReal]
    simp [← dist_eq, dist_nonneg]

  have conv : SeriesConvergent _ := ⟨_, convto⟩
  contrapose! conv
  have spos : s > 0 := by
    rw [← ENNReal.bot_eq_zero]
    apply bot_lt_iff_ne_bot.mpr
    exact ne_bot_of_gt conv
  have s'pos : 0 < s' := by
    convert (toReal_lt_toReal zero_ne_top snetop).mpr spos

  apply series_divergent_of_termseq_not_tendsto_zero
  apply not_tendsto_iff_exists_frequently_nmem.mpr
  use Metric.ball 0 1
  constructor
  . apply Metric.ball_mem_nhds
    norm_num
  apply frequently_atTop.mpr
  intro n

  obtain ⟨n₀, ge1⟩ := eventually_atTop.mp <| tendsto_atTop.mp (divergent_below r_gt snetop conv) 1

  have lt_limsup : 1/r < L := by
    apply (ENNReal.div_lt_iff (Or.inr one_ne_zero) (Or.inr one_ne_top)).mpr
    rw [mul_comm]
    exact (ENNReal.div_lt_iff (Or.inr one_ne_zero) (Or.inr one_ne_top)).mp r_gt
  obtain ⟨N, ⟨Ngt, f_gt⟩⟩ := frequently_lt_of_lt_limsup' lt_limsup (by intros; simp) (max n (max n₀ 1))
  have Ngz : N > 0 := by omega
  have Nnz : N ≠ 0 := (ne_of_lt Ngz).symm

  refine' ⟨N, by omega, _⟩
  apply mem_ball_iff_norm.mp.mt
  apply not_lt_of_ge

  have nroot_gt : 1/r.toReal < rpow ‖ps.coeff N‖ (1/N) := by
    convert (toReal_lt_toReal (ne_top_of_lt f_gt) _).mpr f_gt
    . rw [toReal_div]
      simp
    . rw [toReal_ofReal]
      apply rpow_nonneg (norm_nonneg _)
    . apply ofReal_ne_top

  simp
  apply le_of_lt
  apply lt_of_le_of_lt (ge1 N (by omega))
  rw [div_pow, ← dist_eq_norm, dist_eq, ← mul_one_div, mul_comm, ← one_pow N, ← div_pow, ← s's]
  apply (_root_.mul_lt_mul_right (pow_pos s'pos N)).mpr
  have nroot_pow : ‖ps.coeff N‖ = (‖ps.coeff N‖^(1/(N : ℝ)))^(N : ℝ) := by
    rw [← rpow_mul (norm_nonneg _), one_div_mul_cancel (by norm_cast), Real.rpow_one]
  rw [nroot_pow]
  convert rpow_lt_rpow _ nroot_gt _
  . norm_cast
  . simp
  . norm_cast

theorem radiusOfConvergence_eq_limsup [NormedSpace ℝ F] [CompleteSpace F] {ps : PowerSeries F} :
    RadiusOfConvergence ps = 1 / limsup (fun n ↦ ENNReal.ofReal (rpow ‖ps.coeff n‖ (1/n))) atTop :=
  le_antisymm radiusOfConvergence_le_limsup radiusOfConvergence_ge_limsup

-- Normal convergence and continuity of power series

variable [NormedSpace ℝ F]

open BoundedContinuousFunction in
theorem powerSeries_locally_normally_convergent (ps : PowerSeries F) (rad_pos : RadiusOfConvergence ps ≠ 0) :
    LocallyNormallyConvergent ps.series_on_disk := by
  intro x
  obtain ⟨y, yDisk, ygt⟩ := exists_dist_gt_of_mem_disk ps (Subtype.coe_prop x) rad_pos
  let center' : DiskOfConvergence ps := ⟨ps.center, center_mem_disk⟩
  let ball := Metric.ball center' (dist ps.center y)
  refine' ⟨ball, _, _⟩
  . rw [mem_nhds_iff]
    refine' ⟨ball, subset_refl _, Metric.isOpen_ball, _⟩
    rwa [Metric.mem_ball, dist_comm]
  . have bounded : ∀ n, ∀ z ∈ ball, ‖ps.series_at z n‖ ≤ ‖ps.series_at y n‖ := by
      intro n z zball
      simp
      gcongr
      rw [Metric.mem_ball, dist_comm _ y, dist_eq_norm] at zball
      rw [← dist_eq_norm]
      apply le_of_lt
      exact zball
    use fun n ↦ {
      toFun := ball.restrict <| ps.series_on_disk n
      continuous_toFun := by
        apply ContinuousOn.restrict
        apply Continuous.continuousOn
        rw [series_on_disk]
        have : flip (Set.restrict (DiskOfConvergence ps) (series_at ps)) n = fun (z : DiskOfConvergence ps) ↦ (ps.coeff n) * (z - ps.center)^n := rfl
        continuity
      map_bounded' := by
        use 2*‖ps.series_at y n‖
        intro z w
        dsimp
        rw [dist_eq_norm]
        calc
          ‖ps.series_on_disk n z - ps.series_on_disk n w‖ ≤ ‖ps.series_on_disk n z‖ + ‖ps.series_on_disk n w‖ := norm_sub_le _ _
          _ ≤ ‖ps.series_at y n‖ + ‖ps.series_at y n‖ :=
            add_le_add (bounded _ _ (Subtype.coe_prop _)) (bounded _ _ (Subtype.coe_prop _))
          _ = 2*‖ps.series_at y n‖ := (two_mul _).symm
    }
    constructor
    . intro n
      rfl
    . apply absolutely_convergent_by_direct_comparison 0 <| powerSeries_absolutelyConvergent_of_mem_disk yDisk
      intro n _
      rw [norm_norm]
      dsimp
      rw [norm_le (norm_nonneg _)]
      intro z
      exact bounded n z (Subtype.coe_prop z)

theorem powerSeries_compactly_convergent [CompleteSpace F] (ps : PowerSeries F) (rad_pos : RadiusOfConvergence ps ≠ 0) :
    ∀K, IsCompact K → TendstoUniformlyOn
      (fun n x ↦ ∑ i in range (n+1), ps.series_on_disk i x)
      (fun x ↦ ∑ᵢ ps.series_at x) atTop K := by
  apply tendsto_compactly_of_tendstoLocallyUniformly
  have normConv := powerSeries_locally_normally_convergent ps rad_pos
  convert series_tendstoLocallyUniformly_of_locallyNormallyConvergent _ normConv
  . simp
  . intro z
    obtain ⟨f, convto⟩ := (powerSeries_absolutelyConvergent_of_mem_disk (Subtype.coe_prop z)).convergent
    rw [convto.get_isum]
    exact convto

variable [CompleteSpace F]

theorem powerSeries_continuousOn_disk {ps : PowerSeries F} (rad_pos : RadiusOfConvergence ps ≠ 0) :
    ContinuousOn ps.isum_at (DiskOfConvergence ps) := by
  rw [continuousOn_iff_continuous_restrict]
  refine' series_continuous_of_terms_continuous_and_locallyNormallyConvergent _ (powerSeries_locally_normally_convergent ps rad_pos) _
  . intro z
    show SeriesConvergesTo (ps.series_at z) (ps.isum_at z)
    obtain ⟨y, yconv⟩ := (powerSeries_absolutelyConvergent_of_mem_disk (Subtype.coe_prop z)).convergent
    rwa [isum_at, yconv.get_isum]
  . intro n
    rw [series_on_disk]
    have : flip (Set.restrict (DiskOfConvergence ps) (series_at ps)) n = fun (z : DiskOfConvergence ps) ↦ (ps.coeff n) * (z - ps.center)^n := rfl
    continuity

theorem powerSeries_continuousAt_of_mem_disk {ps : PowerSeries F} (rad_pos : RadiusOfConvergence ps ≠ 0) (mem_disk : x ∈ DiskOfConvergence ps) :
    ContinuousAt ps.isum_at x :=
  (powerSeries_continuousOn_disk rad_pos).continuousAt <| disk_mem_nhds rad_pos mem_disk

theorem powerSeries_continuous_of_rad_inf {ps : PowerSeries F} (rad_inf : RadiusOfConvergence ps = ∞) : Continuous ps.isum_at := by
  rw [continuous_iff_continuousOn_univ]
  convert powerSeries_continuousOn_disk (by rw [rad_inf]; exact top_ne_bot)
  rw [DiskOfConvergence, if_pos rad_inf]

-- Differentiation of power series

def formalDeriv (ps : PowerSeries F) : PowerSeries F := PowerSeries.mk (fun n ↦ (n+1)*(ps.coeff (n+1))) ps.center

private lemma nqn_tendsto_zero {q : ℝ} (qpos : q > 0) (qlt1 : q < 1) : Tendsto (fun (n : ℕ) ↦ n*q^n) atTop (𝓝 0) := by
  apply Metric.tendsto_atTop.mpr
  intro ε εpos
  let x := 1/q - 1
  have xpos : x > 0 := by
    apply sub_pos_of_lt
    exact one_lt_one_div qpos qlt1
  use max 2 (1+⌈(1+(2/(ε*x^2)))⌉₊)
  intro n ngt
  simp [abs_of_pos qpos]
  have : n*q^n = n/(1+x)^n := by
    unfold x
    simp
  rw [this]
  have : (1+x)^n > (Nat.choose n 2)*x^2 := by
    rw [binomial_theorem]
    convert Finset.single_lt_sum (i := 2) (j := 1) (by norm_num) _ _ _ _ using 1
    . simp
    . simp; omega
    . simp; omega
    . apply _root_.mul_pos
      . apply _root_.mul_pos
        . norm_cast
          apply Nat.choose_pos
          omega
        . positivity
      . positivity
    . intro _ _ _
      positivity
  calc
    _ < n/((Nat.choose n 2)*x^2) := by
      gcongr
      norm_cast
      omega
      apply _root_.mul_pos
      norm_cast
      apply Nat.choose_pos
      omega
      positivity
    _ = 2/((n-1)*x^2) := by
      rw [div_mul_eq_div_mul_one_div, div_mul_eq_div_mul_one_div]
      congr 1
      rw [Nat.choose_two_right, Nat.cast_div]
      push_cast
      field_simp
      rw [div_mul_eq_div_mul_one_div, mul_div_cancel_left₀]
      ring_nf
      congr
      rw [Nat.cast_sub]
      simp
      ring
      . omega
      . norm_cast; omega
      . by_cases even : Even n
        . apply dvd_mul_of_dvd_left
          obtain ⟨k, keq⟩ := even
          use k
          rwa [two_mul]
        . apply dvd_mul_of_dvd_right
          obtain ⟨k, keq⟩ := Nat.not_even_iff_odd.mp even
          use k
          omega
      . simp
    _ < ε := by
      apply (div_lt_iff₀' _).mpr
      . rw [mul_assoc, mul_comm]
        apply (div_lt_iff₀' (by positivity)).mp
        apply lt_sub_right_of_add_lt
        apply Nat.lt_of_ceil_lt
        rw [add_comm, mul_comm]
        apply lt_of_lt_of_le _ ngt
        apply lt_of_lt_of_le _ (le_max_right _ _)
        exact lt_one_add _
      . apply _root_.mul_pos
        apply sub_pos_of_lt
        norm_cast
        omega
        positivity

omit [CompleteSpace F] in
private lemma natCast_eq_smul (n : ℕ) : (n : F) = ((n : ℝ) • 1) := by
  induction' n with n' ih
  . simp
  . simpa [add_smul]

theorem formalDeriv_absolutelyConvergent_on_disk {ps : PowerSeries F} (zdisk : z ∈ DiskOfConvergence ps) (rad_pos : RadiusOfConvergence ps ≠ 0) :
    AbsolutelyConvergent (ps.formalDeriv.series_at z) := by
  obtain ⟨w, wdisk, wgt⟩ := exists_dist_gt_of_mem_disk ps zdisk rad_pos
  set ρ := dist ps.center w
  have ρpos : ρ > 0 := by linarith [dist_nonneg (x := ps.center) (y := z)]
  have wgt' : dist z ps.formalDeriv.center < ρ := by rwa [dist_comm]
  obtain ⟨M, le_M⟩ : ∃ M, ∀ n, ‖ps.formalDeriv.coeff n‖ * ρ ^ n ≤ M := by
    obtain ⟨s, sdisk, sgt⟩ := exists_dist_gt_of_mem_disk ps wdisk rad_pos
    set ρ' := dist ps.center s
    have ρ'pos : ρ' > 0 := by linarith
    let q := ρ/ρ'
    have qlt1 : q < 1 := by
      apply (div_lt_one _).mpr sgt
      linarith [dist_nonneg (x := ps.center) (y := w)]
    have qpos : q > 0 := by positivity
    have tt := nqn_tendsto_zero qpos qlt1
    apply Tendsto.bddAbove_range at tt
    rw [bddAbove_def] at tt
    obtain ⟨M₁, b₁⟩ := tt
    have tt : Tendsto (fun n ↦ ‖ps.coeff n‖*ρ'^n) atTop (𝓝 0) := by
      unfold ρ'
      simp_rw [dist_comm, dist_eq_norm, ← norm_pow, ← norm_mul]
      apply tendsto_zero_iff_norm_tendsto_zero.mp
      apply termseq_tendsto_zero_of_series_convergent
      apply AbsolutelyConvergent.convergent
      exact powerSeries_absolutelyConvergent_of_mem_disk sdisk
    apply Tendsto.bddAbove_range at tt
    rw [bddAbove_def] at tt
    obtain ⟨M₂, b₂⟩ := tt
    use M₁*M₂*ρ⁻¹
    intro n
    simp at b₁
    specialize b₁ (n+1)
    simp at b₂
    specialize b₂ (n+1)
    have b₁nn : (n+1)*q^(n+1) ≥ 0 := by positivity
    have M₁nn : M₁ ≥ 0 := by
      apply le_trans b₁nn
      convert b₁
      norm_cast
    have b₂nn : ‖ps.coeff (n+1)‖*ρ'^(n+1) ≥ 0 := by positivity
    have M₂nn : M₂ ≥ 0 := le_trans b₂nn b₂
    simp [formalDeriv]
    calc
      ‖(n : F) + 1‖ * ‖ps.coeff (n+1)‖ * ρ^n = (n + 1) * ‖ps.coeff (n+1)‖ * q^(n+1) * ρ'^(n+1) * ρ⁻¹ := by
        norm_cast
        rw [natCast_eq_smul, norm_smul_of_nonneg (Nat.cast_nonneg _), norm_one, mul_one]
        nth_rw 2 [mul_assoc, mul_assoc]
        congr 1
        unfold q
        field_simp
        ring
      _ = (n+1)*q^(n+1) * (‖ps.coeff (n+1)‖ * ρ'^(n+1)) * ρ⁻¹ := by ring
      _ ≤ _ := by
        norm_cast
        apply mul_le_mul _ (le_refl _) (inv_nonneg_of_nonneg (le_of_lt ρpos)) (mul_nonneg M₁nn M₂nn)
        exact mul_le_mul b₁ b₂ b₂nn M₁nn
  apply powerSeries_absolutelyConvergent_by_abels_lemma _ _ le_M wgt'
  linarith [dist_nonneg (x := ps.center) (y := z)]

theorem formalDeriv_radiusOfConvergence_ge {ps : PowerSeries F} : RadiusOfConvergence ps.formalDeriv ≥ RadiusOfConvergence ps := by
  by_cases rad_pos : RadiusOfConvergence ps ≠ 0
  . by_contra lt
    apply lt_of_not_ge at lt
    obtain ⟨ρ, ρgt, ρlt⟩ := exists_between lt
    let z := ps.center + ρ.toReal•1
    have zderiv_not_conv : SeriesDivergent (ps.formalDeriv.series_at z) := by
      apply powerSeries_divergent_of_gt_radius
      unfold z
      simpa [formalDeriv, dist_eq_norm, norm_smul, ofReal_toReal (ne_top_of_lt ρlt)]
    have zderiv_conv : SeriesConvergent (ps.formalDeriv.series_at z) := by
      apply (formalDeriv_absolutelyConvergent_on_disk _ rad_pos).convergent
      apply mem_disk_of_lt_radius
      convert ρlt
      unfold z
      simp [dist_eq_norm, norm_smul]
      exact ne_top_of_lt ρlt
    contradiction
  . simp at rad_pos
    simp [rad_pos]

private def DerivAtCenter.f₁_ps (ps : PowerSeries F) := PowerSeries.mk (fun n ↦ ps.coeff (n+1)) ps.center

private noncomputable def DerivAtCenter.f₁ (ps : PowerSeries F) (x : F) :=
  if ENNReal.ofReal (dist x ps.center) < RadiusOfConvergence ps
  then (f₁_ps ps).isum_at x
  else ((ps.isum_at x) - (ps.isum_at ps.center)) / (x - ps.center)

omit [NormedSpace ℝ F] [CompleteSpace F] in
private theorem DerivAtCenter.f₁_conv {ps: PowerSeries F} (conv : SeriesConvergent (ps.series_at x)) :
    SeriesConvergent ((f₁_ps ps).series_at x) := by
  by_cases xcenter : x = ps.center
  . rw [xcenter]
    exact ⟨_, powerSeries_converges_to_at_center (f₁_ps ps)⟩

  have : (fun n ↦ ((f₁_ps ps).coeff n) *(x - (f₁_ps ps).center)^n) =
      fun n ↦ (x-(f₁_ps ps).center)⁻¹ * (((f₁_ps ps).coeff n) * (x-(f₁_ps ps).center)^(n+1)) := by
    funext n
    field_simp
    rw [mul_div_assoc]
    congr
    rw [pow_succ, mul_div_cancel_right₀]
    apply sub_ne_zero_of_ne
    convert xcenter using 1
  unfold series_at
  rw [this]
  apply SeriesConvergent.const_mul
  simp [add_comm _ 1]
  convert conv.from (n := 1) using 3
  rw [add_comm]
  rfl

private theorem DerivAtCenter.f₁_radiusOfConvergence_ge {ps : PowerSeries F} : RadiusOfConvergence (f₁_ps ps) ≥ RadiusOfConvergence ps := by
  by_cases rad_pos : RadiusOfConvergence ps ≠ 0
  . by_contra lt
    apply lt_of_not_ge at lt
    obtain ⟨ρ, ρgt, ρlt⟩ := exists_between lt
    let z := ps.center + ρ.toReal•1
    have zderiv_not_conv : SeriesDivergent ((f₁_ps ps).series_at z) := by
      apply powerSeries_divergent_of_gt_radius
      unfold z
      simp [f₁_ps, dist_eq_norm, norm_smul, ofReal_toReal (ne_top_of_lt ρlt)]
      convert ρgt using 1
    have zderiv_conv : SeriesConvergent ((f₁_ps ps).series_at z) := by
      apply f₁_conv
      apply (powerSeries_absolutelyConvergent_of_lt_radius _).convergent
      unfold z
      simp [dist_eq_norm, norm_smul]
      convert ρlt
      rw [ofReal_toReal (ne_top_of_lt ρlt)]
    contradiction
  . simp at rad_pos
    simp [rad_pos]

open Differentiation in
theorem powerSeries_hasDerivAt_center {ps : PowerSeries F} (rad_pos: RadiusOfConvergence ps ≠ 0) :
    HasDerivAt ps.isum_at (ps.coeff 1) ps.center := by
  rw [hasDerivAt_iff_exists_continuous]
  let f₁ := DerivAtCenter.f₁ ps
  refine' ⟨f₁, _, _, _⟩
  . intro x
    by_cases lt_rad : ENNReal.ofReal (dist x ps.center) < RadiusOfConvergence ps
    . dsimp [f₁, DerivAtCenter.f₁]
      have conv : SeriesConvergent (ps.series_at x) := (powerSeries_absolutelyConvergent_of_lt_radius lt_rad).convergent
      simp_rw [if_pos lt_rad, isum_at, (powerSeries_converges_to_at_center ps).get_isum]
      rw [← isum_const_mul_of_series_convergent (DerivAtCenter.f₁_conv conv)]
      unfold series_at DerivAtCenter.f₁_ps
      simp_rw [mul_comm, ← mul_assoc, mul_comm (x - ps.center), ← pow_succ, add_comm _ 1]
      have : (∑ᵢ fun k ↦ (x - ps.center)^(1+k) * ps.coeff (1+k)) = ∑ᵢfrom 1, ps.series_at x := by
        rw [isum_from]
        unfold series_at
        simp_rw [mul_comm]
      rw [this, isum_from_of_series_convergent conv (n := 1)]
      simp [series_at]
      congr
      funext _
      rw [mul_comm]
    . dsimp [f₁, DerivAtCenter.f₁]
      rw [if_neg lt_rad, mul_div_cancel₀]
      ring
      apply sub_ne_zero_of_ne
      contrapose! lt_rad
      rw [dist_eq_zero.mpr lt_rad]
      simp [pos_iff_ne_zero, rad_pos]
  . dsimp [f₁, DerivAtCenter.f₁]
    rw [if_pos]
    unfold isum_at
    convert (powerSeries_converges_to_at_center (DerivAtCenter.f₁_ps ps)).get_isum using 1
    simp [dist_self, pos_iff_ne_zero, rad_pos]
  . apply ContinuousWithinAt.continuousAt (s := DiskOfConvergence ps)
    . rw [continuousWithinAt_iff_continuousAt_restrict _ center_mem_disk]
      have : (DiskOfConvergence ps).restrict f₁ = (DiskOfConvergence ps).restrict (DerivAtCenter.f₁_ps ps).isum_at := by
        funext x
        unfold f₁
        dsimp [DerivAtCenter.f₁]
        rw [if_pos]
        exact lt_radius_of_mem_disk (Subtype.coe_prop x) rad_pos
      rw [this, ← continuousWithinAt_iff_continuousAt_restrict]
      apply ContinuousAt.continuousWithinAt
      apply powerSeries_continuousAt_of_mem_disk
      . apply ne_of_gt
        exact lt_of_lt_of_le (bot_lt_iff_ne_bot.mpr rad_pos) DerivAtCenter.f₁_radiusOfConvergence_ge
      . have : ps.center = (DerivAtCenter.f₁_ps ps).center := rfl
        rw [this]
        exact center_mem_disk
    . exact disk_mem_nhds rad_pos center_mem_disk
