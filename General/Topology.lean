import Mathlib.Analysis.Normed.Field.Basic
import Mathlib.Analysis.Normed.Module.Basic
import Mathlib.Topology.Order.T5

namespace General.Topology

open Set Function

theorem invFun_continuous_of_compact [TopologicalSpace X] [TopologicalSpace Y] [T2Space X] [T2Space Y] [Nonempty X]
    {f : X → Y} (compact : IsCompact M) (inj : Injective f) (cont : ContinuousOn f M) :
    ContinuousOn f.invFun (f '' M) := by
  rw [continuousOn_iff_isClosed]
  intro S Sclosed
  refine' ⟨f '' S ∩ f '' M, _, _⟩
  . apply IsCompact.isClosed
    rw [← image_inter inj]
    apply IsCompact.image_of_continuousOn
    . apply IsCompact.of_isClosed_subset compact _ inter_subset_right
      exact IsClosed.inter Sclosed compact.isClosed
    . exact ContinuousOn.mono cont inter_subset_right
  . rw [inter_assoc, inter_self]
    ext x
    constructor
    . rintro ⟨yPre, ⟨y, yM, rfl⟩⟩
      constructor
      . refine' ⟨y, _, rfl⟩
        rwa [← leftInverse_invFun inj y]
      . exact ⟨y, yM, rfl⟩
    . rintro ⟨⟨y, fyS, rfl⟩, ⟨z, fzM, feq⟩⟩
      constructor
      . simp
        rwa [leftInverse_invFun inj]
      . use z

theorem invFun_continuous_of_compactSpace [TopologicalSpace X] [TopologicalSpace Y] [T2Space X] [T2Space Y] [Nonempty X] [CompactSpace X]
    {f : X → Y} (bij : Function.Bijective f) (cont : Continuous f) :
    Continuous f.invFun := by
  rw [continuous_iff_continuousOn_univ, ← bij.surjective.range_eq, ← image_univ]
  exact invFun_continuous_of_compact CompactSpace.isCompact_univ bij.injective (continuous_iff_continuousOn_univ.mp cont)

open Filter _root_.Topology in
theorem supNorm_tendsto_zero_of_tendstoUniformlyOn [Field K] [SeminormedAddCommGroup K] [NormedSpace ℝ K]
    {f : ℕ → K → K} (unf : TendstoUniformlyOn f F atTop S) :
    Tendsto (fun i ↦ sSup ((norm ∘ (f i - F)) '' S)) atTop (𝓝 0) := by
  by_cases nempty : S = ∅
  . simp [nempty]
  obtain ⟨c, cIn⟩ := Set.nonempty_iff_ne_empty.mpr nempty
  rw [tendstoUniformlyOn_iff_tendsto, tendsto_uniformity_iff_dist_tendsto_zero] at unf
  simp [dist] at unf
  rw [tendsto_prod_iff] at unf
  rw [tendsto_nhds]
  intro s sOpen zIn
  simp
  obtain ⟨r, rpos, ballSub⟩ := Metric.isOpen_iff.mp sOpen 0 zIn
  obtain ⟨ρ, ρpos, ρlt⟩ := exists_between rpos
  let t := Metric.closedBall (0 : ℝ) ρ
  have tSub : t ⊆ s := by
    intro x xIn
    apply (subset_trans _ ballSub) xIn
    simp [t]
    intro y yIn
    simp at yIn ⊢
    linarith
  have tNbhd : t ∈ 𝓝 0 := Metric.closedBall_mem_nhds 0 ρpos
  obtain ⟨U, Uin, V, Vin, h⟩ := unf t tNbhd
  obtain ⟨N, h'⟩ := mem_atTop_sets.mp Uin
  simp at Vin
  use N
  intro n nN
  specialize h' n nN
  suffices mball : sSup ((norm ∘ (f n - F)) '' S) ∈ Metric.ball 0 r by
    exact ballSub mball
  simp
  apply lt_of_le_of_lt _ ρlt
  rw [abs_of_nonneg]; rotate_left
  . apply Real.sSup_nonneg
    intro x xIn
    simp at xIn
    obtain ⟨y, _, yeq⟩ := xIn
    rw [← yeq]
    exact norm_nonneg _
  rw [csSup_le_iff]; rotate_right
  . refine' ⟨‖f n c - F c‖, c, cIn, _⟩
    simp
  intro x xIn; rotate_left
  rw [bddAbove_iff_exists_ge ρ]
  refine' ⟨ρ, le_refl _, _⟩
  intro x xIn
  all_goals
    obtain ⟨y, yIn, yeq⟩ := xIn
    simp at yeq
    have yIn' : y ∈ V := Vin yIn
    specialize h n y h' yIn'
    simp [t, dist_comm (F y), dist_eq_norm] at h
    rwa [← yeq]
